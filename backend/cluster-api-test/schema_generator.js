const PIE = "PIE";
const LINE = "LINE";
const HISTOGRAM = "HISTOGRAM";
const SCATTER = "SCATTER";
const VALUE = "VALUE";

const state = ({skills, agentLocations, eventLocations, eventMagnitudes, agentCount, eventCount, gridBounds}) => ({
    skillGenerator: skills,
    agentLocationGenerator: agentLocations,
    eventLocationGenerator: eventLocations,
    eventMagnitudeGenerator: eventMagnitudes,
    agentCount,
    eventCount,
    gridBounds
});

const normal = (mu, sigma) => ({
    type: "NORMAL",
    mu,
    sigma
});

const uniform = (lower, upper) => ({
    type: "UNIFORM",
    lower,
    upper
});

const choices = (values, probabilities) => ({
    type: "CHOICE",
    values,
    probabilities
});

const independentLocation = (xGenerator, yGenerator) => ({
    type: "INDEPENDENT",
    xGenerator,
    yGenerator
});

const binaryEventMagnitude = (unluckyOdds = 0.5, luckyOdds = 0.5) => choices([-1, 1], [unluckyOdds, luckyOdds]);

const randomMovement = stepSize => ({
    type: "RANDOM",
    stepSize
});

const generalRedistribution = (taxes, redistributions, isTaxPercent = true) => ({
    type: "GENERAL",
    taxBrackets: taxes,
    redistributionBrackets: redistributions,
    isTaxPercent
});

const wealthVaryingValue = (wealthCutoffs, values, areCutoffsPercentile = true) => ({
    wealthCutoffs,
    values,
    areCutoffsPercentile
});

const eventInteraction = (unlucky, lucky, wealth) => ({
    probabilityUnluckyEvents: unlucky,
    probabilityLuckyEvents: lucky,
    wealth
});

const linear = ({multiplier, offset, min, max, variable}) => ({
    type: "LINEAR",
    multiplier,
    offset,
    min,
    max,
    variable
});

const multiplicativeEventWealth = (unlucky, lucky, magnitude) => ({
    type: "MULTIPLICATIVE",
    unluckyFactor: unlucky,
    luckyFactor: lucky,
    includeEventMagnitude: magnitude
});

const template = ({name, state, agentMovement, eventMovement, wealthRedistribution, eventInteraction, totalIterationCount, count = 1}) => ({
    name,
    stateConfig: state,
    agentMovement,
    eventMovement,
    wealthRedistribution,
    eventInteractionConfig: eventInteraction,
    totalIterationCount,
    count
});

const schema = (template, visualizer, execution) => {
    const type = template ? "FULL" : "STATS";
    return {
        type,
        template,
        visualizer,
        execution
    };
};

const topPercentWealth = (percent) => ({
    type: "TOP_PERCENT_FIXED_WEALTH",
    wealthPercent: percent
});

const visualization = (type, statistic, config = {}) => ({
    type,
    statistic,
    config
});

const visualizer = (name, perIteration, acrossIterations) => ({
    name,
    visualizations: {
        perIteration,
        acrossIterations
    }
});

const execution = ({simulator = "SERIES", state = "FORK_JOIN", iteration = "PARALLEL", steps = "FORK_JOIN", combine = "FORK_JOIN", stats = "FORK_JOIN", callback = "PARALLEL", incremental = true}) => ({
    simulator: simulator,
    createSimulationState: state,
    simulationIteration: iteration,
    simulationSteps: steps,
    combineStates: combine,
    statisticsTasks: stats,
    statisticsCallback: callback,
    incrementalUpdates: incremental
});

const multi = (...values) => ({
    type: "MULTI_VALUE",
    values
});

const output = schema(
    template({
        name: "Name",
        state: state({
              skills: multi(normal(multi(0.5, 1.0), 0.2), uniform(1, 2)),
              agentLocations: independentLocation(
                  uniform(-20, 20),
                  uniform(-20, 20)
              ),
              eventLocations: independentLocation(
                  uniform(-20, 20),
                  uniform(-20, 20)
              ),
              eventMagnitudes: binaryEventMagnitude(),
              agentCount: 1000,
              eventCount: 1000,
              gridBounds: 20
        }),
        agentMovement: randomMovement(1),
        eventMovement: null,
        wealthRedistribution: generalRedistribution(
            wealthVaryingValue([0, 50, 70], [20, 30, 40]),
            wealthVaryingValue([10, 20, 30], [1, 2, 3])
        ),
        eventInteraction: eventInteraction(
            unlucky = linear({
                multiplier: 1,
                offset: -0.7,
                min: 0,
                max: 0.2,
                variable: 'SKILL'
            }),
            lucky = linear({
                multiplier: 1,
                offset: -0.7,
                min: 0,
                max: 0.2,
                variable: 'SKILL'
            }),
            wealth = multiplicativeEventWealth(2, 0.5, false)
        ),
        totalIterationCount: 80,
        count: 2
    }),
    visualizer(
        "Visualizer",
        [visualization(VALUE, topPercentWealth(20))],
        []
    ),
    execution({})
);

function copy(data) {
    const proc = require('child_process').spawn('pbcopy');
    proc.stdin.write(data); proc.stdin.end();
}

console.dir(output, {depth: null, colors: true});
copy(JSON.stringify(output, null, "  "));