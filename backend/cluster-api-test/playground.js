const { createFilter, pruneFilter, runFilter, ObjectResolver, ValueResolver, LeafResolver } = require( "./object-filter");

const obj1 = {
    keyOne: {
        inner: {
            type: 'TYPE1',
            p1: 0,
            p2: {
                type: 'ABC',
                hi: 10,
                test: 11
            }
        }
    },
    keyTwo: {
        a: 3,
        b: 4
    }
};

const obj2 = {
    keyOne: {
        inner: {
            type: 'TYPE1',
            p1: 2,
            p2: {
                type: 'ABC',
                hi: 10,
                test: 11
            }
        }
    },
    keyTwo: {
        a: 3,
        b: 4
    }
};

const obj3 = {
    keyOne: {
        inner: {
            type: 'TYPE2',
            u: 7
        }
    },
    keyTwo: {
        a: 3,
        b: 4
    }
};

const no1 = {
    one: {
        a: {
            c: 6,
            d: {
                e: 8
            }
        },
        b: 3
    }
};

const no2 = {
    one: {
        a: {
            c: 6,
            d: {
                e: 7
            }
        },
        b: 3
    }
};

let filter = createFilter([obj1, obj2, obj3], [1, 2, 3]);
// filter = pruneFilter(filter);

let resolver = new ObjectResolver(
    {
        keyOne: new ObjectResolver(
            {
                inner: new ValueResolver({
                    TYPE1: new ObjectResolver({
                        p1: new ValueResolver({
                            2: new LeafResolver()
                        })
                    })
                })
            }
        )
    }
);
console.log(JSON.stringify(filter, null, 4));
const resolved = runFilter(filter, resolver);
console.log(resolved);