const WebSocket = require('ws');

// const api = {
//     // If false, must use series steps to get "correct" state on each update
//     execution: {
//         statisticsTasks: ParallelType,
//         statisticsCallback: ParallelType,
//         createSimulationState: ParallelType,
//         simulationIteration: ParallelType,
//         simulationSteps: ParallelType,
//         combineStates: ParallelType
//     },
//     // false just sends all stats back, true groups all stats by iteration number and sends back collections
//     // for all simulations present
//     grouping: boolean,
//     simulations: [SimulationConfig],
//     statistics: [StatConfig],
// };
//
// const ParallelType = ["FORK_JOIN", "FIXED_THREAD_POOL", "INLINE"]
// const GroupingOptions = ["NO_GROUPING", "GROUPED_AND_UNGROUPED", "GROUPED"]

const ws = new WebSocket('ws://localhost:1337');

ws.on('message', data => {
   console.log(data);
});

const makeSimulationConfig = ids => {
    return {
        ids: ids,
        config: {
            group_key: 'GROUP_1',
            state_config: {
                skill_generator: {
                    type: 'NORMAL',
                    mu: 0.6,
                    sigma: 0.2
                },
                agent_location_generator: {
                    type: 'INDEPENDENT',
                    x_generator: {
                        type: 'UNIFORM',
                        lower: -20,
                        upper: 20
                    },
                    y_generator: {
                        type: 'UNIFORM',
                        lower: -20,
                        upper: 20
                    }
                },
                event_location_generator: {
                    type: 'INDEPENDENT',
                    x_generator: {
                        type: 'UNIFORM',
                        lower: -20,
                        upper: 20
                    },
                    y_generator: {
                        type: 'UNIFORM',
                        lower: -20,
                        upper: 20
                    }
                },
                event_magnitude_generator: {
                    type: 'CHOICE',
                    values: [-1, 1],
                    probabilities: [0.5, 0.5]
                },
                agent_count: 1000,
                event_count: 500,
                grid_bounds: 20
            },
            agent_movement: {
                type: 'RANDOM',
                step_size: 1
            },
            event_movement: null,
            wealth_redistribution: {
                type: 'GENERAL',
                tax_brackets: {
                    wealth_cutoffs: [0, 50, 70],
                    values: [20, 30, 40],
                    are_cutoffs_percentile: true
                },
                redistribution_brackets: {
                    wealth_cutoffs: [10, 20, 30],
                    values: [1, 2, 3],
                    are_cutoffs_percentile: true
                },
                is_tax_percent: true
            },
            event_interaction_config: {
                probability_lucky_events: {
                    type: 'LINEAR',
                    multiplier: 1,
                    offset: 0,
                    min: 0.3,
                    max: 1,
                    variable: 'SKILL'
                },
                probability_unlucky_events: {
                    type: 'LINEAR',
                    multiplier: 1,
                    offset: -0.7,
                    min: 0,
                    max: 0.2,
                    variable: 'SKILL'
                },
                wealth: {
                    type: 'MULTIPLICATIVE',
                    lucky_factor: 2,
                    unlucky_factor: 0.5,
                    include_event_magnitude: false
                }
            },
            total_iteration_count: 80,
        }
    }
};

const FULL_REQUEST = {
    id: 'REQUEST_ID',
    type: 'FULL',
    execution: {
        simulator: 'SERIES',
        create_simulation_state: 'FORK_JOIN',
        simulation_iteration: 'PARALLEL',
        simulation_steps: 'FORK_JOIN',
        combine_states: 'FORK_JOIN',
        statistics_tasks: 'FORK_JOIN',
        statistics_callback: 'PARALLEL',
        incremental_updates: true,
    },
    grouping: 'GROUPED_AND_UNGROUPED',
    simulations: [
        makeSimulationConfig(['ID_1', 'ID_2']),
    ],
    statistics: [
        {
            id: 'STAT_ID_1',
            type: 'TOP_PERCENT_FIXED_WEALTH',
            wealth_percent: .80
        }
    ]
};

const STATS_REQUEST = {
    id: 'HELLO',
    type: 'STATS',
    statistics: [
        {
            id: 'STAT_ID_1',
            type: 'TOP_PERCENT_FIXED_WEALTH',
            wealth_percent: .80
        }
    ],
    execution: {
        statistics_tasks: 'FORK_JOIN',
        statistics_callback: 'PARALLEL',
    },
    group_keys: ['GROUP_1'],
    jobId: null,
    grouping: 'GROUPED_AND_UNGROUPED'
};

const TEST = {
    "id": "5f84f159-e8e9-475c-a562-3c170aa202fc",
    "type": "FULL",
    "simulations": [
        {
            "ids": [
                "19378992-fb64-436c-9d71-da9b12b282ec",
                "658793b8-acc0-4702-b776-4fe71b4e3d1f"
            ],
            "config": {
                "name": "Name",
                "stateConfig": {
                    "skillGenerator": {
                        "type": "NORMAL",
                        "mu": 0.5,
                        "sigma": 0.2
                    },
                    "agentLocationGenerator": {
                        "type": "INDEPENDENT",
                        "xGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        },
                        "yGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        }
                    },
                    "eventLocationGenerator": {
                        "type": "INDEPENDENT",
                        "xGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        },
                        "yGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        }
                    },
                    "eventMagnitudeGenerator": {
                        "type": "CHOICE",
                        "values": [
                            -1,
                            1
                        ],
                        "probabilities": [
                            0.5,
                            0.5
                        ]
                    },
                    "agentCount": 100001,
                    "eventCount": 1000,
                    "gridBounds": 20
                },
                "agentMovement": {
                    "type": "RANDOM",
                    "stepSize": 1
                },
                "eventMovement": null,
                "wealthRedistribution": {
                    "type": "GENERAL",
                    "taxBrackets": {
                        "wealthCutoffs": [
                            0,
                            50,
                            70
                        ],
                        "values": [
                            20,
                            30,
                            40
                        ],
                        "areCutoffsPercentile": true
                    },
                    "redistributionBrackets": {
                        "wealthCutoffs": [
                            10,
                            20,
                            30
                        ],
                        "values": [
                            1,
                            2,
                            3
                        ],
                        "areCutoffsPercentile": true
                    },
                    "isTaxPercent": true
                },
                "eventInteractionConfig": {
                    "probabilityUnluckyEvents": {
                        "type": "LINEAR",
                        "multiplier": 1,
                        "offset": -0.7,
                        "min": 0,
                        "max": 0.2,
                        "variable": "SKILL"
                    },
                    "probabilityLuckyEvents": {
                        "type": "LINEAR",
                        "multiplier": 1,
                        "offset": -0.7,
                        "min": 0,
                        "max": 0.2,
                        "variable": "SKILL"
                    },
                    "wealth": {
                        "type": "MULTIPLICATIVE",
                        "unluckyFactor": 2,
                        "luckyFactor": 0.5,
                        "includeEventMagnitude": false
                    }
                },
                "totalIterationCount": 80,
                "groupKey": "615a33c6-a46b-4d2e-a752-445aebef82b3",
                "jobId": "9070c72b-2688-4d4d-b71d-64b737a74100"
            }
        },
        {
            "ids": [
                "d9f2dc9e-bfe2-440f-888d-febe402d44a6",
                "a1c5465c-ccf0-4a10-8355-f96a394c52c9"
            ],
            "config": {
                "name": "Name",
                "stateConfig": {
                    "skillGenerator": {
                        "type": "NORMAL",
                        "mu": 1,
                        "sigma": 0.2
                    },
                    "agentLocationGenerator": {
                        "type": "INDEPENDENT",
                        "xGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        },
                        "yGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        }
                    },
                    "eventLocationGenerator": {
                        "type": "INDEPENDENT",
                        "xGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        },
                        "yGenerator": {
                            "type": "UNIFORM",
                            "lower": -20,
                            "upper": 20
                        }
                    },
                    "eventMagnitudeGenerator": {
                        "type": "CHOICE",
                        "values": [
                            -1,
                            1
                        ],
                        "probabilities": [
                            0.5,
                            0.5
                        ]
                    },
                    "agentCount": 100001,
                    "eventCount": 1000,
                    "gridBounds": 20
                },
                "agentMovement": {
                    "type": "RANDOM",
                    "stepSize": 1
                },
                "eventMovement": null,
                "wealthRedistribution": {
                    "type": "GENERAL",
                    "taxBrackets": {
                        "wealthCutoffs": [
                            0,
                            50,
                            70
                        ],
                        "values": [
                            20,
                            30,
                            40
                        ],
                        "areCutoffsPercentile": true
                    },
                    "redistributionBrackets": {
                        "wealthCutoffs": [
                            10,
                            20,
                            30
                        ],
                        "values": [
                            1,
                            2,
                            3
                        ],
                        "areCutoffsPercentile": true
                    },
                    "isTaxPercent": true
                },
                "eventInteractionConfig": {
                    "probabilityUnluckyEvents": {
                        "type": "LINEAR",
                        "multiplier": 1,
                        "offset": -0.7,
                        "min": 0,
                        "max": 0.2,
                        "variable": "SKILL"
                    },
                    "probabilityLuckyEvents": {
                        "type": "LINEAR",
                        "multiplier": 1,
                        "offset": -0.7,
                        "min": 0,
                        "max": 0.2,
                        "variable": "SKILL"
                    },
                    "wealth": {
                        "type": "MULTIPLICATIVE",
                        "unluckyFactor": 2,
                        "luckyFactor": 0.5,
                        "includeEventMagnitude": false
                    }
                },
                "totalIterationCount": 80,
                "groupKey": "aa4fe5d6-80ab-4ff7-874e-a195a11245b0",
                "jobId": "9070c72b-2688-4d4d-b71d-64b737a74100"
            }
        }
    ],
    "statistics": [
        {
            "type": "TOP_PERCENT_FIXED_WEALTH",
            "wealthPercent": 20,
            "combiners": {
                "782e943a-2ce6-4377-a620-8ffadb8bcecb": {
                    "type": "NONE"
                }
            },
            "id": "782e943a-2ce6-4377-a620-8ffadb8bcecb"
        }
    ],
    "execution": {
        "simulator": "SERIES",
        "createSimulationState": "FORK_JOIN",
        "simulationIteration": "PARALLEL",
        "simulationSteps": "FORK_JOIN",
        "combineStates": "FORK_JOIN",
        "statisticsTasks": "FORK_JOIN",
        "statisticsCallback": "PARALLEL",
        "incrementalUpdates": true
    }
};

ws.on('open', () => {
    // ws.send(JSON.stringify(FULL_REQUEST));
    ws.send(JSON.stringify(TEST));
});