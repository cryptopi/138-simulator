const NO_TYPE = "NO_TYPE";
const TYPE_KEY = "type";

class FilterNode {

    constructor() {
        this.leaves = new Set();
        this.leafList = []; // DEBUG ONLY
        this.hasChoice = false;
    }

    setLeaves(leafSet) {
        this.leaves = leafSet;
        this.leafList = Array.from(this.leaves);
    }
}

class Leaf extends FilterNode {

    constructor(values) {
        super();
        this.type = "LEAF";
        this.values = values;
        this.setLeaves(new Set(this.values));
    }
}

class ValueFilter extends FilterNode {

    constructor(values, valueMap) {
        super();
        this.type = "VALUE_FILTER";
        this.values = [...values];
        this.values.sort();
        this.valueMap = valueMap;
        this.values.forEach(value => {
            this.setLeaves(new Set([...this.leaves, ...this.valueMap[value].leaves]));
        });
        this.hasChoice = this.values.length > 1;
        this.isTypeFilter = false;
    }

    getFilter(value) {
        return this.valueMap[value];
    }

    setFilter(value, filter) {
        this.valueMap[value] = filter;
    }
}

class TypeFilter extends ValueFilter {

    constructor(values, valueMap) {
        super(values, valueMap);
        this.isTypeFilter = true;
        this.type = "TYPE_FILTER";
    }
}

class KeyedFilter extends FilterNode {

    constructor(keyFilterMap) {
        super();
        this.type = "KEYED_FILTER";
        this.keyFilterMap = keyFilterMap;
        Object.keys(this.keyFilterMap).forEach(key => {
            this.setLeaves(new Set([...this.leaves, ...this.keyFilterMap[key].leaves]));
        });
        this.hasChoice = false;
        Object.keys(this.keyFilterMap).forEach(key => {
            const node = this.keyFilterMap[key];
            if (node.hasChoice) {
                this.hasChoice = true;
            }
        });
    }

    getFilter(key) {
        return this.keyFilterMap[key];
    }

    setFilter(key, value) {
        this.keyFilterMap[key] = value;
    }
}

const getType = value => {
    return value.type ? value.type : NO_TYPE;
};

const haveSameType = values => {
    return values.map(value => getType(value)).every((val, i, arr) => val === arr[0]);
};

const isObject = obj => {
    return obj instanceof Object && !Array.isArray(obj);
};

const createFilter = (objects, leaves) => {
    const first = objects[0];
    if (!isObject(first)) {
        const values = objects;
        const valueLeafMap = {};
        // TODO need special logic if values are arrays
        // this only works if primitives
        values.forEach((value, i) => {
            const list = valueLeafMap[value] || [];
            list.push(leaves[i]);
            valueLeafMap[value] = list;
        });
        Object.keys(valueLeafMap).forEach(key => {
            valueLeafMap[key] = new Leaf(valueLeafMap[key]);
        });
        return new ValueFilter(Array.from(new Set(values)), valueLeafMap);
    }

    if (haveSameType(objects)) {
        const keyFilters = {};
        Object.keys(first).forEach(key => {
            if (key === TYPE_KEY) {
                return;
            }
            keyFilters[key] = createFilter(objects.map(obj => obj[key]), leaves);
        });
        return new KeyedFilter(keyFilters);
    }

    // Different types in objects, need type filter on type first
    const typeMapIndices = {};
    objects.forEach((obj, i) => {
        const type = getType(obj);
        const list = typeMapIndices[type] || [];
        list.push(i);
        typeMapIndices[type] = list;
    });

    const typeFilters = {};
    Object.keys(typeMapIndices).forEach(type => {
        const typedObjects = typeMapIndices[type].map(i => objects[i]);
        const typedLeaves = typeMapIndices[type].map(i => leaves[i]);
        typeFilters[type] = createFilter(typedObjects, typedLeaves);
    });
    return new TypeFilter(Object.keys(typeFilters), typeFilters);
};

const pruneFilter = filter => {
    if (!filter.hasChoice) {
        return new Leaf(filter.leaves);
    }
    if (filter instanceof ValueFilter) {
        filter.values.forEach(value => {
            filter.setFilter(value, pruneFilter(filter.getFilter(value)));
        });
        return filter;
    }
    if (filter instanceof KeyedFilter) {
        const keys = Object.keys(filter.keyFilterMap);
        keys.forEach(key => {
            filter.setFilter(key, pruneFilter(filter.getFilter(key)));
        });
    }
    return filter;
};

class LeafResolver {}

class ObjectResolver {

    constructor(keyResolverMap, isAnd) {
        this.keyResolverMap = keyResolverMap;
        this.isAnd = isAnd;
    }

    static makeWithMap(keyResolverMap, isAnd) {
        return new ObjectResolver(keyResolverMap, isAnd);
    }

    static makeAndWithMap(keyResolverMap) {
        return ObjectResolver.makeWithMap(keyResolverMap, true);
    }

    static makeOrWithMap(keyResolverMap) {
        return ObjectResolver.makeWithMap(keyResolverMap, false);
    }

    getResolver(key) {
        return this.keyResolverMap[key];
    }
}

class ValueResolver {

    constructor (valueResolverMap, isAnd, isWildcard) {
        this.valueResolverMap = valueResolverMap;
        this.isAnd = isAnd;
        this.isWildcard = isWildcard;
    }

    static makeWildcard() {
        return new ValueResolver(null, false, true);
    }

    static makeWithMap(valueResolverMap, isAnd) {
        return new ValueResolver(valueResolverMap, isAnd, false);
    }

    static makeOrWithMap(valueResolverMap) {
        return ValueResolver.makeWithMap(valueResolverMap, false);
    }

    static makeAndWithMap(valueResolverMap) {
        return ValueResolver.makeWithMap(valueResolverMap, true);
    }

    getResolver(value) {
        if (this.isWildcard) {
            return new LeafResolver();
        }
        return this.valueResolverMap[value];
    }
}

// TODO refactor to set-utils

const intersect = (a, b) => new Set([...a].filter(x => b.has(x)));
const union = (a, b) => new Set([...a, ...b]);

const runFilter = (filter, resolver) => {
    if (!filter) {
        return new Set();
    }
    if (resolver instanceof LeafResolver) {
        return filter.leaves;
    }
    const setFunction = filter.isAnd ? intersect : union;
    if (filter instanceof KeyedFilter && resolver instanceof ObjectResolver) {
        let leaves = new Set();
        Object.keys(resolver.keyResolverMap).forEach(key => {
            const keyFilter = filter.getFilter(key);
            if (!keyFilter) {
                throw new Error(`Key ${key} not present in filter ${filter}`);
            }
            leaves = setFunction(leaves, runFilter(keyFilter, resolver.getResolver(key)));
        });
        return leaves;
    }
    if (filter instanceof ValueFilter && resolver instanceof ValueResolver) {
        let leaves = new Set();
        const values = resolver.isWildcard ? filter.values : Object.keys(resolver.valueResolverMap);
        values.forEach(value => {
            const newLeaves = runFilter(filter.getFilter(value), resolver.getResolver(value));
            leaves = setFunction(leaves, newLeaves);
        });
        return leaves;
    }
    throw new Error(
        `Pairing between filter ${typeof(filter)} and 
        resolver ${typeof(resolver)} is not recognized`
    );
};

module.exports = {
    createFilter,
    pruneFilter,
    runFilter,
    ObjectResolver,
    ValueResolver,
    LeafResolver
};