package tasks;

import java.util.concurrent.Future;

public interface Task<T> {

    Future<T> compute();
}
