package tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class VoidFutureGroup implements Future<Void> {

    private List<Future<Void>> futures;

    public VoidFutureGroup(List<Future<Void>> futures) {
        this.futures = futures;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        boolean success = true;
        for (Future<Void> future : futures) {
            success &= future.cancel(mayInterruptIfRunning);
        }
        return success;
    }

    @Override
    public boolean isCancelled() {
        for (Future<Void> future : futures) {
            if (!future.isCancelled()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isDone() {
        for (Future<Void> future : futures) {
            if (!future.isDone()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Void get() throws InterruptedException, ExecutionException {
        for (Future<Void> future : futures) {
            future.get();
        }
        return null;
    }

    @Override
    public Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        for (Future<Void> future : futures) {
            future.get(timeout, unit);
        }
        return null;
    }
}
