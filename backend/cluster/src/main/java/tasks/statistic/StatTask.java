package tasks.statistic;

import tasks.Task;
import tasks.statistic.results.StatisticResult;

public interface StatTask extends Task<StatisticResult>  {

}
