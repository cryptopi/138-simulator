package tasks.statistic;

import simulations.Simulation;

public abstract class OverallStatTask implements StatTask {

    private Simulation simulation;

    public OverallStatTask(Simulation simulation) {
        this.simulation = simulation;
    }

    protected Simulation getSimulation() {
        return simulation;
    }
}
