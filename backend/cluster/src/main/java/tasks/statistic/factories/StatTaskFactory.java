package tasks.statistic.factories;

import config.statistic.StatConfig;
import simulations.Simulation;
import simulations.state.SimulationState;
import tasks.statistic.IterationStatTask;
import tasks.statistic.OverallStatTask;
import tasks.statistic.StatTask;

public interface StatTaskFactory {

    IterationStatTask createIterationTask(
            String simulationId,
            String groupKey,
            int iteration,
            StatConfig config,
            SimulationState state
    );
    OverallStatTask createOverallTask(StatConfig config, Simulation simulation);
}
