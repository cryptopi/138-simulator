package tasks.statistic.factories;

import config.statistic.StatConfig;
import config.statistic.TopPercentFixedWealthConfig;
import simulations.Simulation;
import simulations.state.SimulationState;
import tasks.statistic.IterationStatTask;
import tasks.statistic.OverallStatTask;
import tasks.statistic.fj.TopPercentFixedWealthTask;

public class ForkJoinStatTaskFactory implements StatTaskFactory {

    @Override
    public IterationStatTask createIterationTask(
            String simulationId,
            String groupKey,
            int iteration,
            StatConfig config,
            SimulationState state
    ) {
        if (config.getClass().equals(TopPercentFixedWealthConfig.class)) {
            return new TopPercentFixedWealthTask(
                    simulationId,
                    groupKey,
                    iteration,
                    state,
                    (TopPercentFixedWealthConfig)config
            );
        }
        throw new IllegalArgumentException(
                "StatConfig " + config + " cannot be instantiated");
    }

    @Override
    public OverallStatTask createOverallTask(StatConfig config, Simulation simulation) {
        throw new IllegalArgumentException(
                "StatConfig " + config + " cannot be instantiated");
    }
}
