package tasks.statistic;

import simulations.state.SimulationState;

public abstract class IterationStatTask implements StatTask {

    private String simulationId;
    private String groupKey;
    private int iteration;
    private SimulationState state;

    public IterationStatTask(
            String simulationId,
            String groupKey,
            int iteration,
            SimulationState state
    ) {
        this.simulationId = simulationId;
        this.groupKey = groupKey;
        this.iteration = iteration;
        this.state = state;
    }

    public String getSimulationId() {
        return simulationId;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public int getIteration() {
        return iteration;
    }

    public SimulationState getState() {
        return state;
    }
}
