package tasks.statistic.fj;

import config.statistic.TopPercentFixedWealthConfig;
import fj.SumDoubleArray;
import simulations.state.SimulationState;
import tasks.statistic.IterationStatTask;
import tasks.statistic.results.StatisticResult;
import utils.CollectionsUtils;
import utils.ForkJoinUtils;

import java.util.Arrays;
import java.util.concurrent.Future;
import java.util.concurrent.RecursiveTask;

public class TopPercentFixedWealthTask extends IterationStatTask {

    private TopPercentFixedWealthConfig config;

    private class ForkJoinTask extends RecursiveTask<StatisticResult> {

        @Override
        protected StatisticResult compute() {
            double[] wealths = getState().getWealths();
            double totalWealth =
                    new SumDoubleArray(wealths).invoke();
            totalWealth *= (config.getWealthPercent() / 100D);
            double[] sortedWealths = CollectionsUtils.copyArray(wealths);
            Arrays.parallelSort(sortedWealths);
            double wealthSum = 0;
            int index = 0;
            while (wealthSum < totalWealth) {
                wealthSum += wealths[index];
                index++;
            }
            return new StatisticResult(
                    config.getId(),
                    getSimulationId(),
                    getGroupKey(),
                    getIteration(),
                    index / (double)wealths.length
            );
        }
    }

    public TopPercentFixedWealthTask(
            String simulationId,
            String groupKey,
            int iteration,
            SimulationState state,
            TopPercentFixedWealthConfig config
    ) {
        super(simulationId, groupKey, iteration, state);
        this.config = config;
    }

    @Override
    public Future<StatisticResult> compute() {
        return ForkJoinUtils.getCommonPool().submit(new ForkJoinTask());
    }
}
