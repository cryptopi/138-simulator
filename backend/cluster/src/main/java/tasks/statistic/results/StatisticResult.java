package tasks.statistic.results;

import com.google.gson.annotations.Expose;

import java.util.Collection;
import java.util.Objects;

public class StatisticResult {

    private String statisticId;
    private String entityId;
    private String groupKey;
    private int iteration;
    private Object value;

    private transient Class<?> cls;

    public StatisticResult(
            String statisticId,
            String entityId,
            String groupKey,
            int iteration,
            Object value
    ) {
        this.statisticId = statisticId;
        this.entityId = entityId;
        this.groupKey = groupKey;
        this.iteration = iteration;
        this.value = value;
        this.cls = this.value.getClass();
    }

    public String getStatisticId() {
        return statisticId;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public int getIteration() {
        return iteration;
    }

    public Object getValue() {
        return value;
    }

    public <T> T as(Class<T> cls) {
        return cls.cast(value);
    }

    public boolean isScalar() {
        return Number.class.isAssignableFrom(cls);
    };

    public Double getScalar() {
        return (Double)cls.cast(value);
    }

    public boolean isArray() {
        return cls.isArray();
    }

    public boolean isCollection() {
        return cls.isArray() || Collection.class.isAssignableFrom(cls);
    }

    public Class<?> getCls() {
        return cls;
    }
}
