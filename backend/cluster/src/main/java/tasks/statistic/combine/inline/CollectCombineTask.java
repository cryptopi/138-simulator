package tasks.statistic.combine.inline;

import config.statistic.StatConfig;
import config.statistic.combine.StatCombinerConfig;
import tasks.statistic.combine.StatCombineTask;
import tasks.statistic.results.StatisticResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class CollectCombineTask extends StatCombineTask {

    public CollectCombineTask(
            StatConfig statConfig,
            StatCombinerConfig combinerConfig,
            Collection<StatisticResult> results
    ) {
        super(statConfig, combinerConfig, results);
    }

    @Override
    public Future<StatisticResult> compute() {
        double[] collection = new double[getResults().size()];
        List<StatisticResult> resultsList = new ArrayList<>(getResults());
        for (int i = 0; i < collection.length; i++) {
            collection[i] = resultsList.get(i).getScalar();
        }

        return CompletableFuture.completedFuture(new StatisticResult(
                getSampleResult().getStatisticId(),
                getSampleResult().getGroupKey(),
                getSampleResult().getGroupKey(),
                getSampleResult().getIteration(),
                collection
        ));
    }
}
