package tasks.statistic.combine.factories;

import config.statistic.combine.StatCombinerConfig;
import config.statistic.StatConfig;
import tasks.Task;
import tasks.statistic.results.StatisticResult;

import java.util.Collection;

public interface StatCombineTaskFactory {

    Task<StatisticResult> create(
            StatConfig statConfig,
            StatCombinerConfig combinerConfig,
            Collection<StatisticResult> results
    );
}
