package tasks.statistic.combine;

import config.statistic.combine.StatCombinerConfig;
import config.statistic.StatConfig;
import tasks.Task;
import tasks.statistic.results.StatisticResult;

import java.util.Collection;

public abstract class StatCombineTask implements Task<StatisticResult> {

    private StatConfig statConfig;
    private StatCombinerConfig combinerConfig;
    private Collection<StatisticResult> results;
    private StatisticResult sampleResult;

    public StatCombineTask(
            StatConfig statConfig,
            StatCombinerConfig combinerConfig,
            Collection<StatisticResult> results
    ) {
        this.statConfig = statConfig;
        this.combinerConfig = combinerConfig;
        this.results = results;
        this.sampleResult = this.results.stream().findFirst().orElse(null);
    }

    public StatConfig getStatConfig() {
        return statConfig;
    }

    public StatCombinerConfig getCombinerConfig() {
        return combinerConfig;
    }

    public Collection<StatisticResult> getResults() {
        return results;
    }

    public StatisticResult getSampleResult() {
        return sampleResult;
    }
}
