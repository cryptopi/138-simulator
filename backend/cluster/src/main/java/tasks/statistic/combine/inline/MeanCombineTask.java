package tasks.statistic.combine.inline;

import config.statistic.combine.StatCombinerConfig;
import config.statistic.StatConfig;
import tasks.statistic.combine.StatCombineTask;
import tasks.statistic.results.StatisticResult;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class MeanCombineTask extends StatCombineTask {

    public MeanCombineTask(
            StatConfig statConfig,
            StatCombinerConfig combinerConfig,
            Collection<StatisticResult> results
    ) {
        super(statConfig, combinerConfig, results);
    }

    @Override
    public Future<StatisticResult> compute() {
        double sum = 0;
        for (StatisticResult result : getResults()) {
            sum += result.getScalar();
        }
        sum /= getResults().size();
        return CompletableFuture.completedFuture(new StatisticResult(
                getSampleResult().getStatisticId(),
                getSampleResult().getGroupKey(),
                getSampleResult().getGroupKey(),
                getSampleResult().getIteration(),
                sum
        ));
    }
}
