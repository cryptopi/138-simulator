package tasks.statistic.combine.factories;

import config.statistic.combine.CollectStatCombineConfig;
import config.statistic.combine.StatCombinerConfig;
import config.statistic.StatConfig;
import config.statistic.combine.MeanStatCombineConfig;
import tasks.Task;
import tasks.statistic.combine.inline.CollectCombineTask;
import tasks.statistic.combine.inline.MeanCombineTask;
import tasks.statistic.results.StatisticResult;

import java.util.Collection;

public class InlineStatCombineTaskFactory implements StatCombineTaskFactory {

    @Override
    public Task<StatisticResult> create(
            StatConfig statConfig,
            StatCombinerConfig combinerConfig,
            Collection<StatisticResult> results
    ) {
        StatisticResult firstResult = results.stream().findFirst().orElse(null);
        if (firstResult == null) {
            return null;
        }
        if (firstResult.isScalar()) {
            if (combinerConfig instanceof MeanStatCombineConfig) {
                return new MeanCombineTask(statConfig, combinerConfig, results);
            } else if (combinerConfig instanceof CollectStatCombineConfig) {
                return new CollectCombineTask(statConfig, combinerConfig, results);
            }
        }
        throw new IllegalArgumentException(
                "Combiner " + combinerConfig + " is not valid with statistic "
                        + statConfig
        );
    }
}
