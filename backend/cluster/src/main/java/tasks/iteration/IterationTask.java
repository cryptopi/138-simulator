package tasks.iteration;

import simulations.state.updates.StateUpdate;

import java.util.List;
import java.util.concurrent.Future;

public interface IterationTask {

    Future<List<StateUpdate>> compute();
}
