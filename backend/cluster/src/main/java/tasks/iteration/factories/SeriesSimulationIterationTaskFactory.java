package tasks.iteration.factories;

import config.SimulationConfig;
import simulations.Simulation;
import simulations.state.SimulationState;
import tasks.state.combiner.SimulationStateCombiner;
import simulations.state.updates.ReplaceState;
import simulations.state.updates.StateUpdate;
import simulations.state.updates.StateUpdateGroup;
import tasks.FutureGroup;
import tasks.step.factories.SimulationStepTaskFactory;
import tasks.iteration.IterationTask;
import tasks.step.StepTask;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class SeriesSimulationIterationTaskFactory extends SimulationIterationTaskFactory {

    private SimulationStepTaskFactory stepTaskFactory;
    private SimulationStateCombiner stateCombiner;
    private boolean useRealState;

    public SeriesSimulationIterationTaskFactory(
            SimulationStepTaskFactory stepTaskFactory,
            SimulationStateCombiner stateCombiner
    ) {
        this(stepTaskFactory, stateCombiner, true);
    }

    public SeriesSimulationIterationTaskFactory(
            SimulationStepTaskFactory stepTaskFactory,
            SimulationStateCombiner stateCombiner,
            boolean useRealState
    ) {
        this.stepTaskFactory = stepTaskFactory;
        this.stateCombiner = stateCombiner;
        this.useRealState = useRealState;
    }

    @Override
    public IterationTask createTask(Simulation simulation, int stageIndex) {
        SimulationConfig config = simulation.getConfig();
        return () -> {
            List<StepTask> tasks = stepTaskFactory.createTasks(config);
            SimulationState lastState = simulation.getState(stageIndex - 1);
            List<Future<StateUpdate>> updateFutures;
            if (useRealState) {
                try {
                    for (StepTask task : tasks) {
                        Future<StateUpdate> future = task.compute(lastState);
                        StateUpdateGroup update =
                                new StateUpdateGroup(future.get());
                        lastState = stateCombiner.combine(lastState, update).get();
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    // TODO exception
                }
                updateFutures = Collections.singletonList(
                        CompletableFuture.completedFuture(
                                new ReplaceState(lastState)
                        )
                );
            } else {
                updateFutures = new ArrayList<>(tasks.size());
                try {
                    for (StepTask task : tasks) {
                        Future<StateUpdate> future = task.compute(lastState);
                        future.get();
                        updateFutures.add(future);
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    // TODO exception
                }
            }

            return new FutureGroup<>(updateFutures);
        };
    }
}
