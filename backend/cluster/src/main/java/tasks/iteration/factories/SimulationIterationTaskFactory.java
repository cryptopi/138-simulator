package tasks.iteration.factories;

import simulations.Simulation;
import tasks.iteration.IterationTask;

public abstract class SimulationIterationTaskFactory {

    public abstract IterationTask createTask(Simulation simulation, int stageIndex);
}
