package tasks.iteration.factories;

import config.SimulationConfig;
import simulations.Simulation;
import simulations.state.SimulationState;
import simulations.state.updates.StateUpdate;
import tasks.FutureGroup;
import tasks.step.factories.SimulationStepTaskFactory;
import tasks.iteration.IterationTask;
import tasks.step.StepTask;

import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class ParallelSimulationIterationTaskFactory extends SimulationIterationTaskFactory {

    private SimulationStepTaskFactory stepTaskFactory;

    public ParallelSimulationIterationTaskFactory(SimulationStepTaskFactory stepTaskFactory) {
        this.stepTaskFactory = stepTaskFactory;
    }

    @Override
    public IterationTask createTask(Simulation simulation, int stageIndex) {
        SimulationConfig config = simulation.getConfig();
        return () -> {
            List<StepTask> tasks = stepTaskFactory.createTasks(config);
            SimulationState lastState = simulation.getState(stageIndex - 1);
            List<Future<StateUpdate>> futures = tasks.stream()
                    .map(task -> task.compute(lastState))
                    .collect(Collectors.toList());
            return new FutureGroup<>(futures);
        };
    }
}
