package tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class FutureGroup<T> implements Future<List<T>> {

    private List<Future<T>> futures;

    public FutureGroup(List<Future<T>> futures) {
        this.futures = futures;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        boolean success = true;
        for (Future<T> future : futures) {
            success &= future.cancel(mayInterruptIfRunning);
        }
        return success;
    }

    @Override
    public boolean isCancelled() {
        for (Future<T> future : futures) {
            if (!future.isCancelled()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isDone() {
        for (Future<T> future : futures) {
            if (!future.isDone()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public List<T> get() throws InterruptedException, ExecutionException {
        List<T> values = new ArrayList<>(futures.size());
        for (Future<T> future : futures) {
            values.add(future.get());
        }
        return values;
    }

    @Override
    public List<T> get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        List<T> values = new ArrayList<>(futures.size());
        for (Future<T> future : futures) {
            values.add(future.get(timeout, unit));
        }
        return values;
    }
}
