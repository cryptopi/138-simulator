package tasks;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class ValueTask<T> implements Task<T> {

    private T value;

    public ValueTask(T value) {
        this.value = value;
    }

    @Override
    public Future<T> compute() {
        return CompletableFuture.completedFuture(value);
    }
}
