package tasks.callback;

import tasks.Task;
import utils.ForkJoinUtils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;

public class ForkJoinCallbackTask<T> extends CallbackTask<T> {

    private Task<T> task;
    private Consumer<T> consumer;

    private class ForkJoinAction extends RecursiveAction {

        @Override
        protected void compute() {
            T result = null;
            try {
                result = task.compute().get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            consumer.accept(result);
        }
    }

    public ForkJoinCallbackTask(Task<T> task, Consumer<T> consumer) {
       super(task, consumer);
    }

    @Override
    public Future<Void> compute() {
        return ForkJoinUtils.getCommonPool().submit(new ForkJoinAction());
    }
}
