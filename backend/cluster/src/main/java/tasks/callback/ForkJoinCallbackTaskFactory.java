package tasks.callback;

import tasks.Task;

import java.util.function.Consumer;

public class ForkJoinCallbackTaskFactory<T> implements CallbackTaskFactory<T> {

    @Override
    public CallbackTask<T> create(Task<T> task, Consumer<T> callback) {
        return new ForkJoinCallbackTask<>(task, callback);
    }
}
