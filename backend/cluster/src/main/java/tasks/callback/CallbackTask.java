package tasks.callback;

import tasks.Task;

import java.util.function.Consumer;

public abstract class CallbackTask<T> implements Task<Void> {

    private Task<T> task;
    private Consumer<T> consumer;

    public CallbackTask(Task<T> task, Consumer<T> consumer) {
        this.task = task;
        this.consumer = consumer;
    }

    public Task<T> getTask() {
        return task;
    }

    public Consumer<T> getConsumer() {
        return consumer;
    }
}
