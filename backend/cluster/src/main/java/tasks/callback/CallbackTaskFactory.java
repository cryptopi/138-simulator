package tasks.callback;

import tasks.Task;

import java.util.function.Consumer;

public interface CallbackTaskFactory<T> {

    CallbackTask<T> create(Task<T> task, Consumer<T> callback);
}
