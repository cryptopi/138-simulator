package tasks.callback;

import tasks.Task;
import utils.ForkJoinUtils;
import utils.ThreadPoolUtils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;

public class ThreadPoolCallbackTask<T> extends CallbackTask<T> {

    public ThreadPoolCallbackTask(Task<T> task, Consumer<T> consumer) {
       super(task, consumer);
    }

    @Override
    public Future<Void> compute() {
        return ThreadPoolUtils.getFixedPool().submit(() -> {
            T result = getTask().compute().get();
            getConsumer().accept(result);
            return null;
        });
    }
}
