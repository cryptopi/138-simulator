package tasks.state.factories;

import config.state.InitialSimulationStateConfig;
import tasks.state.CreateSimulationStateTask;

public interface CreateSimulationStateTaskFactory {

    CreateSimulationStateTask createTask(InitialSimulationStateConfig config);
}
