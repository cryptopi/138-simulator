package tasks.state.factories;

import config.state.*;
import fj.*;
import suppliers.sboolean.BooleanSupplier;
import suppliers.sdouble.ChoiceSupplier;
import suppliers.sdouble.DoubleSupplier;
import suppliers.sdouble.NormalSupplier;
import suppliers.sdouble.UniformSupplier;
import suppliers.sint.*;
import suppliers.sshort.ShortChoiceSupplier;
import suppliers.sshort.ShortNormalSupplier;
import suppliers.sshort.ShortSupplier;
import suppliers.sshort.ShortUniformSupplier;
import tasks.Task;
import tasks.ValueTask;
import tasks.state.CreateSimulationStateTask;
import utils.LocationUtils;

public class ForkJoinCreateSimulationStateTaskFactory implements CreateSimulationStateTaskFactory {

    private Task<double[]> makeTaskWithGeneratorConfig(
            GeneratorConfig config,
            double[] values
    ) {
        return new ForkJoinTaskExecutor<>(
                FillDoubleArray.createWithIndependentSupplier(
                        values,
                        () -> makeDoubleGeneratorSupplier(config)
                )
        );
    }

    private Task<short[]> makeTaskWithGeneratorConfig(
            GeneratorConfig config,
            short[] values
    ) {
        return new ForkJoinTaskExecutor<>(
                FillShortArray.createWithIndependentSupplier(
                        values,
                        () -> makeShortGeneratorSupplier(config)
                )
        );
    }

    private Task<int[]> makeTaskWithLocationConfig(
            LocationGeneratorConfig config,
            int[] values
    ) {
        if (config.getClass().equals(IndependentLocationGeneratorConfig.class)) {
            IndependentLocationGeneratorConfig locationConfig =
                    (IndependentLocationGeneratorConfig)config;
            GeneratorConfig xGenerator = locationConfig.getXGenerator();
            GeneratorConfig yGenerator = locationConfig.getYGenerator();

            return new ForkJoinTaskExecutor<>(
                    FillIntArray.createWithIndependentSupplier(
                            values,
                            () -> new IntIntToIntSupplier(
                                    makeIntGeneratorSupplier(xGenerator),
                                    makeIntGeneratorSupplier(yGenerator),
                                    LocationUtils::getLocation
                            )
                    )
            );
        }
        throw new IllegalArgumentException(
                "Location Generator config " + config + " is not recognized"
        );
    }

    private DoubleSupplier makeDoubleGeneratorSupplier(GeneratorConfig config) {
        if (config.getClass().equals(NormalGeneratorConfig.class)) {
            return new NormalSupplier((NormalGeneratorConfig)config);
        }
        if (config.getClass().equals(UniformGeneratorConfig.class)) {
            return new UniformSupplier((UniformGeneratorConfig)config);
        }
        if (config.getClass().equals(ChoiceGeneratorConfig.class)) {
            return new ChoiceSupplier((ChoiceGeneratorConfig)config);
        }
        throw new IllegalArgumentException(
                "Generator config " + config + " is not recognized"
        );
    }

    private IntSupplier makeIntGeneratorSupplier(GeneratorConfig config) {
        if (config.getClass().equals(NormalGeneratorConfig.class)) {
            return new IntNormalSupplier((NormalGeneratorConfig)config);
        }
        if (config.getClass().equals(UniformGeneratorConfig.class)) {
            return new IntUniformSupplier((UniformGeneratorConfig)config);
        }
        if (config.getClass().equals(ChoiceGeneratorConfig.class)) {
            return new IntChoiceSupplier((ChoiceGeneratorConfig)config);
        }
        throw new IllegalArgumentException(
                "Generator config " + config + " is not recognized"
        );
    }

    private ShortSupplier makeShortGeneratorSupplier(GeneratorConfig config) {
        if (config.getClass().equals(NormalGeneratorConfig.class)) {
            return new ShortNormalSupplier((NormalGeneratorConfig)config);
        }
        if (config.getClass().equals(UniformGeneratorConfig.class)) {
            return new ShortUniformSupplier((UniformGeneratorConfig)config);
        }
        if (config.getClass().equals(ChoiceGeneratorConfig.class)) {
            return new ShortChoiceSupplier((ChoiceGeneratorConfig)config);
        }
        throw new IllegalArgumentException(
                "Generator config " + config + " is not recognized"
        );
    }

    @Override
    public CreateSimulationStateTask createTask(InitialSimulationStateConfig config) {
        final int agentCount = config.getAgentCount();
        final int eventCount = config.getEventCount();
        Task<short[]> generationTask = new ValueTask<>(new short[config.getAgentCount()]);
        Task<int[]> parentTask = new ForkJoinTaskExecutor<>(
                FillIntArray.createWithIndependentSupplier(
                        new int[agentCount],
                        () -> IntSupplier.singletonSupplier(-1)
                )
        );

        Task<double[]> skillTask = makeTaskWithGeneratorConfig(
                config.getSkillGenerator(),
                new double[agentCount]
        );

        Task<int[]> agentLocationTask = makeTaskWithLocationConfig(
                config.getAgentLocationGenerator(),
                new int[agentCount]
        );

        Task<int[]> eventLocationTask = makeTaskWithLocationConfig(
                config.getEventLocationGenerator(),
                new int[eventCount]
        );

        Task<short[]> eventMagnitudeTask = makeTaskWithGeneratorConfig(
                config.getEventMagnitudeGenerator(),
                new short[eventCount]
        );

        Task<double[]> wealthTask = new ForkJoinTaskExecutor<>(
                FillDoubleArray.createWithIndependentSupplier(
                        new double[agentCount],
                        () -> DoubleSupplier.singletonSupplier(1)
                )
        );
        Task<boolean[]> areActiveTask = new ForkJoinTaskExecutor<>(
                FillBooleanArray.createWithIndependentSupplier(
                        new boolean[config.getAgentCount()],
                        () -> BooleanSupplier.singletonSupplier(true)
                )
        );

        return new CreateSimulationStateTask(
                generationTask,
                parentTask,
                skillTask,
                wealthTask,
                agentLocationTask,
                eventLocationTask,
                eventMagnitudeTask,
                areActiveTask,
                config.getGridBounds()
        );
    }
}
