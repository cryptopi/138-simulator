package tasks.state;

import simulations.state.SimulationState;
import tasks.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class CreateSimulationStateTask implements Task<SimulationState> {

    private Task<short[]> generationsTask;
    private Task<int[]> parentsTask;
    private Task<double[]> skillsTask;
    private Task<double[]> wealthsTask;
    private Task<int[]> agentLocationTask;
    private Task<int[]> eventLocationTask;
    private Task<short[]> eventMagnitudeTask;
    private Task<boolean[]> areActiveTask;
    private int gridBounds;

    public CreateSimulationStateTask(
            Task<short[]> generationsTask,
            Task<int[]> parentsTask,
            Task<double[]> skillsTask,
            Task<double[]> wealthsTask,
            Task<int[]> agentLocationTask,
            Task<int[]> eventLocationTask,
            Task<short[]> eventMagnitudeTask,
            Task<boolean[]> areActiveTask,
            int gridBounds
    ) {
        this.generationsTask = generationsTask;
        this.parentsTask = parentsTask;
        this.skillsTask = skillsTask;
        this.wealthsTask = wealthsTask;
        this.agentLocationTask = agentLocationTask;
        this.eventLocationTask = eventLocationTask;
        this.eventMagnitudeTask = eventMagnitudeTask;
        this.areActiveTask = areActiveTask;
        this.gridBounds = gridBounds;
    }

    public Future<SimulationState> compute() {
        Future<short[]> generationsFuture = generationsTask.compute();
        Future<int[]> parentsFuture = parentsTask.compute();
        Future<double[]> skillsFuture = skillsTask.compute();
        Future<double[]> wealthsFuture = wealthsTask.compute();
        Future<int[]> agentLocationFuture = agentLocationTask.compute();
        Future<int[]> eventLocationFuture = eventLocationTask.compute();
        Future<short[]> eventMagnitudeFuture = eventMagnitudeTask.compute();
        Future<boolean[]> areActiveFuture = areActiveTask.compute();

        List<Future<?>> futures = new ArrayList<>();
        futures.add(generationsFuture);
        futures.add(parentsFuture);
        futures.add(skillsFuture);
        futures.add(wealthsFuture);
        futures.add(agentLocationFuture);
        futures.add(eventLocationFuture);
        futures.add(eventMagnitudeFuture);
        futures.add(areActiveFuture);
        return new Future<>() {

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                boolean success = true;
                for (Future<?> future : futures) {
                    success &= future.cancel(mayInterruptIfRunning);
                }
                return success;
            }

            @Override
            public boolean isCancelled() {
                for (Future<?> future : futures) {
                    if (!future.isCancelled()) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            public boolean isDone() {
                for (Future<?> future : futures) {
                    if (!future.isDone()) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            public SimulationState get() throws InterruptedException, ExecutionException {
                return new SimulationState(
                        generationsFuture.get(),
                        parentsFuture.get(),
                        skillsFuture.get(),
                        wealthsFuture.get(),
                        agentLocationFuture.get(),
                        eventLocationFuture.get(),
                        eventMagnitudeFuture.get(),
                        areActiveFuture.get(),
                        gridBounds
                );
            }

            @Override
            public SimulationState get(
                    long timeout,
                    TimeUnit unit
            ) throws InterruptedException, ExecutionException, TimeoutException {
                return new SimulationState(
                        generationsFuture.get(timeout, unit),
                        parentsFuture.get(timeout, unit),
                        skillsFuture.get(timeout, unit),
                        wealthsFuture.get(timeout, unit),
                        agentLocationFuture.get(timeout, unit),
                        eventLocationFuture.get(timeout, unit),
                        eventMagnitudeFuture.get(timeout, unit),
                        areActiveFuture.get(timeout, unit),
                        gridBounds
                );
            }
        };
    }
}
