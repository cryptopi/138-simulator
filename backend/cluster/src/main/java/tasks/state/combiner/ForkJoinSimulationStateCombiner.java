package tasks.state.combiner;

import simulations.state.SimulationState;
import simulations.state.updates.*;
import utils.ForkJoinUtils;
import utils.LocationUtils;

import java.util.concurrent.Future;
import java.util.concurrent.RecursiveTask;

public class ForkJoinSimulationStateCombiner implements SimulationStateCombiner {

    private static class ForkJoinCombinerTask extends RecursiveTask<SimulationState> {

        private int agentStart;
        private int agentEnd;
        private int eventStart;
        private int eventEnd;
        private SimulationState prevState;
        private StateUpdateGroup updates;
        private SimulationState destState;
        private boolean appliedIncrementalGenerationUpdates;

        public ForkJoinCombinerTask(
                int agentStart,
                int agentEnd,
                int eventStart,
                int eventEnd,
                SimulationState prevState,
                StateUpdateGroup updates,
                SimulationState destState,
                boolean appliedIncrementalGenerationUpdates
        ) {
            this.agentStart = agentStart;
            this.agentEnd = agentEnd;
            this.eventStart = eventStart;
            this.eventEnd = eventEnd;
            this.prevState = prevState;
            this.updates = updates;
            this.destState = destState;
            this.appliedIncrementalGenerationUpdates =
                    appliedIncrementalGenerationUpdates;
        }

        public ForkJoinCombinerTask(
                SimulationState prevState,
                StateUpdateGroup updates,
                SimulationState destState,
                boolean appliedIncrementalGenerationUpdates
        ) {
            this(
                    0,
                    prevState.getAgentCount(),
                    0,
                    prevState.getEventCount(),
                    prevState,
                    updates,
                    destState,
                    appliedIncrementalGenerationUpdates
            );
        }

        private boolean shouldSplit() {
            int agentCount = agentEnd - agentStart;
            int eventCount = eventEnd - eventStart;
            return (agentCount > 100_000 || eventCount > 100_000)
                    && getSurplusQueuedTaskCount() < 5;
        }

        private void applyIncrementalGenerationUpdates() {
            for (GenerationUpdate update : updates.getGenerationUpdates()) {
                destState.appendData(
                        update.getGenerations(),
                        update.getParents(),
                        update.getSkills(),
                        update.getWealths(),
                        update.getLocations(),
                        update.getAreActive()
                );
            }
        }

        private void applyFullGenerationUpdate(GenerationUpdate update) {
            if (update != null) {
                destState.setGenerations(update.getGenerations());
                destState.setParents(update.getParents());
                destState.setSkills(update.getSkills());
                destState.setWealths(update.getWealths());
                destState.setAgentLocations(update.getLocations());
                destState.setAreActive(update.getAreActive());
            }
        }

        private void applyFullSkillUpdate(SkillUpdate update) {
            if (update != null) {
                destState.setSkills(update.getSkills());
            }
        }

        private void applyFullWealthUpdate(WealthUpdate update) {
            if (update != null) {
                destState.setWealths(update.getWealths());
            }
        }

        private void applyFullAgentLocationUpdate(AgentLocationUpdate update)  {
            if (update != null) {
                destState.setAgentLocations(update.getLocations());
            }
        }

        private void applyFullEventLocationUpdate(EventLocationUpdate update) {
            if (update != null) {
                destState.setEventLocations(update.getLocations());
            }
        }

        private void applyFullActiveUpdate(ActiveUpdate update) {
            if (update != null) {
                destState.setAreActive(update.getAreActive());
            }
        }

        private void computeDirectly() {
            for (int i = agentStart; i < agentEnd; i++) {
                for (SkillUpdate update : updates.getSkillUpdates()) {
                    destState.skills[i] += update.getSkills()[i];
                }
                for (WealthUpdate update : updates.getWealthUpdates()) {
                    destState.wealths[i] += update.getWealths()[i];
                }
                for (AgentLocationUpdate update : updates.getAgentLocationUpdates()) {
                    destState.agentLocations[i] = LocationUtils.addLocations(
                            destState.agentLocations[i],
                            update.getLocations()[i],
                            destState.getGridBounds()
                    );
                }
                for (EventLocationUpdate update : updates.getEventLocationUpdates()) {
                    destState.eventLocations[i] = LocationUtils.addLocations(
                            destState.eventLocations[i],
                            update.getLocations()[i],
                            destState.getGridBounds()
                    );
                }
                ActiveUpdate activeUpdate = updates.getLastActiveUpdate();
                if (activeUpdate != null) {
                    System.arraycopy(
                            activeUpdate.getAreActive(),
                            agentStart,
                            destState.areActive,
                            agentStart,
                            agentEnd - agentStart
                    );
                }
            }
        }

        private void computeForkJoin() {
            if (shouldSplit()) {
//                System.out.println("Total agent count: " + (agentEnd - agentStart));
//                System.out.println("Agent start: " + agentStart);
//                System.out.println("Agent end: " + agentEnd);
                int agentMidpoint = (agentEnd + agentStart) / 2;
//                System.out.println("Midpoint: " + agentMidpoint);
                int eventMidpoint = (eventEnd + eventStart) / 2;
                final ForkJoinCombinerTask one = new ForkJoinCombinerTask(
                        agentStart,
                        agentMidpoint,
                        eventStart,
                        eventMidpoint,
                        prevState,
                        updates,
                        destState,
                        appliedIncrementalGenerationUpdates
                );
                one.fork();
                final ForkJoinCombinerTask two = new ForkJoinCombinerTask(
                        agentMidpoint + 1,
                        agentEnd,
                        eventMidpoint + 1,
                        eventEnd,
                        prevState,
                        updates,
                        destState,
                        appliedIncrementalGenerationUpdates
                );
                two.compute();
                one.join();
            } else {
                computeDirectly();
            }
        }

        @Override
        protected SimulationState compute() {
            if (updates.hasReplaceUpdate()) {
                destState = updates.getReplaceUpdate().getState();
            } else {
                if (updates.isIncremental()) {
                    if (!appliedIncrementalGenerationUpdates) {
                        applyIncrementalGenerationUpdates();
                        appliedIncrementalGenerationUpdates = true;
                    }
                    computeForkJoin();
                } else {
                    applyFullGenerationUpdate(updates.getLastGenerationUpdate());
                    applyFullSkillUpdate(updates.getLastSkillUpdate());
                    applyFullWealthUpdate(updates.getLastWealthUpdate());
                    applyFullAgentLocationUpdate(updates.getLastAgentLocationUpdate());
                    applyFullEventLocationUpdate(updates.getLastEventLocationUpdate());
                    applyFullActiveUpdate(updates.getLastActiveUpdate());
                }
            }

            return destState;
        }
    }


    @Override
    public Future<SimulationState> combine(
            SimulationState prevState,
            StateUpdateGroup updates
    ) {
        // TODO performance could move all non-fork join paths out here and
        // run in the main thread since involve like 7 operations to avoid
        // giant unnecessary memory copy
        // But will have to check which fields are still null after all
        // updates are applied and copy just those over from prevState
        SimulationState dest = prevState.copy();
        ForkJoinCombinerTask task = new ForkJoinCombinerTask(prevState, updates, dest, false);
        return ForkJoinUtils.getCommonPool().submit(task);
    }
}
