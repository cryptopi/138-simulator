package tasks.state.combiner;

import simulations.state.SimulationState;
import simulations.state.updates.StateUpdateGroup;

import java.util.concurrent.Future;

public interface SimulationStateCombiner {

    Future<SimulationState> combine(
            SimulationState prevState,
            StateUpdateGroup updates
    );
}
