package tasks.step.fj;

import simulations.state.SimulationState;
import simulations.state.updates.StateUpdate;

import java.util.concurrent.RecursiveTask;

public abstract class BaseStepForkJoinTask<T extends StateUpdate> extends RecursiveTask<StateUpdate> {

    private SimulationState prevState;
    private T update;
    private int start;
    private int end;

    public BaseStepForkJoinTask(
            SimulationState prevState,
            T update,
            int start,
            int end
    ) {
        this.prevState = prevState;
        this.update = update;
        this.start = start;
        this.end = end;
    }

    public SimulationState getPrevState() {
        return prevState;
    }

    public T getUpdate() {
        return update;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    protected int getSplitLength() {
        return 100_000;
    }

    protected int getMaxSurplusQueuedTaskCount() {
        return 5;
    }

    protected boolean shouldSplit() {
        return end - start > getSplitLength()
                && getSurplusQueuedTaskCount() < getMaxSurplusQueuedTaskCount();
    }

    protected abstract void computeDirectly();

    protected abstract BaseStepForkJoinTask<T> split(int start, int end);

    protected void init() {}

    @Override
    protected StateUpdate compute() {
        init();
        if (shouldSplit()) {
            int midpoint = (start + end) / 2;
            final BaseStepForkJoinTask<T> one = split(0, midpoint);
            final BaseStepForkJoinTask<T> two = split(midpoint + 1, end);
            one.fork();
            two.compute();
            one.join();
        } else {
            computeDirectly();
        }
        return update;
    }
}
