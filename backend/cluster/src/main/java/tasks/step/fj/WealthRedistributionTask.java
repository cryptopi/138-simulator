package tasks.step.fj;

import config.GeneralWealthRedistributionConfig;
import config.PercentileWealthVaryingValue;
import config.WealthVaryingValue;
import simulations.state.SimulationState;
import simulations.state.updates.StateUpdate;
import simulations.state.updates.WealthUpdate;
import fj.base.SplittingForkJoinTaskNoReturn;
import tasks.step.BaseStepTask;
import utils.CollectionsUtils;
import utils.ForkJoinUtils;
import utils.StatisticsUtils;

import java.util.Arrays;
import java.util.concurrent.Future;

// TODO check for percentage logic
public class WealthRedistributionTask extends BaseStepTask<GeneralWealthRedistributionConfig> {

    private static class CollectionForkJoinTask extends SplittingForkJoinTaskNoReturn<double[]> {

        private double[] wealths;
        private WealthVaryingValue absoluteTaxBrackets;
        private boolean isTaxPercent;

        public CollectionForkJoinTask(
                double[] wealths,
                double[] contributions,
                WealthVaryingValue absoluteTaxBrackets,
                boolean isTaxPercent
        ) {
            this(
                    0,
                    wealths.length,
                    wealths,
                    contributions,
                    absoluteTaxBrackets,
                    isTaxPercent
            );
        }

        public CollectionForkJoinTask(
                int start,
                int end,
                double[] wealths,
                double[] contributions,
                WealthVaryingValue absoluteTaxBrackets,
                boolean isTaxPercent
        ) {
            super(start, end, contributions);
            this.wealths = wealths;
            this.absoluteTaxBrackets = absoluteTaxBrackets;
            this.isTaxPercent = isTaxPercent;
        }

        @Override
        protected void computeDirectly() {
            int start = getStart();
            int end = getEnd();
            double[] contributions = getValue();
            // TODO feature could have marginal tax brackets
            for (int i = start; i < end; i++) {
                double wealth = wealths[i];
                double value = absoluteTaxBrackets.getValueForWealth(wealth);
                double contribution;
                if (isTaxPercent) {
                    contribution = -wealth * value;
                } else {
                    contribution = -value;
                }
                contributions[i] = contribution;
            }
        }

        @Override
        protected CollectionForkJoinTask split(int start, int end) {
            return new CollectionForkJoinTask(
                    start,
                    end,
                    wealths,
                    getValue(),
                    absoluteTaxBrackets,
                    isTaxPercent
            );
        }
    }

    private class ForkJoinTask extends BaseStepForkJoinTask<WealthUpdate> {

        private WealthVaryingValue absoluteTaxBrackets;
        private WealthVaryingValue absoluteRedistributionBrackets;
        private double[] perAgentContributions;
        private double[] fundsPerAgentInRedistributionBracket;
        private boolean initialized;


        public ForkJoinTask(SimulationState prevState, WealthUpdate update) {
            super(prevState, update, 0, prevState.getAgentCount());
        }

        public ForkJoinTask(
                SimulationState prevState,
                WealthUpdate update,
                int start,
                int end,
                WealthVaryingValue absoluteTaxBrackets,
                WealthVaryingValue absoluteRedistributionBrackets,
                double[] perAgentContributions,
                double[] fundsPerAgentInRedistributionBracket,
                boolean initialized
        ) {
            super(prevState, update, start, end);
            this.absoluteTaxBrackets = absoluteTaxBrackets;
            this.absoluteRedistributionBrackets = absoluteRedistributionBrackets;
            this.perAgentContributions = perAgentContributions;
            this.fundsPerAgentInRedistributionBracket =
                    fundsPerAgentInRedistributionBracket;
            this.initialized = initialized;
        }

        // TODO performance maybe sort no matter what since that will make it
        // so you don't need to binary search for each wealth - can just
        // iterate in order
        @Override
        protected void init() {
            if (initialized) {
                return;
            }
            initialized = true;
            PercentileWealthVaryingValue taxBrackets = getConfig().getTaxBrackets();
            PercentileWealthVaryingValue disBrackets = getConfig().getRedistributionBrackets();
            if (!(taxBrackets.areCuttofsPercentile()
                    && disBrackets.areCuttofsPercentile())) {
                throw new IllegalArgumentException(
                        "Tax brackets and redistribution brackets must " +
                                "be defined in terms of percentiles"
                );
            }
            // Recall that the first value must be 0, so values of length 1
            // do not require a sort since 0 is the same as a percentile
            // and as a absolute value.
            boolean needsTaxSort = taxBrackets.getValues().length > 1;
            boolean needsDisSort = disBrackets.getValues().length > 1;
            if (needsTaxSort || needsDisSort) {
                double[] sortedWealths = CollectionsUtils.copyArray(
                        getPrevState().getWealths()
                );
                Arrays.parallelSort(sortedWealths);
                if (needsTaxSort) {
                    double[] percentileWealthCutoffs =
                            taxBrackets.getWealthCutoffs();
                    double[] absoluteWealthCutoffs = Arrays
                            .stream(percentileWealthCutoffs)
                            .map(p -> StatisticsUtils.getPercentile(
                                    sortedWealths, p
                            ))
                            .toArray();
                    absoluteTaxBrackets = new WealthVaryingValue(
                            absoluteWealthCutoffs,
                            taxBrackets.getValues(),
                            false
                    );
                }
                if (needsDisSort) {
                    double[] percentileWealthCutoffs =
                            disBrackets.getWealthCutoffs();
                    double[] absoluteWealthCutoffs = Arrays
                            .stream(percentileWealthCutoffs)
                            .map(p -> StatisticsUtils.getPercentile(
                                    sortedWealths, p
                            ))
                            .toArray();
                    absoluteRedistributionBrackets = new WealthVaryingValue(
                            absoluteWealthCutoffs,
                            disBrackets.getValues(),
                            false
                    );
                }
            } else {
                absoluteTaxBrackets = new WealthVaryingValue(
                        taxBrackets.getWealthCutoffs(),
                        taxBrackets.getValues(),
                        false
                );
                absoluteRedistributionBrackets = new WealthVaryingValue(
                        disBrackets.getWealthCutoffs(),
                        disBrackets.getValues(), false
                );
            }
            perAgentContributions = new double[getPrevState().getAgentCount()];
            new CollectionForkJoinTask(
                    getPrevState().getWealths(),
                    perAgentContributions,
                    absoluteTaxBrackets,
                    getConfig().isTaxPercent()
            ).invoke();

            // Split the total contribution amongst the redistribution brackets
            // as specified by their respective percentage allotments.
            double totalContribution = -Arrays.stream(perAgentContributions).parallel().sum();
            fundsPerAgentInRedistributionBracket =
                    new double[disBrackets.getWealthCutoffs().length];
            int redistributionBracketCount =
                    disBrackets.getWealthCutoffs().length;
            int lastAgentCount = 0;
            for (int i = 0; i < redistributionBracketCount; i++) {
                int agentCount = StatisticsUtils.getCountBeforePercentile(
                        redistributionBracketCount,
                        disBrackets.getWealthCutoffs()[i]
                );
                int deltaAgentCount = agentCount - lastAgentCount;
                double fundsForBracket =
                        totalContribution * disBrackets.getValues()[i];
                lastAgentCount = agentCount;
                fundsPerAgentInRedistributionBracket[i] =
                        fundsForBracket / ((double)deltaAgentCount);
            }
        }

        @Override
        protected void computeDirectly() {
            double [] wealths = getPrevState().getWealths();
            for (int i = getStart(); i < getEnd(); i++) {
                int redistributionBracket =
                        absoluteRedistributionBrackets.getBracketIndex(
                                wealths[i]
                        );
                double handout =
                        fundsPerAgentInRedistributionBracket[redistributionBracket];
                getUpdate().getWealths()[i] = - perAgentContributions[i] + handout;
                if (!isIncremental()) {
                    getUpdate().getWealths()[i] += wealths[i];
                }
            }
        }

        @Override
        protected BaseStepForkJoinTask<WealthUpdate> split(int start, int end) {
            return new ForkJoinTask(
                    getPrevState(),
                    getUpdate(),
                    start,
                    end,
                    absoluteTaxBrackets,
                    absoluteRedistributionBrackets,
                    perAgentContributions,
                    fundsPerAgentInRedistributionBracket,
                    initialized
            );
        }
    }

    public WealthRedistributionTask(
            GeneralWealthRedistributionConfig config,
            boolean incremental
    ) {
        super(config, incremental);
    }

    @Override
    public Future<StateUpdate> compute(SimulationState previous) {
        WealthUpdate update = new WealthUpdate(
                isIncremental(),
                new double[previous.getAgentCount()]
        );
        return ForkJoinUtils.getCommonPool().submit(
                new ForkJoinTask(previous, update)
        );
    }
}
