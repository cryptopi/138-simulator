package tasks.step.fj;

import config.EventInteractionConfig;
import config.EventWealthConfig;
import config.NumberConfig;
import functions.AgentEventFunction;
import functions.AgentFunction;
import functions.EventFunction;
import functions.FunctionFactory;
import simulations.state.SimulationState;
import simulations.state.updates.StateUpdate;
import simulations.state.updates.WealthUpdate;
import tasks.step.BaseStepTask;
import utils.CollectionsUtils;
import utils.ForkJoinUtils;
import utils.ProbabilityUtils;

import java.util.Map;
import java.util.concurrent.Future;

public class EventInteractionTask extends BaseStepTask<EventInteractionConfig> {

    private AgentEventFunction probabilityUtilizeLuckyEvent;
    private AgentEventFunction probabilityAvoidUnluckyEvent;
    private AgentEventFunction wealthFunction;

    private class ForkJoinTask extends BaseStepForkJoinTask<WealthUpdate> {

        private Map<Integer, Short> eventLocations;

        public ForkJoinTask(SimulationState prevState, WealthUpdate update) {
            super(prevState, update, 0, prevState.getAgentCount());
            this.eventLocations = CollectionsUtils.toMap(
                    prevState.getEventLocations(),
                    prevState.getEventMagnitudes()
            );
        }

        public ForkJoinTask(
                SimulationState prevState,
                WealthUpdate update,
                int start,
                int end,
                Map<Integer, Short> eventLocations
        ) {
            super(prevState, update, start, end);
            this.eventLocations = eventLocations;
        }

        @Override
        protected BaseStepForkJoinTask<WealthUpdate> split(int start, int end) {
            return new ForkJoinTask(
                    getPrevState(),
                    getUpdate(),
                    start,
                    end,
                    eventLocations
            );
        }

        @Override
        protected void computeDirectly() {
            SimulationState prevState = getPrevState();
            for (int i = getStart(); i < getEnd(); i++) {
                int agentLocation = prevState.getAgentLocations()[i];
                double previousWealth = prevState.getWealths()[i];
                double wealth = previousWealth;
                if (eventLocations.containsKey(agentLocation)) {
                    double skill = prevState.getSkills()[i];
                    short magnitude = eventLocations.get(agentLocation);
                    boolean lucky = magnitude > 0;
                    boolean gotLucky = lucky && ProbabilityUtils.withProbability(
                            probabilityUtilizeLuckyEvent.compute(
                                    skill,
                                    previousWealth,
                                    magnitude
                            )
                    );
                    boolean gotUnlucky = !lucky && !ProbabilityUtils.withProbability(
                            probabilityAvoidUnluckyEvent.compute(
                                    skill,
                                    previousWealth,
                                    magnitude
                            )
                    );
                    if (gotLucky || gotUnlucky) {
                        wealth = wealthFunction.compute(
                                skill,
                                previousWealth,
                                magnitude
                        );
                    }
                }
                if (isIncremental()) {
                    wealth -= previousWealth;
                }
                getUpdate().getWealths()[i] = wealth;
            }
        }
    }

    public EventInteractionTask(
            EventInteractionConfig config,
            boolean incremental,
            FunctionFactory functionFactory
    ) {
        super(config, incremental);
        init(config, functionFactory);
    }

    private void init(EventInteractionConfig config, FunctionFactory factory) {
        NumberConfig probabilityLucky = config.getProbabilityLuckyEvents();
        NumberConfig probabilityUnlucky = config.getProbabilityUnluckyEvents();
        EventWealthConfig eventWealth = config.getWealth();
        probabilityUtilizeLuckyEvent = factory.createNumericFunction(probabilityLucky);
        probabilityAvoidUnluckyEvent = factory.createNumericFunction(probabilityUnlucky);
        wealthFunction = factory.createEventWealthFunction(eventWealth);
    }

    @Override
    public Future<StateUpdate> compute(SimulationState previous) {
        WealthUpdate update = new WealthUpdate(
                isIncremental(),
                new double[previous.getAgentCount()]
        );
        return ForkJoinUtils.getCommonPool().submit(
                new ForkJoinTask(previous, update)
        );
    }
}
