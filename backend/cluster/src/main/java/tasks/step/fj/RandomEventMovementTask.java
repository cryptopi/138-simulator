package tasks.step.fj;

import config.RandomMovementConfig;
import simulations.state.SimulationState;
import simulations.state.updates.EventLocationUpdate;
import simulations.state.updates.StateUpdate;
import tasks.step.BaseStepTask;
import utils.ForkJoinUtils;
import utils.LocationUtils;

import java.util.Random;
import java.util.concurrent.Future;

public class RandomEventMovementTask extends BaseStepTask<RandomMovementConfig> {

    private class ForkJoinTask extends BaseStepForkJoinTask<EventLocationUpdate> {

        private Random generator;

        public ForkJoinTask(SimulationState prevState, EventLocationUpdate update) {
            super(prevState, update, 0, prevState.getEventCount());
            this.generator = new Random();
        }

        public ForkJoinTask(
                SimulationState prevState,
                EventLocationUpdate update,
                int start,
                int end
        ) {
            super(prevState, update, start, end);
            this.generator = new Random();
        }

        @Override
        protected BaseStepForkJoinTask<EventLocationUpdate> split(int start, int end) {
            return new ForkJoinTask(getPrevState(), getUpdate(), start, end);
        }

        @Override
        protected void computeDirectly() {
            EventLocationUpdate update = getUpdate();
            for (int i = getStart(); i < getEnd(); i++) {
                boolean one = generator.nextDouble() < 0.5;
                boolean two = generator.nextDouble() < 0.5;
                short xChange = 0;
                short yChange = 0;
                short stepSize = (short)getConfig().getStepSize();
                if (one && two) {
                    yChange = stepSize;
                } else if (!one && two) {
                    xChange = stepSize;
                } else if (one && !two) {
                    yChange = (short)-stepSize;
                } else {
                    xChange = (short)-stepSize;
                }
                if (isIncremental()) {
                    update.getLocations()[i] =
                            LocationUtils.getLocation(xChange, yChange);
                } else {
                    update.getLocations()[i] = LocationUtils.addLocations(
                            getPrevState().getEventLocations()[i],
                            LocationUtils.getLocation(xChange, yChange),
                            getPrevState().getGridBounds()
                    );
                }
            }
        }
    }

    public RandomEventMovementTask(
            RandomMovementConfig config,
            boolean incremental
    ) {
        super(config, incremental);
    }

    @Override
    public Future<StateUpdate> compute(SimulationState previous) {
        EventLocationUpdate update = new EventLocationUpdate(
                isIncremental(),
                new int[previous.getEventCount()]
        );
        return ForkJoinUtils.getCommonPool().submit(
                new ForkJoinTask(previous, update)
        );
    }
}
