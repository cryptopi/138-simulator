package tasks.step.fj;

import config.RandomMovementConfig;
import simulations.state.SimulationState;
import simulations.state.updates.AgentLocationUpdate;
import simulations.state.updates.StateUpdate;
import tasks.step.BaseStepTask;
import utils.ForkJoinUtils;
import utils.LocationUtils;

import java.util.Random;
import java.util.concurrent.Future;

public class RandomAgentMovementTask extends BaseStepTask<RandomMovementConfig> {

    private class ForkJoinTask extends BaseStepForkJoinTask<AgentLocationUpdate> {

        private Random generator;

        public ForkJoinTask(SimulationState prevState, AgentLocationUpdate update) {
            super(prevState, update, 0, prevState.getAgentCount());
            this.generator = new Random();
        }

        public ForkJoinTask(
                SimulationState prevState,
                AgentLocationUpdate update,
                int start,
                int end
        ) {
            super(prevState, update, start, end);
            this.generator = new Random();
        }

        @Override
        protected BaseStepForkJoinTask<AgentLocationUpdate> split(int start, int end) {
            return new ForkJoinTask(getPrevState(), getUpdate(), start, end);
        }

        @Override
        protected void computeDirectly() {
            AgentLocationUpdate update = getUpdate();
            for (int i = getStart(); i < getEnd(); i++) {
                boolean one = generator.nextDouble() < 0.5;
                boolean two = generator.nextDouble() < 0.5;
                short xChange = 0;
                short yChange = 0;
                short stepSize = (short)getConfig().getStepSize();
                if (one && two) {
                    yChange = stepSize;
                } else if (!one && two) {
                    xChange = stepSize;
                } else if (one && !two) {
                    yChange = (short)-stepSize;
                } else {
                    xChange = (short)-stepSize;
                }
                if (isIncremental()) {
                    update.getLocations()[i] =
                            LocationUtils.getLocation(xChange, yChange);
                } else {
                    update.getLocations()[i] = LocationUtils.addLocations(
                            getPrevState().getAgentLocations()[i],
                            LocationUtils.getLocation(xChange, yChange),
                            getPrevState().getGridBounds()
                    );
                }
            }
        }
    }

    public RandomAgentMovementTask(
            RandomMovementConfig config,
            boolean incremental
    ) {
        super(config, incremental);
    }

    @Override
    public Future<StateUpdate> compute(SimulationState previous) {
        AgentLocationUpdate update = new AgentLocationUpdate(
                isIncremental(),
                new int[previous.getAgentCount()]
        );
        return ForkJoinUtils.getCommonPool().submit(
                new ForkJoinTask(previous, update)
        );
    }
}
