package tasks.step;

import simulations.state.SimulationState;
import simulations.state.updates.StateUpdate;

import java.util.concurrent.Future;

public interface StepTask {

    Future<StateUpdate> compute(SimulationState previous);
}
