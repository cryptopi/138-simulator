package tasks.step.factories;

import config.SimulationConfig;
import config.StepConfig;
import config.StepType;
import tasks.step.StepTask;

import java.util.ArrayList;
import java.util.List;

public abstract class SimulationStepTaskFactory {

    public abstract StepTask createTask(StepType type, StepConfig config);

    public List<StepTask> createTasks(SimulationConfig config) {
        List<StepTask> tasks = new ArrayList<>();
        tasks.add(createTask(
                StepType.AGENT_MOVEMENT,
                config.getAgentMovement()
        ));
        tasks.add(createTask(
                StepType.EVENT_INTERACTION,
                config.getEventInteractionConfig()
        ));
        if (config.getEventMovement().isPresent()) {
            tasks.add(createTask(
                    StepType.EVENT_MOVEMENT,
                    config.getEventMovement().get()
            ));
        }
        return tasks;
    }
}
