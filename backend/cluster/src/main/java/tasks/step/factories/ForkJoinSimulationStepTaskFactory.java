package tasks.step.factories;

import config.*;
import functions.FunctionFactory;
import tasks.step.fj.EventInteractionTask;
import tasks.step.fj.RandomAgentMovementTask;
import tasks.step.StepTask;
import tasks.step.fj.RandomEventMovementTask;
import tasks.step.fj.WealthRedistributionTask;

public class ForkJoinSimulationStepTaskFactory extends SimulationStepTaskFactory {

    private boolean incremental;
    private FunctionFactory functionFactory;

    public ForkJoinSimulationStepTaskFactory(
            boolean incremental,
            FunctionFactory functionFactory
    ) {
        this.incremental = incremental;
        this.functionFactory = functionFactory;
    }

    @Override
    public StepTask createTask(StepType type, StepConfig config) {
        if (type ==  StepType.AGENT_MOVEMENT &&
                config.getClass().equals(RandomMovementConfig.class)) {
            return new RandomAgentMovementTask(
                    (RandomMovementConfig)config, incremental
            );
        }
        if (type ==  StepType.EVENT_MOVEMENT &&
                config.getClass().equals(RandomMovementConfig.class)) {
            return new RandomEventMovementTask(
                    (RandomMovementConfig)config, incremental
            );
        }
        if (type == StepType.EVENT_INTERACTION &&
                config.getClass().equals(EventInteractionConfig.class)) {
            return new EventInteractionTask(
                    (EventInteractionConfig) config,
                    incremental,
                    functionFactory
            );
        }
        if (type == StepType.WEALTH_REDISTRIBUTION &&
                config.getClass().equals(GeneralWealthRedistributionConfig.class)) {
            return new WealthRedistributionTask(
                    (GeneralWealthRedistributionConfig)config, incremental
            );
        }
        throw new IllegalArgumentException(
                "StepType " + type + " and StepConfig " +
                        config + " cannot be instantiated"
        );
    }
}
