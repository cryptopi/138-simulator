package tasks.step;

public abstract class BaseStepTask<T> implements StepTask {

    private T config;
    private boolean incremental;

    public BaseStepTask(T config, boolean incremental) {
        this.config = config;
        this.incremental = incremental;
    }

    final protected T getConfig() {
        return config;
    }

    final protected boolean isIncremental() {
        return incremental;
    }
}
