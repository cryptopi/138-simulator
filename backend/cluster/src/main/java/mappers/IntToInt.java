package mappers;

import suppliers.sint.IntSupplier;

public interface IntToInt {

    int supply(int num);

    static IntToInt fromSupplier(IntSupplier supplier) {
        return i -> supplier.get();
    }
}
