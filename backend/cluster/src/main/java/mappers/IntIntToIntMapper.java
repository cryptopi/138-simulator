package mappers;

public interface IntIntToIntMapper {

    int map(int one, int two);
}
