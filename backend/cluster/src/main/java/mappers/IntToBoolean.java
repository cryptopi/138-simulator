package mappers;

import suppliers.sboolean.BooleanSupplier;

public interface IntToBoolean {

    boolean supply(int num);

    static IntToBoolean fromSupplier(BooleanSupplier supplier) {
        return i -> supplier.get();
    }
}
