package mappers;

import suppliers.sshort.ShortSupplier;

public interface IntToShort {

    short supply(int num);

    static IntToShort fromSupplier(ShortSupplier supplier) {
        return i -> supplier.get();
    }
}
