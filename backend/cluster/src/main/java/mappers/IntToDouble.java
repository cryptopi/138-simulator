package mappers;

import suppliers.sdouble.DoubleSupplier;

public interface IntToDouble {

    double supply(int num);

    static IntToDouble fromSupplier(DoubleSupplier supplier) {
        return i -> supplier.get();
    }
}
