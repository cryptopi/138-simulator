package utils;

public class NumberUtils {

    public static int wrap(int num, int min, int max) {
        if (num > max) {
            return min;
        }
        if (num < min) {
            return max;
        }
        return num;
    }
}
