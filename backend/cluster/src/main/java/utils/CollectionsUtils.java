package utils;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;

public class CollectionsUtils {

    public static <T> T getLast(List<T> list) {
        if (list.size() == 0) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    public static int[] resizeArray(int[] arr, int factor) {
        int[] resized = new int[arr.length * factor];
        System.arraycopy(arr, 0, resized, 0, arr.length);
        return resized;
    }

    public static short[] resizeArray(short[] arr, int factor) {
        short[] resized = new short[arr.length * factor];
        System.arraycopy(arr, 0, resized, 0, arr.length);
        return resized;
    }

    public static double[] resizeArray(double[] arr, int factor) {
        double[] resized = new double[arr.length * factor];
        System.arraycopy(arr, 0, resized, 0, arr.length);
        return resized;
    }

    public static boolean[] resizeArray(boolean[] arr, int factor) {
        boolean[] resized = new boolean[arr.length * factor];
        System.arraycopy(arr, 0, resized, 0, arr.length);
        return resized;
    }

    public static Set<Integer> toSet(int[] arr) {
        Set<Integer> set = new HashSet<>(arr.length);
        for (int num : arr) {
            set.add(num);
        }
        return set;
    }

    public static Map<Integer, Integer> toMap(int[] keys, int[] values) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        return map;
    }

    public static Map<Integer, Short> toMap(int[] keys, short[] values) {
        Map<Integer, Short> map = new HashMap<>();
        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        return map;
    }

    public static int[] range(int stop) {
        return range(0, stop);
    }

    public static int[] range(int start, int stop) {
        int[] arr = new int[stop - start];
        for (int i = start; i < stop; i++) {
            arr[i] = i;
        }
        return arr;
    }

    public static int[] getSortedIndices(int[] arr) {
        return Arrays
                .stream(CollectionsUtils.range(arr.length))
                .boxed()
                .sorted(Comparator.comparingInt(a -> arr[a]))
                .mapToInt(i -> i)
                .toArray();
    }

    public static int[] getSortedIndices(double[] arr) {
        return Arrays
                .stream(CollectionsUtils.range(arr.length))
                .boxed()
                .sorted(Comparator.comparingDouble(a -> arr[a]))
                .mapToInt(i -> i)
                .toArray();
    }

    // https://stackoverflow.com/a/1683662
    public static void inPlaceReorder(int[] arr, int[] permutation) {
        boolean[] done = new boolean[permutation.length];
        for (int i = 0; i < permutation.length; i++) {
            if (!done[i]) {
                int t = arr[i];
                for (int j = i;;) {
                    done[j] = true;
                    if (permutation[j] != i) {
                        arr[j] = arr[permutation[j]];
                        j = permutation[j];
                    } else {
                        arr[j] = t;
                        break;
                    }
                }
            }
        }
    }

    // https://stackoverflow.com/a/1683662
    public static void inPlaceReorder(double[] arr, int[] permutation) {
        boolean[] done = new boolean[permutation.length];
        for (int i = 0; i < permutation.length; i++) {
            if (!done[i]) {
                double t = arr[i];
                for (int j = i;;) {
                    done[j] = true;
                    if (permutation[j] != i) {
                        arr[j] = arr[permutation[j]];
                        j = permutation[j];
                    } else {
                        arr[j] = t;
                        break;
                    }
                }
            }
        }
    }

    public static int[] copyArray(int[] arr) {
        int[] dest = new int[arr.length];
        System.arraycopy(arr, 0, dest, 0, arr.length);
        return dest;
    }

    public static double[] copyArray(double[] arr) {
        double[] dest = new double[arr.length];
        System.arraycopy(arr, 0, dest, 0, arr.length);
        return dest;
    }

    @SuppressWarnings("unchecked")
    public static <K, V> Map<K, V> listToMap(List<Object> list) {
        Map<K, V> map = new HashMap<>();
        for (int i = 0; i < list.size(); i+=2) {
            K key = (K)list.get(i);
            V value = (V)list.get(i + 1);
            map.put(key, value);
        }
        return map;
    }

    public static <K, V> V putInNestedConcurrentMap(
            ConcurrentMap<K, V> map,
            K key,
            V emptyValue,
            Consumer<V> onInsert
    ) {
        V previous = map.putIfAbsent(key, emptyValue);
        if (previous == null) {
            onInsert.accept(emptyValue);
            return emptyValue;
        } else {
            onInsert.accept(previous);
            return previous;
        }
    }
}
