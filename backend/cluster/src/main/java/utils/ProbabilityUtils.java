package utils;

import java.util.Random;

public class ProbabilityUtils {

    private static Random generator = new Random(); // TODO make this be an instance so it is seedable and less fragile

    public static boolean withProbability(double probability) {
        return generator.nextFloat() < probability;
    }
}
