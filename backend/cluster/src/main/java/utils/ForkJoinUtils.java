package utils;

import java.util.concurrent.ForkJoinPool;

public class ForkJoinUtils {

    public static ForkJoinPool getCommonPool() {
        return ForkJoinPool.commonPool();
    }
}
