package utils;

@SuppressWarnings("DuplicatedCode")
public class StatisticsUtils {

    public static boolean isInteger(double num) {
        return num == Math.floor(num) && !Double.isInfinite(num);
    }

    public static int getCountBeforePercentile(int arrLength, double percentile) {
        double indexFloat = (percentile / 100D) * arrLength;
        int lastIndexBefore;
        if (isInteger(indexFloat)) {
            lastIndexBefore = (int)Math.floor(indexFloat);
        } else {
            lastIndexBefore = (int)Math.ceil(indexFloat) - 2;
        }
        return Math.max(0, lastIndexBefore + 1);
    }

    public static int getPercentile(int[] sorted, double percentile) {
        double indexFloat = (percentile / 100D) * sorted.length;
        int value;
        if (isInteger(indexFloat)) {
            int indexOne = (int)Math.floor(indexFloat);
            int indexTwo = indexOne + 1;
            indexTwo = Math.min(indexTwo, sorted.length);
            value = (sorted[indexOne] + sorted[indexTwo]) / 2;
        } else {
            int index = (int)Math.ceil(indexFloat);
            index = Math.max(index, 1);
            value = sorted[index - 1];
        }
        return value;
    }

    public static double getPercentile(double[] sorted, double percentile) {
        double indexFloat = (percentile / 100D) * sorted.length;
        double value;
        if (isInteger(indexFloat)) {
            int indexOne = (int)Math.floor(indexFloat);
            int indexTwo = indexOne + 1;
            indexTwo = Math.min(indexTwo, sorted.length);
            value = (sorted[indexOne] + sorted[indexTwo]) / 2;
        } else {
            int index = (int)Math.ceil(indexFloat);
            index = Math.max(index, 1);
            value = sorted[index - 1];
        }
        return value;
    }
}
