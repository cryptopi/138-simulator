package utils;

public class LocationUtils {

    public static int getLocation(int x, int y) {
        return (x << 16) | y;
    }

    public static short getX(int location) {
        return (short)((location << 16) >> 16);
    }

    public static short getY(int location) {
        return (short)((location >> 16));
    }

    public static int addLocations(int a, int b, int bounds) {
        short x = (short) (getX(a) + getX(b));
        short y = (short) (getY(a) + getY(b));
        x = (short)NumberUtils.wrap(x, -bounds, bounds);
        y = (short)NumberUtils.wrap(y, -bounds, bounds);
        return getLocation(x, y);
    }
}
