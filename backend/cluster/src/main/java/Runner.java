import com.google.gson.GsonBuilder;
import config.*;
import config.state.*;
import config.statistic.StatConfig;
import config.statistic.TopPercentFixedWealthConfig;
import executors.simulations.SeriesSimulator;
import executors.simulations.Simulator;
import executors.statistics.StatisticsExecutor;
import executors.statistics.StatisticGrouping;
import functions.FunctionFactory;
import org.java_websocket.server.WebSocketServer;
import server.EndpointRequestHandler;
import server.parsers.RequestParserImpl;
import server.websocket.SimulationServer;
import simulations.SimulationRepository;
import tasks.callback.ThreadPoolCallbackTaskFactory;
import tasks.iteration.factories.ParallelSimulationIterationTaskFactory;
import tasks.state.combiner.ForkJoinSimulationStateCombiner;
import tasks.state.factories.ForkJoinCreateSimulationStateTaskFactory;
import tasks.statistic.combine.factories.InlineStatCombineTaskFactory;
import tasks.statistic.factories.ForkJoinStatTaskFactory;
import tasks.step.factories.ForkJoinSimulationStepTaskFactory;
import utils.ForkJoinUtils;
import utils.ThreadPoolUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class Runner {

    private static void runSimulation() {
        final int BOUNDS = 50;
        final int agentCount = 30;
        final int eventCount = 15;
        InitialSimulationStateConfig stateConfig = new InitialSimulationStateConfig(
                new NormalGeneratorConfig(0.6, 0.2),
                new IndependentLocationGeneratorConfig(
                        new UniformGeneratorConfig(-BOUNDS, BOUNDS),
                        new UniformGeneratorConfig(-BOUNDS, BOUNDS)
                ),
                new IndependentLocationGeneratorConfig(
                        new UniformGeneratorConfig(-BOUNDS, BOUNDS),
                        new UniformGeneratorConfig(-BOUNDS, BOUNDS)
                ),
                new ChoiceGeneratorConfig(
                        new double[] {-1, 1},
                        new double[] {0.5, 0.5}
                ),
                agentCount,
                eventCount,
                BOUNDS
        );

        List<StatConfig> statConfigs = new ArrayList<>();
        statConfigs.add(new TopPercentFixedWealthConfig(
                UUID.randomUUID().toString(),
                false,
                .80
        ));

        StatisticsExecutor statisticsExecutor = new StatisticsExecutor(
                statConfigs,
                new ForkJoinStatTaskFactory(),
                new ThreadPoolCallbackTaskFactory<>(),
                System.out::println,
                new InlineStatCombineTaskFactory()
        );

        SimulationConfig simulationConfig = new SimulationConfig(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                stateConfig,
                new RandomMovementConfig(1),
                null,
                null,
                new EventInteractionConfig(
                        new LinearProbabilityConfig(1, 0, 0.3, 1, "SKILL"),
                        new LinearProbabilityConfig(1, -0.7, 0, 0.2, "SKILL"),
                        new MultiplicativeEventWealthConfig(2, 0.5, false)
                ),
                80
        );

        SimulationRepository repository = new SimulationRepository();
        Simulator simulator = null;
        try {
            simulator = new SeriesSimulator(
                    new ForkJoinCreateSimulationStateTaskFactory(),
                    new ParallelSimulationIterationTaskFactory(
                            new ForkJoinSimulationStepTaskFactory(
                                    true,
                                    new FunctionFactory()
                            )
                    ),
                    new ForkJoinSimulationStateCombiner(),
                    statisticsExecutor.createSimulatorCallback(),
                    repository,
                    simulationConfig
            );
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
        simulator.simulate();
        ForkJoinUtils.getCommonPool().shutdown();
        ThreadPoolUtils.getFixedPool().shutdown();
    }

    public static void startServer() {
        WebSocketServer server = new SimulationServer(
                "localhost",
                1337,
                new EndpointRequestHandler(
                        new RequestParserImpl(new GsonBuilder()),
                        new SimulationRepository(),
                        new FunctionFactory()
                )
        );
        server.run();
    }

    public static void main(String[] args) {
//        runSimulation();
        startServer();
    }
}
