package simulations;

import config.SimulationConfig;
import simulations.state.SimulationState;

import java.util.HashMap;
import java.util.Map;

public class Simulation {

    private SimulationConfig config;
    private Map<Integer, SimulationState> simulationStates;
    private int nextIterationIndex;

    public Simulation(
            SimulationConfig config,
            SimulationState initialState
    ) {
        this.config = config;
        this.simulationStates = new HashMap<>();
        this.simulationStates.put(0, initialState);
        this.nextIterationIndex = 1;
    }

    public SimulationConfig getConfig() {
        return config;
    }

    public SimulationState getState(int iteration) {
        return simulationStates.get(iteration);
    }

    public SimulationState getLastState() {
        return getState(getNextIteration() - 1);
    }

    public int getNextIteration() {
        return nextIterationIndex;
    }

    public boolean hasNextIteration() {
        return nextIterationIndex <= config.getTotalIterationCount();
    }

    public void incrementIteration() {
        nextIterationIndex++;
    }

    public void setStepState(int iteration, SimulationState state) {
        simulationStates.put(iteration, state);
    }
}
