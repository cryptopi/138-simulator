package simulations;

import utils.CollectionsUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

public class SimulationRepository {

    private ConcurrentMap<String, Simulation> simulationsById;
    private ConcurrentMap<String, ConcurrentLinkedQueue<Simulation>> simulationsByGroup;
    private ConcurrentMap<String, ConcurrentLinkedQueue<Simulation>> simulationsByJob;

    public SimulationRepository() {
        this.simulationsById = new ConcurrentHashMap<>();
        this.simulationsByGroup = new ConcurrentHashMap<>();
        this.simulationsByJob = new ConcurrentHashMap<>();
    }

    public void addSimulation(Simulation simulation) {
        simulationsById.put(simulation.getConfig().getId(), simulation);
        CollectionsUtils.putInNestedConcurrentMap(
                simulationsByGroup,
                simulation.getConfig().getGroupKey(),
                new ConcurrentLinkedQueue<>(),
                queue -> queue.add(simulation)
        );
        CollectionsUtils.putInNestedConcurrentMap(
                simulationsByJob,
                simulation.getConfig().getJobId(),
                new ConcurrentLinkedQueue<>(),
                queue -> queue.add(simulation)
        );
    }

    public Simulation getSimulationById(String id) {
        return simulationsById.get(id);
    }

    public Collection<Simulation> getSimulationsByGroup(String groupId) {
        return simulationsByGroup.get(groupId);
    }

    public Collection<Simulation> getSimulationsByJob(String jobId) {
        return simulationsByJob.get(jobId);
    }

}
