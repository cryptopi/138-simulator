package simulations.state.updates;

import simulations.state.SimulationState;

public class ReplaceState extends StateUpdate {

    private SimulationState state;

    public ReplaceState(SimulationState state) {
        super(StateUpdateType.REPLACE, false);
        this.state = state;
    }

    public SimulationState getState() {
        return state;
    }
}
