package simulations.state.updates;

import utils.CollectionsUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StateUpdateGroup {

    private List<GenerationUpdate> generationUpdates;
    private List<SkillUpdate> skillUpdates;
    private List<WealthUpdate> wealthUpdates;
    private List<AgentLocationUpdate> agentLocationUpdates;
    private List<EventLocationUpdate> eventLocationUpdates;
    private List<ActiveUpdate> activeUpdates;
    private ReplaceState replaceUpdate;
    private boolean incremental;

    public StateUpdateGroup(StateUpdate ... updates) {
        this(Arrays.asList(updates));
    }

    public StateUpdateGroup(List<StateUpdate> updates) {
        this.generationUpdates = new ArrayList<>();
        this.skillUpdates = new ArrayList<>();
        this.wealthUpdates = new ArrayList<>();
        this.agentLocationUpdates = new ArrayList<>();
        this.eventLocationUpdates = new ArrayList<>();
        this.activeUpdates = new ArrayList<>();
        this.replaceUpdate = null;
        populateMembers(updates);
    }

    private void populateMembers(List<StateUpdate> updates) {
        boolean first = true;
        for (StateUpdate update : updates) {
            if (!first && incremental != update.isIncremental()) {
                throw new IllegalStateUpdateSequence(
                        "All updates must have same incremental value"
                );
            }
            incremental = update.isIncremental();
            switch (update.getType()) {
                case GENERATION:
                    generationUpdates.add((GenerationUpdate)update);
                    break;
                case SKILL:
                    skillUpdates.add((SkillUpdate)update);
                    break;
                case WEALTH:
                    wealthUpdates.add((WealthUpdate)update);
                    break;
                case AGENT_LOCATION:
                    agentLocationUpdates.add((AgentLocationUpdate)update);
                    break;
                case EVENT_LOCATION:
                    eventLocationUpdates.add((EventLocationUpdate)update);
                    break;
                case ACTIVE:
                    activeUpdates.add((ActiveUpdate)update);
                    break;
                case REPLACE:
                    if (replaceUpdate != null) {
                        throw new IllegalStateUpdateSequence(
                                "Multiple replace updates present"
                        );
                    }
                    replaceUpdate = (ReplaceState)update;
                    break;
                default:
                    throw new IllegalArgumentException("State Update " +
                            update + " is not recognized");
            }
            first = false;
        }
    }

    public boolean hasReplaceUpdate() {
        return getReplaceUpdate() != null;
    }

    public boolean isIncremental() {
        return incremental;
    }

    public List<GenerationUpdate> getGenerationUpdates() {
        return generationUpdates;
    }

    public List<SkillUpdate> getSkillUpdates() {
        return skillUpdates;
    }

    public List<WealthUpdate> getWealthUpdates() {
        return wealthUpdates;
    }

    public List<AgentLocationUpdate> getAgentLocationUpdates() {
        return agentLocationUpdates;
    }

    public List<EventLocationUpdate> getEventLocationUpdates() {
        return eventLocationUpdates;
    }

    public List<ActiveUpdate> getActiveUpdates() {
        return activeUpdates;
    }

    public GenerationUpdate getLastGenerationUpdate() {
        return CollectionsUtils.getLast(getGenerationUpdates());
    }

    public SkillUpdate getLastSkillUpdate() {
        return CollectionsUtils.getLast(getSkillUpdates());
    }

    public WealthUpdate getLastWealthUpdate() {
        return CollectionsUtils.getLast(getWealthUpdates());
    }

    public AgentLocationUpdate getLastAgentLocationUpdate() {
        return CollectionsUtils.getLast(getAgentLocationUpdates());
    }

    public EventLocationUpdate getLastEventLocationUpdate() {
        return CollectionsUtils.getLast(getEventLocationUpdates());
    }

    public ActiveUpdate getLastActiveUpdate() {
        return CollectionsUtils.getLast(getActiveUpdates());
    }

    public ReplaceState getReplaceUpdate() {
        return replaceUpdate;
    }
}
