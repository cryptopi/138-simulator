package simulations.state.updates;

public class AgentLocationUpdate extends StateUpdate {

    private int[] locations;

    public AgentLocationUpdate(boolean incremental, int[] locations) {
        super(StateUpdateType.AGENT_LOCATION, incremental);
        this.locations = locations;
    }

    public int[] getLocations() {
        return locations;
    }
}
