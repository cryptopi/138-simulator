package simulations.state.updates;

public class GenerationUpdate extends StateUpdate {

    private short[] generations;
    private int[] parents;
    private double[] skills;
    private double[] wealths;
    private int[] locations;
    private boolean[] areActive;

    public GenerationUpdate(
            boolean incremental,
            short[] generations,
            int[] parents,
            double[] skills,
            double[] wealths,
            int[] locations,
            boolean[] areActive
    ) {
        super(StateUpdateType.GENERATION, incremental);
        this.generations = generations;
        this.parents = parents;
        this.skills = skills;
        this.wealths = wealths;
        this.locations = locations;
        this.areActive = areActive;
    }

    public short[] getGenerations() {
        return generations;
    }

    public int[] getParents() {
        return parents;
    }

    public double[] getSkills() {
        return skills;
    }

    public double[] getWealths() {
        return wealths;
    }

    public int[] getLocations() {
        return locations;
    }

    public boolean[] getAreActive() {
        return areActive;
    }
}
