package simulations.state.updates;

public class WealthUpdate extends StateUpdate {

    private double[] wealths;

    public WealthUpdate(boolean incremental, double[] wealths) {
        super(StateUpdateType.WEALTH, incremental);
        this.wealths = wealths;
    }

    public double[] getWealths() {
        return wealths;
    }
}
