package simulations.state.updates;

public enum StateUpdateType {

    GENERATION,
    SKILL,
    WEALTH,
    AGENT_LOCATION,
    EVENT_LOCATION,
    ACTIVE,
    REPLACE
}
