package simulations.state.updates;

public class EventLocationUpdate extends StateUpdate {

    private int[] locations;

    public EventLocationUpdate(boolean incremental, int[] locations) {
        super(StateUpdateType.AGENT_LOCATION, incremental);
        this.locations = locations;
    }

    public int[] getLocations() {
        return locations;
    }
}
