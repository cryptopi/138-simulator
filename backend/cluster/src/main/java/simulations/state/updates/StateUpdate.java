package simulations.state.updates;

public abstract class StateUpdate {

    private StateUpdateType type;
    private boolean incremental;

    public StateUpdate(StateUpdateType type, boolean incremental) {
        this.type = type;
        this.incremental = incremental;
    }

    public StateUpdateType getType() {
        return type;
    }

    public boolean isIncremental() {
        return incremental;
    }
}
