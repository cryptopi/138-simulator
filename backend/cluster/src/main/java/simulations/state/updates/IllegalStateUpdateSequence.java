package simulations.state.updates;

public class IllegalStateUpdateSequence extends Error {

    public IllegalStateUpdateSequence() {
        super();
    }

    public IllegalStateUpdateSequence(String message) {
        super(message);
    }

    public IllegalStateUpdateSequence(String message, Exception ex) {
        super(message, ex);
    }
}
