package simulations.state.updates;

public class ActiveUpdate extends StateUpdate {

    private boolean[] areActive;

    public ActiveUpdate(boolean incremental, boolean[] areActive) {
        super(StateUpdateType.ACTIVE, incremental);
        this.areActive = areActive;
    }

    public boolean[] getAreActive() {
        return areActive;
    }
}
