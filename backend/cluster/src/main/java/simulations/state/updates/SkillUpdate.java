package simulations.state.updates;

public class SkillUpdate extends StateUpdate {

    private double[] skills;

    public SkillUpdate(boolean incremental, double[] skills) {
        super(StateUpdateType.SKILL, incremental);
        this.skills = skills;
    }

    public double[] getSkills() {
        return skills;
    }
}
