package simulations.state;

/**
 * TYPES:
 *  Generation: short
 *  X, Y coordinates: short
 *  Index of agents: int
 *  Index of events: int
 */

import utils.CollectionsUtils;

import java.util.Arrays;

/**
 * RESTRICTIONS:
 *  Max 32.7k generations
 *  Max 32.7k edge width grid (technically twice this, use negative indexing)
 *  2 billion agents
 *  2 billion events
 */

// NOTE ALL ARRAYS MUST BE SAME LENGTH

public class SimulationState {

    private static final int RESIZE_FACTOR = 2;

    public short[] generations;
    public int[] parents; // Twice the length
    public double[] skills;
    public double[] wealths;
    public int[] agentLocations; // Same length
    public int[] eventLocations; // Same length
    public short[] eventMagnitudes;
    public boolean[] areActive; // TODO perf replace with bitset?
    private int agentCount;
    private int eventCount;
    private int gridBounds;
    private int maxAgentCapacity;

    public SimulationState() {
        this(null, null, null, null, null, null, null, null, 0);
    }

    public SimulationState(int agentCount, int eventCount, int gridBounds) {
        this(
                new short[agentCount],
                new int[agentCount],
                new double[agentCount],
                new double[agentCount],
                new int[agentCount],
                new int[eventCount],
                new short[eventCount],
                new boolean[agentCount],
                gridBounds
        );
    }

    public SimulationState(
            short[] generations,
            int[] parents,
            double[] skills,
            double[] wealths,
            int[] agentLocations,
            int[] eventLocations,
            short[] eventMagnitudes,
            boolean[] areActive,
            int gridBounds
    ) {
        this.generations = generations;
        this.parents = parents;
        this.skills = skills;
        this.wealths = wealths;
        this.agentLocations = agentLocations;
        this.eventLocations = eventLocations;
        this.eventMagnitudes = eventMagnitudes;
        this.areActive = areActive;
        this.agentCount = agentLocations == null ? 0 : agentLocations.length;
        this.eventCount = eventLocations == null ? 0 : eventLocations.length;
        this.gridBounds = gridBounds;
        this.maxAgentCapacity = this.agentCount;
    }

    private void increaseAgentCapacity() {
        generations = CollectionsUtils.resizeArray(generations, RESIZE_FACTOR);
        parents = CollectionsUtils.resizeArray(parents, RESIZE_FACTOR);
        skills = CollectionsUtils.resizeArray(skills, RESIZE_FACTOR);
        wealths = CollectionsUtils.resizeArray(wealths, RESIZE_FACTOR);
        agentLocations = CollectionsUtils.resizeArray(agentLocations, RESIZE_FACTOR);
        areActive = CollectionsUtils.resizeArray(areActive, RESIZE_FACTOR);
        maxAgentCapacity *= RESIZE_FACTOR;
    }

    public void appendData(
            short[] generations,
            int[] parents,
            double[] skills,
            double[] wealths,
            int[] agentLocations,
            boolean[] areActive
    ) {
        int additionalCount = agentLocations.length;
        while (maxAgentCapacity - agentCount < additionalCount) {
            increaseAgentCapacity();
        }
        System.arraycopy(generations, 0, this.generations, agentCount, generations.length);
        System.arraycopy(parents, 0, this.parents, agentCount, parents.length);
        System.arraycopy(skills, 0, this.skills, agentCount, skills.length);
        System.arraycopy(wealths, 0, this.wealths, agentCount, wealths.length);
        System.arraycopy(agentLocations, 0, this.agentLocations, agentCount, agentLocations.length);
        System.arraycopy(areActive, 0, this.areActive, agentCount, areActive.length);
        agentCount += additionalCount;
    }

    public int getAgentCount() {
        return agentCount;
    }

    public int getEventCount() {
        return eventCount;
    }

    public int getGridBounds() {
        return gridBounds;
    }

    public short[] getGenerations() {
        return generations;
    }

    public int[] getParents() {
        return parents;
    }

    public double[] getSkills() {
        return skills;
    }

    public double[] getWealths() {
        return wealths;
    }

    public int[] getAgentLocations() {
        return agentLocations;
    }

    public int[] getEventLocations() {
        return eventLocations;
    }

    public short[] getEventMagnitudes() {
        return eventMagnitudes;
    }

    public boolean[] getAreActive() {
        return areActive;
    }

    public short getGeneration(int index) {
        return generations[index];
    }

    public double getSkill(int index) {
        return skills[index];
    }

    public int getAgentLocation(int index) {
        return agentLocations[index];
    }

    public int getEventLocation(int index) {
        return eventLocations[index];
    }

    public boolean isActive(int index) {
        return areActive[index];
    }

    public void setGenerations(short[] generations) {
        this.generations = generations;
    }

    public void setParents(int[] parents) {
        this.parents = parents;
    }

    public void setSkills(double[] skills) {
        this.skills = skills;
    }

    public void setWealths(double[] wealths) {
        this.wealths = wealths;
    }

    public void setAgentLocations(int[] agentLocations) {
        this.agentLocations = agentLocations;
    }

    public void setEventLocations(int[] eventLocations) {
        this.eventLocations = eventLocations;
    }

    public void setAreActive(boolean[] areActive) {
        this.areActive = areActive;
    }

    private int getAncestorOneIndex(int index, int distance) {
        int ancestorIndex = index;
        for (int i = 0; i < distance; i++) {
            ancestorIndex = parents[ancestorIndex * 2];
        }
        return ancestorIndex;
    }

    private int getAncestorTwoIndex(int index, int distance) {
        int ancestorIndex = index;
        for (int i = 0; i < distance; i++) {
            ancestorIndex = parents[ancestorIndex * 2 + 1];
        }
        return ancestorIndex;
    }

    public short getAncestorOneGeneration(int index, int distance) {
        return getGeneration(getAncestorOneIndex(index, distance));
    }

    public short getAncestorTwoGeneration(int index, int distance) {
        return getGeneration(getAncestorTwoIndex(index, distance));
    }

    public double getAncestorOneSkill(int index, int distance) {
        return getSkill(getAncestorOneIndex(index, distance));
    }

    public double getAncestorTwoSkill(int index, int distance) {
        return getSkill(getAncestorTwoIndex(index, distance));
    }

    public int getAncestorOneLocation(int index, int distance) {
        return getAgentLocation(getAncestorOneIndex(index, distance));
    }

    public int getAncestorTwoLocation(int index, int distance) {
        return getAgentLocation(getAncestorTwoIndex(index, distance));
    }

    public boolean isAncestorOneActive(int index, int distance) {
        return isActive(getAncestorOneIndex(index, distance));
    }

    public boolean isAncestorTwoActive(int index, int distance) {
        return isActive(getAncestorTwoIndex(index, distance));
    }

    public SimulationState copy() {
        SimulationState copy = new SimulationState(agentCount, eventCount, gridBounds);
        System.arraycopy(generations, 0, copy.generations, 0, generations.length);
        System.arraycopy(parents, 0, copy.parents, 0, parents.length);
        System.arraycopy(skills, 0, copy.skills, 0, skills.length);
        System.arraycopy(wealths, 0, copy.wealths, 0, wealths.length);
        System.arraycopy(agentLocations, 0, copy.agentLocations, 0, agentLocations.length);
        System.arraycopy(eventLocations, 0, copy.eventLocations, 0, eventLocations.length);
        System.arraycopy(eventMagnitudes, 0, copy.eventMagnitudes, 0, eventMagnitudes.length);
        System.arraycopy(areActive, 0, copy.areActive, 0, areActive.length);

        copy.agentCount = agentCount;
        copy.eventCount = eventCount;
        copy.gridBounds = gridBounds;
        copy.maxAgentCapacity = maxAgentCapacity;
        return copy;
    }
}
