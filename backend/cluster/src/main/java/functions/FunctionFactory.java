package functions;

import config.EventWealthConfig;
import config.LinearProbabilityConfig;
import config.MultiplicativeEventWealthConfig;
import config.NumberConfig;

public class FunctionFactory {

    public AgentEventFunction createNumericFunction(NumberConfig config) {
        if (config.getClass().equals(LinearProbabilityConfig.class)) {
            LinearProbabilityConfig linearConfig = (LinearProbabilityConfig)config;
            int wealth = linearConfig.getVariable().equals("WEALTH") ? 1 : 0;
            int skill = linearConfig.getVariable().equals("SKILL") ? 1 : 0;
            return new LinearAgentFunction(
                    wealth * linearConfig.getMultiplier(),
                    skill * linearConfig.getMultiplier(),
                    linearConfig.getOffset(),
                    linearConfig.getMin(),
                    linearConfig.getMax()
            );
        }
        throw new IllegalArgumentException(
                "NumberConfig " + config + " is not recognized"
        );
    }

    public AgentEventFunction createEventWealthFunction(EventWealthConfig config) {
        if (config.getClass().equals(MultiplicativeEventWealthConfig.class)) {
            MultiplicativeEventWealthConfig eventConfig = (MultiplicativeEventWealthConfig)config;
            return (skill, wealth, eventMagnitude) -> {
                boolean lucky = eventMagnitude > 0;
                double factor =
                        lucky ? eventConfig.getLuckyFactor() : eventConfig.getUnluckyFactor();
                double magnitudeFactor =
                        Math.abs(eventConfig.includeEventMagnitude() ? eventMagnitude : 1);
                if (!lucky) {
                    magnitudeFactor = 1 / magnitudeFactor;
                }
                return wealth * factor * magnitudeFactor;
            };
        }
        throw new IllegalArgumentException(
                "EventWealthConfig " + config + " is not recognized"
        );
    }
}
