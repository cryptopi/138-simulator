package functions;

public interface AgentEventFunction {

    double compute(double skill, double wealth, double eventMagnitude);
}
