package functions;

public class LinearAgentFunction implements AgentFunction, AgentEventFunction {

    private double wealthSlope;
    private double skillSlope;
    private double intercept;
    private double min;
    private double max;

    public LinearAgentFunction(
            double wealthSlope,
            double skillSlope,
            double intercept,
            double min,
            double max
    ) {
        this.wealthSlope = wealthSlope;
        this.skillSlope = skillSlope;
        this.intercept = intercept;
        this.min = min;
        this.max = max;
    }

    @Override
    public double compute(double skill, double wealth, double eventMagnitude) {
        return compute(skill, wealth);
    }

    @Override
    public double compute(double skill, double wealth) {
        double value = skillSlope * skill + wealthSlope * wealth + intercept;
        value = Math.max(value, min);
        value = Math.min(value, max);
        return value;
    }

}
