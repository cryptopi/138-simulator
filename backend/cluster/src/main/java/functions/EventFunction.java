package functions;

public interface EventFunction {

    double compute(double magnitude);
}
