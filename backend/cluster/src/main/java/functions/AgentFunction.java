package functions;

public interface AgentFunction {

    double compute(double skill, double wealth);
}
