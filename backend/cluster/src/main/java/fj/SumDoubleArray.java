package fj;

import fj.base.SplittingForkJoinTaskWithReturn;

public class SumDoubleArray extends SplittingForkJoinTaskWithReturn<double[], Double> {

    public SumDoubleArray(double[] value) {
        this(value, 0, value.length);
    }

    public SumDoubleArray(
            double[] value,
            int start,
            int end
    ) {
        super(value, start, end, Double::sum);
    }

    @Override
    protected Double computeDirectly() {
        double sum = 0;
        double[] values = getValue();
        for (int i = getStart(); i < getEnd(); i++) {
            sum += values[i];
        }
        return sum;
    }

    @Override
    protected SplittingForkJoinTaskWithReturn<double[], Double> split(int start, int end) {
        return new SumDoubleArray(getValue(), start, end);
    }
}
