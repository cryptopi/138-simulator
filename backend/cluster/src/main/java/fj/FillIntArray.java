package fj;

import fj.base.SplittingForkJoinTaskNoReturn;
import mappers.IntToInt;
import suppliers.HighOrderSupplier;
import suppliers.sint.IntSupplier;

import java.util.function.Supplier;

public class FillIntArray extends SplittingForkJoinTaskNoReturn<int[]> {

    private Supplier<IntToInt> supplier;

    public FillIntArray(int[] value, Supplier<IntToInt> supplier) {
        this(0, value.length, value, supplier);
    }

    public FillIntArray(
            int start,
            int end,
            int[] value,
            Supplier<IntToInt> supplier
    ) {
        super(start, end, value);
        this.supplier = supplier;
    }

    public static FillIntArray createWithIndependentSupplier(
            int[] value,
            Supplier<IntSupplier> supplier
    ) {
        return new FillIntArray(
                value,
                new HighOrderSupplier<>(supplier, IntToInt::fromSupplier)
        );
    }

    @Override
    protected void computeDirectly() {
        IntToInt valueSupplier = supplier.get();
        for (int i = getStart(); i < getEnd(); i++) {
            getValue()[i] = valueSupplier.supply(i);
        }
    }

    @Override
    protected SplittingForkJoinTaskNoReturn<int[]> split(int start, int end) {
        return new FillIntArray(start, end, getValue(), supplier);
    }
}
