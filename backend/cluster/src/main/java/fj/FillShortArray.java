package fj;

import fj.base.SplittingForkJoinTaskNoReturn;
import mappers.IntToShort;
import suppliers.HighOrderSupplier;
import suppliers.sshort.ShortSupplier;
import mappers.IntToInt;

import java.util.function.Supplier;

public class FillShortArray extends SplittingForkJoinTaskNoReturn<short[]> {

    private Supplier<IntToShort> supplier;

    public FillShortArray(short[] value, Supplier<IntToShort> supplier) {
        this(0, value.length, value, supplier);
    }

    public FillShortArray(
            int start,
            int end,
            short[] value,
            Supplier<IntToShort> supplier
    ) {
        super(start, end, value);
        this.supplier = supplier;
    }

    public static FillShortArray createWithIndependentSupplier(
            short[] value,
            Supplier<ShortSupplier> supplier
    ) {
        return new FillShortArray(
                value,
                new HighOrderSupplier<>(supplier, IntToShort::fromSupplier)
        );
    }

    @Override
    protected void computeDirectly() {
        IntToShort valueSupplier = supplier.get();
        for (int i = getStart(); i < getEnd(); i++) {
            getValue()[i] = valueSupplier.supply(i);
        }
    }

    @Override
    protected SplittingForkJoinTaskNoReturn<short[]> split(int start, int end) {
        return new FillShortArray(start, end, getValue(), supplier);
    }
}
