package fj;

import fj.base.SplittingForkJoinTaskNoReturn;
import mappers.IntToDouble;
import suppliers.sdouble.DoubleSupplier;
import suppliers.HighOrderSupplier;

import java.util.function.Supplier;

public class FillDoubleArray extends SplittingForkJoinTaskNoReturn<double[]> {

    private Supplier<IntToDouble> supplier;

    public FillDoubleArray(double[] value, Supplier<IntToDouble> supplier) {
        this(0, value.length, value, supplier);
    }

    public FillDoubleArray(
            int start,
            int end,
            double[] value,
            Supplier<IntToDouble> supplier
    ) {
        super(start, end, value);
        this.supplier = supplier;
    }

    public static FillDoubleArray createWithIndependentSupplier(
            double[] value,
            Supplier<DoubleSupplier> supplier
    ) {
        return new FillDoubleArray(
                value,
                new HighOrderSupplier<>(supplier, IntToDouble::fromSupplier)
        );
    }

    @Override
    protected void computeDirectly() {
        IntToDouble valueSupplier = supplier.get();
        for (int i = getStart(); i < getEnd(); i++) {
            getValue()[i] = valueSupplier.supply(i);
        }
    }

    @Override
    protected SplittingForkJoinTaskNoReturn<double[]> split(int start, int end) {
        return new FillDoubleArray(start, end, getValue(), supplier);
    }
}
