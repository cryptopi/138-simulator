package fj.base;

import java.util.concurrent.RecursiveTask;
import java.util.function.BiFunction;

public abstract class SplittingForkJoinTaskWithReturn<V, T> extends RecursiveTask<T> {

    private V value;
    private int start;
    private int end;
    private BiFunction<T, T, T> combiner;

    public SplittingForkJoinTaskWithReturn(
            V value,
            int start,
            int end,
            BiFunction<T, T, T> combiner
    ) {
        this.value = value;
        this.start = start;
        this.end = end;
        this.combiner = combiner;
    }

    public V getValue() {
        return value;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    protected int getSplitLength() {
        return 100_000;
    }

    protected int getMaxSurplusQueuedTaskCount() {
        return 5;
    }

    protected boolean shouldSplit() {
        return end - start > getSplitLength()
                && getSurplusQueuedTaskCount() < getMaxSurplusQueuedTaskCount();
    }

    protected abstract T computeDirectly();

    protected abstract SplittingForkJoinTaskWithReturn<V, T> split(int start, int end);

    protected void init() {}

    @Override
    protected T compute() {
        init();
        if (shouldSplit()) {
            int midpoint = (start + end) / 2;
            final SplittingForkJoinTaskWithReturn<V, T> one = split(0, midpoint);
            final SplittingForkJoinTaskWithReturn<V, T> two = split(midpoint + 1, end);
            one.fork();
            T valueTwo = two.compute();
            T valueOne = one.join();
            return combiner.apply(valueOne, valueTwo);
        } else {
            return computeDirectly();
        }
    }
}
