package fj.base;

import java.util.concurrent.RecursiveTask;

public abstract class SplittingForkJoinTaskNoReturn<T> extends RecursiveTask<T> {

    private int start;
    private int end;
    private T value;

    public SplittingForkJoinTaskNoReturn(
            int start,
            int end,
            T value
    ) {
        this.start = start;
        this.end = end;
        this.value = value;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public T getValue() {
        return value;
    }

    protected int getSplitLength() {
        return 100_000;
    }

    protected int getMaxSurplusQueuedTaskCount() {
        return 5;
    }

    protected boolean shouldSplit() {
        return end - start > getSplitLength()
                && getSurplusQueuedTaskCount() < getMaxSurplusQueuedTaskCount();
    }

    protected abstract void computeDirectly();

    protected void init() {}

    protected abstract SplittingForkJoinTaskNoReturn<T> split(int start, int end);

    @Override
    protected T compute() {
        init();
        if (shouldSplit()) {
            int midpoint = (start + end) / 2;
            final SplittingForkJoinTaskNoReturn<T> one = split(0, midpoint);
            final SplittingForkJoinTaskNoReturn<T> two = split(midpoint + 1, end);
            one.fork();
            two.compute();
            one.join();
        } else {
            computeDirectly();
        }
        return value;
    }
}
