package fj.base;

import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

public abstract class SplittingForkJoinAction<T> extends RecursiveAction {

    private int start;
    private int end;
    private T value;

    public SplittingForkJoinAction(
            int start,
            int end,
            T value
    ) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public T getValue() {
        return value;
    }

    protected int getSplitLength() {
        return 100_000;
    }

    protected int getMaxSurplusQueuedTaskCount() {
        return 5;
    }

    protected boolean shouldSplit() {
        return end - start > getSplitLength()
                && getSurplusQueuedTaskCount() < getMaxSurplusQueuedTaskCount();
    }

    protected abstract void computeDirectly();

    protected void init() {}

    protected abstract SplittingForkJoinAction<T> split(int start, int end);

    @Override
    protected void compute() {
        init();
        if (shouldSplit()) {
            int midpoint = (start + end) / 2;
            final SplittingForkJoinAction<T> one = split(0, midpoint);
            final SplittingForkJoinAction<T> two = split(midpoint + 1, end);
            one.fork();
            two.compute();
            one.join();
        } else {
            computeDirectly();
        }
    }
}
