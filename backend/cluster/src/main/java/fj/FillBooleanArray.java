package fj;

import fj.base.SplittingForkJoinTaskNoReturn;
import suppliers.sboolean.BooleanSupplier;
import suppliers.HighOrderSupplier;
import mappers.IntToBoolean;

import java.util.function.Supplier;

public class FillBooleanArray extends SplittingForkJoinTaskNoReturn<boolean[]> {

    private Supplier<IntToBoolean> supplier;

    public FillBooleanArray(
            boolean[] value,
            Supplier<IntToBoolean> supplier
    ) {
        this(0, value.length, value, supplier);
    }

    public static FillBooleanArray createWithIndependentSupplier(
            boolean[] value,
            Supplier<BooleanSupplier> supplier
    ) {
        return new FillBooleanArray(
                value,
                new HighOrderSupplier<>(supplier, IntToBoolean::fromSupplier)
        );
    }

    public FillBooleanArray(
            int start,
            int end,
            boolean[] value,
            Supplier<IntToBoolean> supplier
    ) {
        super(start, end, value);
        this.supplier = supplier;
    }

    @Override
    protected void computeDirectly() {
        IntToBoolean valueSupplier = supplier.get();
        for (int i = getStart(); i < getEnd(); i++) {
            getValue()[i] = valueSupplier.supply(i);
        }
    }

    @Override
    protected SplittingForkJoinTaskNoReturn<boolean[]> split(int start, int end) {
        return new FillBooleanArray(start, end, getValue(), supplier);
    }
}
