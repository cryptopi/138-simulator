package fj;

import tasks.Task;
import utils.ForkJoinUtils;

import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.Future;

public class ForkJoinTaskExecutor<T> implements Task<T> {

    private ForkJoinTask<T> task;

    public ForkJoinTaskExecutor(ForkJoinTask<T> task) {
        this.task = task;
    }

    @Override
    public Future<T> compute() {
        return ForkJoinUtils.getCommonPool().submit(task);
    }
}
