package fj;

import fj.base.SplittingForkJoinTaskWithReturn;

public class SumIntArray extends SplittingForkJoinTaskWithReturn<int[], Integer> {

    public SumIntArray(int[] value) {
        this(value, 0, value.length);
    }

    public SumIntArray(
            int[] value,
            int start,
            int end
    ) {
        super(value, start, end, Integer::sum);
    }

    @Override
    protected Integer computeDirectly() {
        int sum = 0;
        int[] value = getValue();
        for (int i = getStart(); i < getEnd(); i++) {
            sum += value[i];
        }
        return sum;
    }

    @Override
    protected SplittingForkJoinTaskWithReturn<int[], Integer> split(int start, int end) {
        return new SumIntArray(getValue(), start, end);
    }
}
