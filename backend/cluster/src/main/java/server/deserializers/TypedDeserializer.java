package server.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import config.state.GeneratorConfig;
import server.exceptions.IllegalRequest;
import utils.CollectionsUtils;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

public class TypedDeserializer<T> implements JsonDeserializer<T> {

    private Map<String, Class<? extends T>> classMap;

    public TypedDeserializer(Object ... classMapList) {
        this.classMap = CollectionsUtils.listToMap(Arrays.asList(classMapList));
    }

    @Override
    public T deserialize(
            JsonElement jsonElement,
            Type type,
            JsonDeserializationContext context
    ) throws JsonParseException {
        if (jsonElement.isJsonNull()) {
            return null;
        }
        JsonElement typeElement = jsonElement.getAsJsonObject().get("type");
        if (typeElement == null) {
            throw new JsonParseException(
                    "ClassMap with keys " + classMap.keySet() + " missing type info"
            );
        }
        String stringType =
                jsonElement.getAsJsonObject().get("type").getAsString();
        if (classMap.containsKey(stringType)) {
            return context.deserialize(jsonElement, classMap.get(stringType));
        }
        throw new IllegalArgumentException(
                "Type " + stringType + " is not recognized"
        );
    }
}
