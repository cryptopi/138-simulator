package server.parsers;

import server.requests.Request;

public interface RequestParser {

    Request parse(String message);
}
