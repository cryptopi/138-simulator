package server.parsers;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import config.*;
import config.state.*;
import config.statistic.StatConfig;
import config.statistic.TopPercentFixedWealthConfig;
import config.statistic.combine.CollectStatCombineConfig;
import config.statistic.combine.MeanStatCombineConfig;
import config.statistic.combine.NoCombineConfig;
import config.statistic.combine.StatCombinerConfig;
import net.dongliu.gson.GsonJava8TypeAdapterFactory;
import server.requests.FullRequest;
import server.requests.Request;
import server.deserializers.TypedDeserializer;
import server.requests.StatisticsRequest;

public class RequestParserImpl implements RequestParser {

    private Gson gson;

    public RequestParserImpl(GsonBuilder builder) {
        setDeserializers(builder);
    }

    private void setDeserializers(GsonBuilder builder) {
        builder.registerTypeAdapter(Request.class, new TypedDeserializer<Request>(
                "FULL", FullRequest.class,
                "STATS", StatisticsRequest.class
        ));
        builder.registerTypeAdapter(GeneratorConfig.class, new TypedDeserializer<GeneratorConfig>(
                "NORMAL", NormalGeneratorConfig.class,
                "UNIFORM", UniformGeneratorConfig.class,
                "CHOICE", ChoiceGeneratorConfig.class
        ));
        builder.registerTypeAdapter(LocationGeneratorConfig.class, new TypedDeserializer<LocationGeneratorConfig>(
                "INDEPENDENT", IndependentLocationGeneratorConfig.class
        ));
        builder.registerTypeAdapter(AgentMovementConfig.class, new TypedDeserializer<AgentMovementConfig>(
                "RANDOM", RandomMovementConfig.class
        ));
        builder.registerTypeAdapter(EventMovementConfig.class, new TypedDeserializer<EventMovementConfig>(
                "RANDOM", RandomMovementConfig.class
        ));
        builder.registerTypeAdapter(MovementConfig.class, new TypedDeserializer<EventMovementConfig>(
                "RANDOM", RandomMovementConfig.class
        ));
        builder.registerTypeAdapter(RedistributionConfig.class, new TypedDeserializer<RedistributionConfig>(
                "GENERAL", GeneralWealthRedistributionConfig.class
        ));
        builder.registerTypeAdapter(StatConfig.class, new TypedDeserializer<StatConfig>(
                "TOP_PERCENT_FIXED_WEALTH", TopPercentFixedWealthConfig.class
        ));
        builder.registerTypeAdapter(EventWealthConfig.class, new TypedDeserializer<EventWealthConfig>(
                "MULTIPLICATIVE", MultiplicativeEventWealthConfig.class
        ));
        builder.registerTypeAdapter(NumberConfig.class, new TypedDeserializer<NumberConfig>(
                "LINEAR", LinearProbabilityConfig.class
        ));
        builder.registerTypeAdapter(StatCombinerConfig.class, new TypedDeserializer<StatCombinerConfig>(
                "NONE", NoCombineConfig.class,
                "COLLECT", CollectStatCombineConfig.class,
                "MEAN", MeanStatCombineConfig.class
        ));
        builder.registerTypeAdapterFactory(new GsonJava8TypeAdapterFactory());
        builder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
        builder.serializeNulls();
        gson = builder.create();
    }

    @Override
    public Request parse(String message) {
        Request request = gson.fromJson(message, Request.class);
//        if (request instanceof FullRequest) {
//            for (SimulationConfig config : ((FullRequest)request).getSimulations()) {
//                config.setJobId(request.getId());
//            }
//        }
        System.out.println(request);
        System.out.println("Done parsing");
        return request;
    }
}
