package server;

public class ExecutionPreferences {

    private ParallelOrSeries simulator;
    private ParallelType createSimulationState;
    private ParallelOrSeries simulationIteration;
    private ParallelType simulationSteps;
    private ParallelType combineStates;
    private ParallelType statisticsTasks;
    private ParallelType statisticsCallback;
    private boolean incrementalUpdates;
    private boolean useRealState;

    private ExecutionPreferences() {}

    public ParallelOrSeries getSimulator() {
        return simulator;
    }

    public ParallelType getCreateSimulationState() {
        return createSimulationState;
    }

    public ParallelOrSeries getSimulationIteration() {
        return simulationIteration;
    }

    public ParallelType getSimulationSteps() {
        return simulationSteps;
    }

    public ParallelType getCombineStates() {
        return combineStates;
    }

    public ParallelType getStatisticsTasks() {
        return statisticsTasks;
    }

    public ParallelType getStatisticsCallback() {
        return statisticsCallback;
    }

    public boolean shouldUseIncrementalUpdates() {
        return incrementalUpdates;
    }

    public boolean shouldUseRealState() {
        return useRealState;
    }

    @Override
    public String toString() {
        return "ExecutionPreferences{" +
                "simulator=" + simulator +
                ", createSimulationState=" + createSimulationState +
                ", simulationIteration=" + simulationIteration +
                ", simulationSteps=" + simulationSteps +
                ", combineStates=" + combineStates +
                ", statisticsTasks=" + statisticsTasks +
                ", statisticsCallback=" + statisticsCallback +
                ", incrementalUpdates=" + incrementalUpdates +
                ", useRealState=" + useRealState +
                '}';
    }
}
