package server.exceptions;

public class RequestException extends Exception {

    private String requestId;

    public RequestException(String requestId, String message) {
        super(message);
        this.requestId = requestId;
    }

    public RequestException(String requestId, String message, Exception ex) {
        super(message, ex);
        this.requestId = requestId;
    }

    public RequestException(String requestId, Exception ex) {
        super(ex);
        this.requestId = requestId;
    }

    public RequestException(String message) {
        super(message);
        this.requestId = null;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
