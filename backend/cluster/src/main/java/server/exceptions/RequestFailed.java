package server.exceptions;

public class RequestFailed extends RequestException {

    public RequestFailed(String requestId, String message) {
        super(requestId, message);
    }

    public RequestFailed(String requestId, String message, Exception ex) {
        super(requestId, message, ex);
    }

    public RequestFailed(String requestId, Exception ex) {
        super(requestId, ex);
    }

    public RequestFailed(String requestId) {
        super(requestId);
    }
}
