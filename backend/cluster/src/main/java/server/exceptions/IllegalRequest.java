package server.exceptions;

public class IllegalRequest extends RequestException {

    public IllegalRequest(String requestId, String message) {
        super(requestId, message);
    }

    public IllegalRequest(String requestId, String message, Exception ex) {
        super(requestId, message, ex);
    }

    public IllegalRequest(String requestId, Exception ex) {
        super(requestId, ex);
    }

    public IllegalRequest(String requestId) {
        super(requestId);
    }
}
