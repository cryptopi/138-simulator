package server;

import config.SimulationConfig;
import executors.simulations.*;
import executors.simulations.factories.ParallelSimulatorFactory;
import executors.simulations.factories.SeriesSimulatorFactory;
import executors.simulations.factories.SimulatorFactory;
import executors.statistics.StatisticsExecutor;
import functions.FunctionFactory;
import org.java_websocket.WebSocket;
import server.exceptions.IllegalRequest;
import server.exceptions.RequestFailed;
import server.parsers.RequestParser;
import server.requests.FullRequest;
import server.requests.Request;
import server.requests.StatisticsRequest;
import server.websocket.StatisticWebsocketWriter;
import simulations.SimulationRepository;
import tasks.callback.CallbackTaskFactory;
import tasks.callback.ThreadPoolCallbackTaskFactory;
import tasks.iteration.factories.ParallelSimulationIterationTaskFactory;
import tasks.iteration.factories.SeriesSimulationIterationTaskFactory;
import tasks.iteration.factories.SimulationIterationTaskFactory;
import tasks.state.combiner.ForkJoinSimulationStateCombiner;
import tasks.state.combiner.SimulationStateCombiner;
import tasks.state.factories.CreateSimulationStateTaskFactory;
import tasks.state.factories.ForkJoinCreateSimulationStateTaskFactory;
import tasks.statistic.combine.factories.InlineStatCombineTaskFactory;
import tasks.statistic.factories.ForkJoinStatTaskFactory;
import tasks.statistic.factories.StatTaskFactory;
import tasks.statistic.results.StatisticResult;
import tasks.step.factories.ForkJoinSimulationStepTaskFactory;
import tasks.step.factories.SimulationStepTaskFactory;
import utils.ThreadPoolUtils;

import java.util.concurrent.ExecutionException;

public class EndpointRequestHandler {

    private RequestParser parser;
    private SimulationRepository simulationRepository;
    private FunctionFactory functionFactory;

    public EndpointRequestHandler(
            RequestParser parser,
            SimulationRepository simulationRepository,
            FunctionFactory functionFactory
    ) {
        this.parser = parser;
        this.simulationRepository = simulationRepository;
        this.functionFactory = functionFactory;
    }

    public void handle(String message, WebSocket conn) throws IllegalRequest, RequestFailed {
        Request request = parser.parse(message);
        try {
            executeRequest(request, conn);
        } catch (IllegalRequest | RequestFailed ex) {
            ex.setRequestId(request.getId());
            throw ex;
        }
    }

    private SimulatorFactory getSimulatorFactory(ParallelOrSeries setting) throws IllegalRequest {
        if (setting == ParallelOrSeries.PARALLEL) {
            return new ParallelSimulatorFactory();
        }
        if (setting == ParallelOrSeries.SERIES) {
            return new SeriesSimulatorFactory();
        }
        throw new IllegalRequest(
                "ParallelOrSeries " + setting + " is not recognized"
        );
    }

    private CreateSimulationStateTaskFactory getCreateStateTaskFactory(ParallelType setting) throws IllegalRequest {
        if (setting == ParallelType.INLINE) {
            throw new IllegalRequest(
                    "Cannot create inline state creation factories"
            );
        }
        if (setting == ParallelType.FORK_JOIN) {
            return new ForkJoinCreateSimulationStateTaskFactory();
        }
        if (setting == ParallelType.PARALLEL) {
            throw new IllegalRequest(
                    "Cannot create parallel state creation factories"
            );
        }
        throw new IllegalRequest(
                "ParallelType setting " + setting + " is not recognized"
        );
    }

    private SimulationIterationTaskFactory getIterationTaskFactory(
            ParallelOrSeries setting,
            SimulationStepTaskFactory stepTaskFactory,
            SimulationStateCombiner stateCombiner,
            boolean useRealState
    ) throws IllegalRequest {
        if (setting == ParallelOrSeries.PARALLEL) {
            if (useRealState) {
                throw new IllegalRequest(
                        "Cannot use real state with a parallel " +
                                "iteration task factory"
                );
            }
            return new ParallelSimulationIterationTaskFactory(stepTaskFactory);
        }
        if (setting == ParallelOrSeries.SERIES) {
            return new SeriesSimulationIterationTaskFactory(
                    stepTaskFactory,
                    stateCombiner,
                    useRealState
            );
        }
        throw new IllegalRequest(
                "ParallelOrSeries " + setting + " is not recognized"
        );
    }

    private SimulationStepTaskFactory getStepTaskFactory(
            ParallelType setting,
            boolean incremental
    ) throws IllegalRequest {
        if (setting == ParallelType.INLINE) {
            throw new IllegalRequest(
                    "Cannot create inline step task factories"
            );
        }
        if (setting == ParallelType.FORK_JOIN) {
            return new ForkJoinSimulationStepTaskFactory(incremental, functionFactory);
        }
        if (setting == ParallelType.PARALLEL) {
            throw new IllegalRequest(
                    "Cannot create parallel step task factories"
            );
        }
        throw new IllegalRequest(
                "ParallelType setting " + setting + " is not recognized"
        );
    }

    private SimulationStateCombiner getStateCombiner(ParallelType setting) throws IllegalRequest {
        if (setting == ParallelType.INLINE) {
            throw new IllegalRequest(
                    "Cannot create inline state combiner"
            );
        }
        if (setting == ParallelType.FORK_JOIN) {
            return new ForkJoinSimulationStateCombiner();
        }
        if (setting == ParallelType.PARALLEL) {
            throw new IllegalRequest(
                    "Cannot create parallel state combiners"
            );
        }
        throw new IllegalRequest(
                "ParallelType setting " + setting + " is not recognized"
        );
    }

    private StatTaskFactory getStatTaskFactory(ParallelType setting) throws IllegalRequest {
        if (setting == ParallelType.INLINE) {
            throw new IllegalRequest(
                    "Cannot create inline stat task factories"
            );
        }
        if (setting == ParallelType.FORK_JOIN) {
            return new ForkJoinStatTaskFactory();
        }
        if (setting == ParallelType.PARALLEL) {
            throw new IllegalRequest(
                    "Cannot create parallel stat task factories"
            );
        }
        throw new IllegalRequest(
                "ParallelType setting " + setting + " is not recognized"
        );
    }

    private CallbackTaskFactory<StatisticResult> getCallbackTaskFactory(ParallelType setting) throws IllegalRequest {
        if (setting == ParallelType.INLINE) {
            throw new IllegalRequest(
                    "Cannot create inline callback task factories"
            );
        }
        if (setting == ParallelType.FORK_JOIN) {
            throw new IllegalRequest(
                    "Cannot create fork join callback task factories"
            );
        }
        if (setting == ParallelType.PARALLEL) {
            return new ThreadPoolCallbackTaskFactory<>();
        }
        throw new IllegalRequest(
                "ParallelType setting " + setting + " is not recognized"
        );
    }

    private void executeFullRequest(FullRequest request, WebSocket conn) throws ExecutionException, InterruptedException, IllegalRequest {
        Simulator simulator = null;
        ExecutionPreferences prefs = request.getExecution();
        SimulatorFactory simulatorFactory =
                getSimulatorFactory(prefs.getSimulator());
        CreateSimulationStateTaskFactory createStateTaskFactory =
                getCreateStateTaskFactory(prefs.getCreateSimulationState());
        SimulationStepTaskFactory stepTaskFactory =
                getStepTaskFactory(
                        prefs.getSimulationSteps(),
                        prefs.shouldUseIncrementalUpdates()
                );
        SimulationStateCombiner stateCombiner =
                getStateCombiner(prefs.getCombineStates());
        SimulationIterationTaskFactory iterationTaskFactory =
                getIterationTaskFactory(
                        prefs.getSimulationIteration(),
                        stepTaskFactory,
                        stateCombiner,
                        prefs.shouldUseRealState()
                );

        StatisticsExecutor statisticsExecutor = new StatisticsExecutor(
                request.getStatistics(),
                getStatTaskFactory(prefs.getStatisticsTasks()),
                getCallbackTaskFactory(prefs.getStatisticsCallback()),
                new StatisticWebsocketWriter(conn),
                new InlineStatCombineTaskFactory()
        );

        SimulatorCallback statCallback =
                statisticsExecutor.createSimulatorCallback();
        int totalIterationCount = request
                .getSimulations()
                .stream()
                .map(SimulationConfig::getTotalIterationCount)
                .mapToInt(a -> a)
                .sum();
        SimulatorCallback progressCallback =
                new ProgressUpdateSimulatorCallback(
                        request.getId(),
                        conn
                );
        simulator = simulatorFactory.createSimulator(
                request.getSimulations(),
                createStateTaskFactory,
                iterationTaskFactory,
                stateCombiner,
                statCallback.andThen(progressCallback),
                simulationRepository
        );
        final SimulatorContainer container =
                new SimulatorContainer(simulator);
        ThreadPoolUtils.getFixedPool().submit(container);
    }

    private void executeStatsRequest(StatisticsRequest request, WebSocket conn) throws IllegalRequest {
        ExecutionPreferences prefs = request.getExecution();
        String[] groupKeys = request.getGroupKeys();
        String jobId = request.getJobId();

        StatisticsExecutor statisticsExecutor = new StatisticsExecutor(
                request.getStatistics(),
                getStatTaskFactory(prefs.getStatisticsTasks()),
                getCallbackTaskFactory(prefs.getStatisticsCallback()),
                new StatisticWebsocketWriter(conn),
                new InlineStatCombineTaskFactory()
        );

        SimulatorCallback statCallback =
                statisticsExecutor.createSimulatorCallback();
        Simulator simulator = new HistoricalSimulator(
                jobId,
                groupKeys,
                statCallback,
                simulationRepository
        );
        final SimulatorContainer container =
                new SimulatorContainer(simulator);
        ThreadPoolUtils.getFixedPool().submit(container);
    }

    private void executeRequest(Request request, WebSocket conn) throws IllegalRequest, RequestFailed {
        try {
            if (request.getClass().equals(StatisticsRequest.class)) {
                executeStatsRequest((StatisticsRequest)request, conn);
            } else if (request.getClass().equals(FullRequest.class)){
                executeFullRequest((FullRequest)request, conn);
            } else {
                throw new IllegalArgumentException(
                        "Request " + request + " is not of a valid type"
                );
            }
        } catch (ExecutionException | InterruptedException e) {
            throw new RequestFailed("Request failed", e);
        }
    }
}
