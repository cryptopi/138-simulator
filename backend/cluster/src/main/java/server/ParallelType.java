package server;

public enum ParallelType {

    FORK_JOIN, PARALLEL, INLINE
}
