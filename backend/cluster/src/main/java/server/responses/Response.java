package server.responses;

public class Response {

    private ResponseType type;

    protected Response() {}

    public Response(ResponseType type) {
        this.type = type;
    }
}
