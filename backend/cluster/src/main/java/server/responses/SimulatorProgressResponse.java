package server.responses;

public class SimulatorProgressResponse extends Response {

    private String requestId;
    private String simulationId;
    private int iterationsCompleted;

    private SimulatorProgressResponse() {}

    public SimulatorProgressResponse(String requestId, String simulationId, int iterationsCompleted) {
        super(ResponseType.SIMULATOR_PROGRESS);
        this.requestId = requestId;
        this.simulationId = simulationId;
        this.iterationsCompleted = iterationsCompleted;
    }
}
