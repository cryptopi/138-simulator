package server.responses;

import tasks.statistic.results.StatisticResult;

public class StatisticResponse extends Response {

    private StatisticResult result;

    private StatisticResponse() {}

    public StatisticResponse(StatisticResult result) {
        super(ResponseType.STAT_RESULT);
        this.result = result;
    }

//    private static ResponseType getStatisticType(StatisticResult result) {
//        if (result instanceof SimulationStatisticResult) {
//            return ResponseType.STAT_RESULT;
//        }
//        if (result instanceof GroupStatisticResult) {
//            return ResponseType.GROUP_STAT_RESULT;
//        }
//        throw new IllegalArgumentException(
//                "StatisticResult " + result + " is not recognized"
//        );
//    }

    @Override
    public String toString() {
        return "StatisticResponse{" +
                "result=" + result +
                '}';
    }
}
