package server.responses;

public class ErrorResponse extends Response {

    private String requestId;
    private String message;

    private ErrorResponse() {}

    public ErrorResponse(String requestId, String message) {
        super(ResponseType.ERROR);
        this.requestId = requestId;
        this.message = message;
    }
}
