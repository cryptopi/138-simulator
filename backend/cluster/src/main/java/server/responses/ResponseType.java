package server.responses;

public enum ResponseType {

    STAT_RESULT,
    ERROR,
    SIMULATOR_PROGRESS,
    STATS_PROGRESS
}
