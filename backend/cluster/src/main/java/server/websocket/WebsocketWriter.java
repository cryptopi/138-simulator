package server.websocket;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.dongliu.gson.GsonJava8TypeAdapterFactory;
import org.java_websocket.WebSocket;

import java.util.function.Consumer;

public class WebsocketWriter<T> {

    private WebSocket webSocket;
    private Gson gson;

    public WebsocketWriter(WebSocket webSocket) {
        this.webSocket = webSocket;
        this.gson = new GsonBuilder()
                .registerTypeAdapterFactory(
                        new GsonJava8TypeAdapterFactory()
                ).serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create();
    }

    public void write(T obj) {
        try {
            String json = gson.toJson(obj);
            webSocket.send(json);
        } catch (Error e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
