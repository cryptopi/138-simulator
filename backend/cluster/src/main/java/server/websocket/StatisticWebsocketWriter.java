package server.websocket;

import org.java_websocket.WebSocket;
import server.responses.StatisticResponse;
import tasks.statistic.results.StatisticResult;

import java.util.function.Consumer;

public class StatisticWebsocketWriter extends WebsocketWriter<StatisticResponse> implements Consumer<StatisticResult> {

    public StatisticWebsocketWriter(WebSocket webSocket) {
        super(webSocket);
    }

    @Override
    public void accept(StatisticResult statisticResult) {
        write(new StatisticResponse(statisticResult));
    }
}
