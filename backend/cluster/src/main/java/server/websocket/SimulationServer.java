package server.websocket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.dongliu.gson.GsonJava8TypeAdapterFactory;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import server.EndpointRequestHandler;
import server.exceptions.IllegalRequest;
import server.exceptions.RequestFailed;
import server.responses.ErrorResponse;
import server.responses.Response;
import server.responses.ResponseType;

import java.net.InetSocketAddress;

public class SimulationServer extends WebSocketServer {

    private EndpointRequestHandler handler;
    private Gson gson;

    public SimulationServer(String host, int port, EndpointRequestHandler handler) {
        super(new InetSocketAddress(host, port));
        this.handler = handler;
        this.gson = new GsonBuilder()
                .registerTypeAdapterFactory(
                        new GsonJava8TypeAdapterFactory()
                )
                .serializeNulls()
                .create();
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake clientHandshake) {
        // Send confirmed
        System.out.println(
                "Got websocket connection from "
                        + conn.getRemoteSocketAddress()
        );
    }

    @Override
    public void onClose(WebSocket conn, int i, String s, boolean b) {
        System.out.println(
                "Connected from " + conn.getRemoteSocketAddress() + " dropped"
        );
    }

    @Override
    public void onMessage(WebSocket conn, String s) {
        System.out.println("Got message: " + s);
        try {
            handler.handle(s, conn);
        } catch (IllegalRequest | RequestFailed ex) {
            ex.printStackTrace();
            String json = gson.toJson(new ErrorResponse(
                    ex.getRequestId(),
                    ex.getMessage()
            ));
            conn.send(json);
        }
    }

    @Override
    public void onError(WebSocket conn, Exception e) {
        e.printStackTrace();
        String message = e.getMessage();
        if (message == null) {
            message = e.getClass().getName();
        }
        String json = gson.toJson(new ErrorResponse(
                null,
                message
        ));
        conn.send(json);
        System.out.println(
                "Error from " + e
        );
    }

    @Override
    public void onStart() {
        System.out.println("WebSocket server started!");
    }
}
