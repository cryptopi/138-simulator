package server.requests;

import config.EventInteractionConfig;
import config.MovementConfig;
import config.RedistributionConfig;
import config.SimulationConfig;
import config.state.InitialSimulationStateConfig;

import java.util.ArrayList;
import java.util.List;

public class RepeatedSimulationConfig {

    private String[] ids;
    private SimulationConfig config;

    private RepeatedSimulationConfig() {}

    public String[] getIds() {
        return ids;
    }

    public SimulationConfig getConfig() {
        return config;
    }

    public List<SimulationConfig> repeat() {
        List<SimulationConfig> configs = new ArrayList<>();
        for (String id: ids) {
            configs.add(new SimulationConfig(
                    id,
                    config.getGroupKey(),
                    config.getJobId(),
                    config.getStateConfig(),
                    config.getAgentMovement(),
                    config.getEventMovement().orElse(null),
                    config.getWealthRedistribution().orElse(null),
                    config.getEventInteractionConfig(),
                    config.getTotalIterationCount()
            ));
        }
        return configs;
    }
}
