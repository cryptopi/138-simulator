package server.requests;

public class StatisticsRequest extends Request {

    private String[] groupKeys;
    private String jobId;

    private StatisticsRequest() {}

    public String[] getGroupKeys() {
        return groupKeys;
    }

    public String getJobId() {
        return jobId;
    }
}
