package server.requests;

import config.SimulationConfig;
import config.statistic.StatConfig;
import executors.statistics.StatisticGrouping;
import server.ExecutionPreferences;

import java.util.List;

public class Request {

    private String id;
    private List<StatConfig> statistics;
    private ExecutionPreferences execution;

    protected Request() {}

    public String getId() {
        return id;
    }

    public List<StatConfig> getStatistics() {
        return statistics;
    }

    public ExecutionPreferences getExecution() {
        return execution;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id='" + id + '\'' +
                ", statistics=" + statistics +
                ", execution=" + execution +
                '}';
    }
}
