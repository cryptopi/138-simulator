package server.requests;

import config.SimulationConfig;

import java.util.ArrayList;
import java.util.List;

public class FullRequest extends Request {

    private List<RepeatedSimulationConfig> simulations;

    private FullRequest() {}

    public List<SimulationConfig> getSimulations() {
        List<SimulationConfig> configs = new ArrayList<>();
        for (RepeatedSimulationConfig simulation : simulations) {
            configs.addAll(simulation.repeat());
        }
        return configs;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id='" + getId() + '\'' +
                ", simulations=" + getSimulations() +
                ", statistics=" + getStatistics() +
                ", execution=" + getExecution() +
                '}';
    }
}
