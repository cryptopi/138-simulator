import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.StatefulRedisConnectionImpl;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.lambdaworks.redis.codec.ByteArrayCodec;
import org.apache.commons.math3.util.MathArrays;

import java.io.File;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class Playground {

    private static class TestClass {
        public int[] hi;
    }

    private static int[] getArray(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = i;
        }
        MathArrays.shuffle(arr);
        return arr;
    }

    private static int sumSingle(int[] arr) {
        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        return sum;
    }

    private static int sumMulti(int[] arr, ThreadPoolExecutor service) throws InterruptedException, ExecutionException {
        final int THREADS = 8;
        final int STRIDE = arr.length / THREADS;
        CountDownLatch latch = new CountDownLatch(THREADS);
        List<Future<Integer>> futures = new ArrayList<>();
        for (int i = 0; i < THREADS; i++) {
            final int index = i;
            futures.add(service.submit(() -> {
               int sum = 0;
               for (int j = 0; j < STRIDE; j++) {
                   sum += arr[STRIDE * index + j];
               }
               latch.countDown();
               return sum;
            }));
        }
        latch.await();
        int sum = 0;
        for (Future<Integer> future : futures) {
            sum += future.get();
        }
        return sum;
    }

    private static class Summer extends RecursiveTask<Integer> {

        private static final int SEGMENTS = 1000;

        private int[] arr;
        private int start;
        private int end;

        public Summer(int[] arr, int start, int end) {
            this.arr = arr;
            this.start = start;
            this.end = end;
        }

        public Integer compute() {
            int count = end - start;
            if (count > arr.length / SEGMENTS && getSurplusQueuedTaskCount() < 5000) {
                final Summer first = new Summer(arr, start, (start + end) / 2);
                final Summer second = new Summer(arr, (start + end) / 2 + 1, end);
                first.fork();
                return second.compute() + first.join();
            } else {
                int sum = 0;
                for (int i = start; i < end; i++) {
                    sum += arr[i];
                }
                return sum;
            }
        }
    }

    private static int sumFork(int[] arr) {
        ForkJoinTask<Integer> task = ForkJoinPool.commonPool().submit(new Summer(arr, 0, arr.length));
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(task::join);
        return task.join();
    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
//        ThreadPoolExecutor service = (ThreadPoolExecutor) Executors.newFixedThreadPool(8);
//        int[] array = getArray(100_000_000);
//        long start = System.nanoTime();
//        final int ITERATIONS = 250;
//        for (int i = 0; i < ITERATIONS; i++) {
////            int sum = sumSingle(array);
////            int sum = sumMulti(array, service);
//            int sum = sumFork(array);
//        }
//
//        long end = System.nanoTime();
//        double delta = (end - start) / 1_000_000_000D / ITERATIONS;
//        System.out.println(delta);
        TestClass test = new TestClass();
        System.out.println(test.hi);
    }

    private static void redis() {
        RedisClient redisClient = RedisClient.create("redis://127.0.0.1:6379");
        StatefulRedisConnection<byte[], byte[]> connection = redisClient.connect(new ByteArrayCodec());
        RedisCommands<byte[], byte[]> syncCommands = connection.sync();
        syncCommands.publish("channel".getBytes(), new byte[1_000_000_0]);
        System.out.println(Instant.now().toEpochMilli() / 1000D);

        System.out.println("Connected to Redis");
    }
}
