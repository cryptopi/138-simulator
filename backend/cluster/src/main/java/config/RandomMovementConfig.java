package config;

import java.util.Objects;

public class RandomMovementConfig implements AgentMovementConfig, EventMovementConfig {

    private int stepSize;

    private RandomMovementConfig() {}

    public RandomMovementConfig(int stepSize) {
        this.stepSize = stepSize;
    }

    public int getStepSize() {
        return stepSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RandomMovementConfig that = (RandomMovementConfig) o;
        return stepSize == that.stepSize;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stepSize);
    }

    @Override
    public String toString() {
        return "RandomMovementConfig{" +
                "stepSize=" + stepSize +
                '}';
    }
}
