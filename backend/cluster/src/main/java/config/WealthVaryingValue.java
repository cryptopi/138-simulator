package config;

import utils.CollectionsUtils;

import java.util.Arrays;
import java.util.Objects;

public class WealthVaryingValue {

    private double[] wealthCutoffs; // lower bounds
    private double[] values;
    private boolean areCutoffsPercentile;

    protected WealthVaryingValue() {}

    public WealthVaryingValue(
            double[] wealthCutoffs,
            double[] values,
            boolean areCutoffsPercentile
    ) {
        this.wealthCutoffs = wealthCutoffs;
        this.values = values;
        this.areCutoffsPercentile = areCutoffsPercentile;
        int[] sortedWealthIndices =
                CollectionsUtils.getSortedIndices(this.wealthCutoffs);
        CollectionsUtils.inPlaceReorder(this.wealthCutoffs, sortedWealthIndices);
        CollectionsUtils.inPlaceReorder(this.values, sortedWealthIndices);
        if (this.wealthCutoffs[0] != 0) {
            throw new IllegalArgumentException(
                    "Wealth cutoffs are lower bounds, so the " +
                            "values must begin at 0"
            );
        }
        if (this.wealthCutoffs.length != this.values.length) {
            throw new IllegalArgumentException(
                    "Wealth cutoffs and values must have the same length"
            );
        }
    }

    public double[] getWealthCutoffs() {
        return wealthCutoffs;
    }

    public boolean areCuttofsPercentile() {
        return areCutoffsPercentile;
    }

    public double[] getValues() {
        return values;
    }

    public int getBracketIndex(double wealth) {
        int index = Arrays.binarySearch(wealthCutoffs, wealth);
        if (index < 0) {
            // this is the first element that is greater than the wealth
            // or the length of the array of all elements are less than
            index = -index + 1;
            // We must subtract 1 from the above index since the ith value
            // corresponds to wealths that are at least the ith wealth
            // but less than the i+1th wealth
            index--;
        }
        return index;
    }

    public double getValueForWealth(double wealth) {
        return values[getBracketIndex(wealth)];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WealthVaryingValue that = (WealthVaryingValue) o;
        return areCutoffsPercentile == that.areCutoffsPercentile &&
                Arrays.equals(wealthCutoffs, that.wealthCutoffs) &&
                Arrays.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(areCutoffsPercentile);
        result = 31 * result + Arrays.hashCode(wealthCutoffs);
        result = 31 * result + Arrays.hashCode(values);
        return result;
    }

    @Override
    public String toString() {
        return "WealthVaryingValue{" +
                "wealthCutoffs=" + Arrays.toString(wealthCutoffs) +
                ", values=" + Arrays.toString(values) +
                ", areCuttofsPercentile=" + areCutoffsPercentile +
                '}';
    }
}
