package config;

public class EventInteractionConfig implements StepConfig {

    private NumberConfig probabilityLuckyEvents;
    private NumberConfig probabilityUnluckyEvents;
    private EventWealthConfig wealth;

    public EventInteractionConfig(
            NumberConfig probabilityLuckyEvents,
            NumberConfig probabilityUnluckyEvents,
            EventWealthConfig wealth
    ) {
        this.probabilityLuckyEvents = probabilityLuckyEvents;
        this.probabilityUnluckyEvents = probabilityUnluckyEvents;
        this.wealth = wealth;
    }

    public NumberConfig getProbabilityLuckyEvents() {
        return probabilityLuckyEvents;
    }

    public NumberConfig getProbabilityUnluckyEvents() {
        return probabilityUnluckyEvents;
    }

    public EventWealthConfig getWealth() {
        return wealth;
    }

    @Override
    public String toString() {
        return "EventInteractionConfig{" +
                "probabilityLuckyEvents=" + probabilityLuckyEvents +
                ", probabilityUnluckyEvents=" + probabilityUnluckyEvents +
                ", wealth=" + wealth +
                '}';
    }
}
