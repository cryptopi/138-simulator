package config.state;

public class IndependentLocationGeneratorConfig implements LocationGeneratorConfig {

    private GeneratorConfig xGenerator;
    private GeneratorConfig yGenerator;

    private IndependentLocationGeneratorConfig() {}

    public IndependentLocationGeneratorConfig(
            GeneratorConfig xGenerator,
            GeneratorConfig yGenerator
    ) {
        this.xGenerator = xGenerator;
        this.yGenerator = yGenerator;
    }

    public GeneratorConfig getXGenerator() {
        return xGenerator;
    }

    public GeneratorConfig getYGenerator() {
        return yGenerator;
    }

    @Override
    public String toString() {
        return "IndependentLocationGeneratorConfig{" +
                "xGenerator=" + xGenerator +
                ", yGenerator=" + yGenerator +
                '}';
    }
}
