package config.state;

public class UniformGeneratorConfig implements GeneratorConfig {

    private double lower;
    private double upper;

    private UniformGeneratorConfig() {}

    public UniformGeneratorConfig(double lower, double upper) {
        this.lower = lower;
        this.upper = upper;
    }

    public double getLower() {
        return lower;
    }

    public double getUpper() {
        return upper;
    }

    @Override
    public String toString() {
        return "UniformGeneratorConfig{" +
                "lower=" + lower +
                ", upper=" + upper +
                '}';
    }
}
