package config.state;

public class InitialSimulationStateConfig {

    private GeneratorConfig skillGenerator;
    private LocationGeneratorConfig agentLocationGenerator;
    private LocationGeneratorConfig eventLocationGenerator;
    private GeneratorConfig eventMagnitudeGenerator;
    private int agentCount;
    private int eventCount;
    private int gridBounds;

    private InitialSimulationStateConfig() {}

    public InitialSimulationStateConfig(
            GeneratorConfig skillGenerator,
            LocationGeneratorConfig agentLocationGenerator,
            LocationGeneratorConfig eventLocationGenerator,
            GeneratorConfig eventMagnitudeGenerator,
            int agentCount,
            int eventCount,
            int gridBounds
    ) {
        this.skillGenerator = skillGenerator;
        this.agentLocationGenerator = agentLocationGenerator;
        this.eventLocationGenerator = eventLocationGenerator;
        this.eventMagnitudeGenerator = eventMagnitudeGenerator;
        this.agentCount = agentCount;
        this.eventCount = eventCount;
        this.gridBounds = gridBounds;
    }

    public GeneratorConfig getSkillGenerator() {
        return skillGenerator;
    }

    public LocationGeneratorConfig getAgentLocationGenerator() {
        return agentLocationGenerator;
    }

    public LocationGeneratorConfig getEventLocationGenerator() {
        return eventLocationGenerator;
    }

    public GeneratorConfig getEventMagnitudeGenerator() {
        return eventMagnitudeGenerator;
    }

    public int getAgentCount() {
        return agentCount;
    }

    public int getEventCount() {
        return eventCount;
    }

    public int getGridBounds() {
        return gridBounds;
    }

    @Override
    public String toString() {
        return "InitialSimulationStateConfig{" +
                "skillGenerator=" + skillGenerator +
                ", agentLocationGenerator=" + agentLocationGenerator +
                ", eventLocationGenerator=" + eventLocationGenerator +
                ", eventMagnitudeGenerator=" + eventMagnitudeGenerator +
                ", agentCount=" + agentCount +
                ", eventCount=" + eventCount +
                ", gridBounds=" + gridBounds +
                '}';
    }
}
