package config.state;

import java.util.Arrays;

public class ChoiceGeneratorConfig implements GeneratorConfig {

    private double[] values;
    private double[] probabilities;

    public ChoiceGeneratorConfig(double[] values, double[] probabilities) {
        this.values = values;
        this.probabilities = probabilities;
    }

    public double[] getValues() {
        return values;
    }

    public double[] getProbabilities() {
        return probabilities;
    }

    @Override
    public String toString() {
        return "ChoiceGeneratorConfig{" +
                "values=" + Arrays.toString(values) +
                ", probabilities=" + Arrays.toString(probabilities) +
                '}';
    }
}
