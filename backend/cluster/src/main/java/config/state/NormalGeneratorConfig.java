package config.state;

public class NormalGeneratorConfig implements GeneratorConfig{

    private double mu;
    private double sigma;

    private NormalGeneratorConfig() {}

    public NormalGeneratorConfig(double mu, double sigma) {
        this.mu = mu;
        this.sigma = sigma;
    }

    public double getMu() {
        return mu;
    }

    public double getSigma() {
        return sigma;
    }

    @Override
    public String toString() {
        return "NormalGeneratorConfig{" +
                "mu=" + mu +
                ", sigma=" + sigma +
                '}';
    }
}
