package config;

public enum StepType {

    AGENT_MOVEMENT,
    EVENT_MOVEMENT,
    EVENT_INTERACTION,
    WEALTH_REDISTRIBUTION
}
