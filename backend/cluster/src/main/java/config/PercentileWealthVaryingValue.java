package config;

public class PercentileWealthVaryingValue extends WealthVaryingValue {

    private PercentileWealthVaryingValue() {}

    public PercentileWealthVaryingValue(
            double[] wealthCutoffs,
            double[] values
    ) {
        super(wealthCutoffs, values, true);
    }
}
