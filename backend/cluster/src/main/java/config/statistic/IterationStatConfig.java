package config.statistic;

public class IterationStatConfig extends StatConfig {

    private boolean lastIterationOnly;

    public IterationStatConfig(String id, boolean lastIterationOnly) {
        super(id);
        this.lastIterationOnly = lastIterationOnly;
    }

    public boolean isLastIterationOnly() {
        return lastIterationOnly;
    }

    @Override
    public String toString() {
        return "IterationStatConfig{" +
                "lastIterationOnly=" + lastIterationOnly +
                '}';
    }
}
