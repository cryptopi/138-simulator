package config.statistic;

/**
 * Gets what top percent of people collective have the provided percent
 * of the total wealth
 */
public class TopPercentFixedWealthConfig extends IterationStatConfig {

    private double wealthPercent;

    public TopPercentFixedWealthConfig(
            String id,
            boolean lastIterationOnly,
            double wealthPercent
    ) {
        super(id, lastIterationOnly);
        this.wealthPercent = wealthPercent;
    }

    public double getWealthPercent() {
        return wealthPercent;
    }

    @Override
    public String toString() {
        return "TopPercentFixedWealthConfig{" +
                "wealthPercent=" + wealthPercent +
                '}';
    }
}
