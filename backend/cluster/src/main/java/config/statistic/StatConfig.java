package config.statistic;

import config.statistic.combine.StatCombinerConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class StatConfig {

    private String id;
    private Map<String, List<StatCombinerConfig>> combiners;

    private StatConfig() {}

    public StatConfig(String id) {
        this.id = id;
        this.combiners = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public Map<String, List<StatCombinerConfig>> getCombiners() {
        return combiners;
    }
}
