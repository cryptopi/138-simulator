package config;

public class MultiplicativeEventWealthConfig implements EventWealthConfig {

    private double luckyFactor;
    private double unluckyFactor;
    private boolean includeEventMagnitude;

    private MultiplicativeEventWealthConfig() {}

    public MultiplicativeEventWealthConfig(
            double luckyFactor,
            double unluckyFactor,
            boolean includeEventMagnitude
    ) {
        this.luckyFactor = luckyFactor;
        this.unluckyFactor = unluckyFactor;
        this.includeEventMagnitude = includeEventMagnitude;
    }

    public double getLuckyFactor() {
        return luckyFactor;
    }

    public double getUnluckyFactor() {
        return unluckyFactor;
    }

    public boolean includeEventMagnitude() {
        return includeEventMagnitude;
    }

    @Override
    public String toString() {
        return "MultiplicativeEventWealthConfig{" +
                "luckyFactor=" + luckyFactor +
                ", unluckyFactor=" + unluckyFactor +
                ", includeEventMagnitude=" + includeEventMagnitude +
                '}';
    }
}
