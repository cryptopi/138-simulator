package config;

public class LinearProbabilityConfig implements NumberConfig {

    private double multiplier;
    private double offset;
    private double min;
    private double max;
    private String variable;

    private LinearProbabilityConfig() {
        this.max = 1;
    }

    public LinearProbabilityConfig(
            double multiplier,
            double offset,
            double min,
            double max,
            String variable
    ) {
        this.multiplier = multiplier;
        this.offset = offset;
        this.min = min;
        this.max = max;
        this.variable = variable;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public double getOffset() {
        return offset;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public String getVariable() {
        return variable;
    }

    @Override
    public String toString() {
        return "Linear{" +
                "multiplier=" + multiplier +
                ", offset=" + offset +
                ", min=" + min +
                ", max=" + max +
                ", variable='" + variable + '\'' +
                '}';
    }
}
