package config;

import java.util.Objects;

public class GeneralWealthRedistributionConfig implements RedistributionConfig {

    private PercentileWealthVaryingValue taxBrackets;
    private boolean isTaxPercent;
    private PercentileWealthVaryingValue redistributionBrackets;

    public GeneralWealthRedistributionConfig(
            PercentileWealthVaryingValue taxBrackets,
            boolean isTaxPercent,
            PercentileWealthVaryingValue redistributionBrackets
    ) {
        this.taxBrackets = taxBrackets;
        this.isTaxPercent = isTaxPercent;
        this.redistributionBrackets = redistributionBrackets;
    }

    public PercentileWealthVaryingValue getTaxBrackets() {
        return taxBrackets;
    }

    public boolean isTaxPercent() {
        return isTaxPercent;
    }

    public PercentileWealthVaryingValue getRedistributionBrackets() {
        return redistributionBrackets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeneralWealthRedistributionConfig that = (GeneralWealthRedistributionConfig) o;
        return isTaxPercent == that.isTaxPercent &&
                Objects.equals(taxBrackets, that.taxBrackets) &&
                Objects.equals(redistributionBrackets, that.redistributionBrackets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taxBrackets, isTaxPercent, redistributionBrackets);
    }

    @Override
    public String toString() {
        return "GeneralWealthRedistributionConfig{" +
                "taxBrackets=" + taxBrackets +
                ", isTaxPercent=" + isTaxPercent +
                ", redistributionBrackets=" + redistributionBrackets +
                '}';
    }
}
