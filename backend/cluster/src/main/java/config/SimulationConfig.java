package config;

import config.state.InitialSimulationStateConfig;

import java.util.Optional;

public class SimulationConfig {

    private String id;
    private String groupKey;
    private String jobId;
    private InitialSimulationStateConfig stateConfig;
    private MovementConfig agentMovement;
    private Optional<MovementConfig> eventMovement;
    private Optional<RedistributionConfig> wealthRedistribution;
    private EventInteractionConfig eventInteractionConfig;
    private int totalIterationCount;

    private SimulationConfig() {}

    public SimulationConfig(
            String id,
            String groupKey,
            String jobId,
            InitialSimulationStateConfig stateConfig,
            MovementConfig agentMovement,
            MovementConfig eventMovement,
            RedistributionConfig wealthRedistribution,
            EventInteractionConfig eventInteractionConfig,
            int totalIterationCount
    ) {
        this.id = id;
        this.groupKey = groupKey;
        this.jobId = jobId;
        this.stateConfig = stateConfig;
        this.agentMovement = agentMovement;
        this.eventMovement = Optional.ofNullable(eventMovement);
        this.wealthRedistribution = Optional.ofNullable(wealthRedistribution);
        this.eventInteractionConfig = eventInteractionConfig;
        this.totalIterationCount = totalIterationCount;
    }

    public String getId() {
        return id;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public String getJobId() {
        return jobId;
    }

    public InitialSimulationStateConfig getStateConfig() {
        return stateConfig;
    }

    public MovementConfig getAgentMovement() {
        return agentMovement;
    }

    public Optional<MovementConfig> getEventMovement() {
        return eventMovement;
    }

    public Optional<RedistributionConfig> getWealthRedistribution() {
        return wealthRedistribution;
    }

    public EventInteractionConfig getEventInteractionConfig() {
        return eventInteractionConfig;
    }

    public int getTotalIterationCount() {
        return totalIterationCount;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    @Override
    public String toString() {
        return "SimulationConfig{" +
                "id='" + id + '\'' +
                ", groupKey='" + groupKey + '\'' +
                ", jobId='" + jobId + '\'' +
                ", stateConfig=" + stateConfig +
                ", agentMovement=" + agentMovement +
                ", eventMovement=" + eventMovement +
                ", wealthRedistribution=" + wealthRedistribution +
                ", eventInteractionConfig=" + eventInteractionConfig +
                ", totalIterationCount=" + totalIterationCount +
                '}';
    }
}
