package executors.statistics;

import config.statistic.IterationStatConfig;
import config.statistic.combine.StatCombinerConfig;
import config.statistic.StatConfig;
import config.statistic.combine.NoCombineConfig;
import executors.simulations.SimulatorCallback;
import simulations.Simulation;
import simulations.state.SimulationState;
import tasks.Task;
import tasks.callback.CallbackTaskFactory;
import tasks.statistic.combine.factories.StatCombineTaskFactory;
import tasks.statistic.factories.StatTaskFactory;
import tasks.statistic.results.StatisticResult;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class StatisticsExecutor {

    private static class GroupKey {
        private String groupKey;
        private String statId;
        private int iterationNumber;

        public GroupKey(String groupKey, String statId, int iterationNumber) {
            this.groupKey = groupKey;
            this.statId = statId;
            this.iterationNumber = iterationNumber;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GroupKey groupKey1 = (GroupKey) o;
            return iterationNumber == groupKey1.iterationNumber &&
                    Objects.equals(groupKey, groupKey1.groupKey) &&
                    Objects.equals(statId, groupKey1.statId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(groupKey, statId, iterationNumber);
        }

        @Override
        public String toString() {
            return "GroupKey{" +
                    "groupKey='" + groupKey + '\'' +
                    ", statId='" + statId + '\'' +
                    ", iterationNumber=" + iterationNumber +
                    '}';
        }
    }

    private StatTaskFactory taskFactory;
    private final Map<String, IterationStatConfig> iterationConfigs;
    private CallbackTaskFactory<StatisticResult> callbackTaskFactory;
    private Consumer<StatisticResult> callback;
    private StatCombineTaskFactory combineTaskFactory;

    // Grouping members
    private int simulationCountPerGroup;
    private ConcurrentMap<GroupKey, LinkedBlockingQueue<StatisticResult>> groupedStatistics;

    public StatisticsExecutor(
            StatTaskFactory taskFactory,
            CallbackTaskFactory<StatisticResult> callbackTaskFactory,
            Consumer<StatisticResult> callback,
            StatCombineTaskFactory combineTaskFactory,
            StatConfig ... configs
    ) {
        this(
                Arrays.asList(configs),
                taskFactory,
                callbackTaskFactory,
                callback,
                combineTaskFactory
        );
    }

    public StatisticsExecutor(
            List<StatConfig> configs,
            StatTaskFactory taskFactory,
            CallbackTaskFactory<StatisticResult> callbackTaskFactory,
            Consumer<StatisticResult> callback,
            StatCombineTaskFactory combineTaskFactory
    ) {
        this.taskFactory = taskFactory;
        this.callbackTaskFactory = callbackTaskFactory;
        this.iterationConfigs = new HashMap<>();
        this.callback = callback;
        this.groupedStatistics = new ConcurrentHashMap<>();
        this.simulationCountPerGroup = -1;
        this.combineTaskFactory = combineTaskFactory;
        partitionConfigs(configs);
        configureGroupingCallback();
    }

    private void partitionConfigs(List<StatConfig> configs) {
        for (StatConfig config : configs) {
            if (config instanceof IterationStatConfig) {
                iterationConfigs.put(config.getId(), (IterationStatConfig)config);
            }
        }
    }

    private void configureGroupingCallback() {
        final Consumer<StatisticResult> destinationCallback = callback;
        // Group by group key and iteration number
        Consumer<StatisticResult> groupingConsumer = result -> {
            GroupKey groupKey = new GroupKey(
                    result.getGroupKey(),
                    result.getStatisticId(),
                    result.getIteration()
            );

            groupedStatistics.putIfAbsent(groupKey, new LinkedBlockingQueue<>());
            LinkedBlockingQueue<StatisticResult> results = groupedStatistics.get(groupKey);
            int size = 0;
            synchronized (this) {
                try {
                    results.put(result);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                size = results.size();
            }
            if (size == simulationCountPerGroup) {
                StatConfig config = iterationConfigs.get(result.getStatisticId());
                for (StatCombinerConfig combiner : config.getCombiners().get(result.getGroupKey())) {
                    // TODO Should release these as they are received and not
                    // as a burst in the end.
                    if (combiner instanceof NoCombineConfig) {
                        for (StatisticResult storedResult : results) {
                            destinationCallback.accept(storedResult);
                        }
                    } else {
                        Task<StatisticResult> combineTask = combineTaskFactory.create(
                                config,
                                combiner,
                                results
                        );
                        try {
                            StatisticResult groupResult =
                                    combineTask.compute().get();
                            destinationCallback.accept(groupResult);
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

//            int groupStatisticsSize = 0;
//            Collection<Object> groupStatistics = null;
//            // TODO performance there has got to be a better way to do this
//            // The issue is that a result is added in thread 1, then thread 2
//            // before .size() is called. Both threads call .size() and get
//            // a result of 2, which leads to the grouped statistics being fired
//            // multiple times.
//            synchronized (this) {
//                groupStatistics = CollectionsUtils.putInNestedConcurrentMap(
//                        groupedStatistics,
//                        groupKey,
//                        new LinkedBlockingQueue<>(),
//                        queue -> {
//                            try {
//                                queue.put(result.getValue());
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                );
//                groupStatisticsSize = groupStatistics.size();
//            }
//
//            Collection<Object> finalGroupStatistics = groupStatistics;
//
//            if (groupStatisticsSize == simulationCount) {
//                // Statistic is done! Send it out.
//                ThreadPoolUtils.getFixedPool().submit(() -> {
//                    destinationCallback.accept(new GroupStatisticResult(
//                           result.getStatisticId(),
//                           result.getGroupKey(),
//                           result.getIteration(),
//                           finalGroupStatistics
//                   ));
//                });
//            }
        };
        callback = groupingConsumer;
//        if (grouping == StatisticGrouping.GROUPED) {
//            callback = groupingConsumer;
//        } else {
//            // Need both grouped and ungrouped stats
//            callback = groupingConsumer.andThen(callback);
//        }
    }

    public void runIterationStats(
            String simulationId,
            String groupKey,
            int iteration,
            SimulationState state,
            boolean lastIteration
    ) {
        for (IterationStatConfig config : iterationConfigs.values()) {
            if (!config.isLastIterationOnly() || lastIteration) {
                Task<Void> task = callbackTaskFactory.create(
                        taskFactory.createIterationTask(
                                simulationId,
                                groupKey,
                                iteration,
                                config,
                                state
                        ),
                        callback
                );
                task.compute();
            }
        }
    }

    public void runOverallStats(Simulation simulation) {
        // In order for these to be implemented, grouping has to be
        // implemented for them.
        // Involves a second concurrent map, with group keys without the
        // iteration number and that should be it.
        throw new UnsupportedOperationException(
                "Overall stats have not been implemented yet"
        );
//        for (OverallStatConfig config : overallConfigs) {
//            Task<Void> task = callbackTaskFactory.create(
//                    taskFactory.createOverallTask(config, simulation),
//                    callback
//            );
//            task.compute();
//        }
    }

    public SimulatorCallback createSimulatorCallback() {
        return new SimulatorCallback() {
            @Override
            public void onStateCompleted(
                    Simulation simulation,
                    int iteration,
                    boolean lastIteration
            ) {
                runIterationStats(
                        simulation.getConfig().getId(),
                        simulation.getConfig().getGroupKey(),
                        iteration,
                        simulation.getState(iteration),
                        lastIteration
                );
            }

            @Override
            public void onSimulationCompleted(Simulation simulation) {
//                runOverallStats(simulation);
            }

            @Override
            public void onSimulatorStarted(Collection<Simulation> simulations) {
                int groupCount =new HashSet<>(
                        simulations
                                .stream()
                                .map(simulation -> simulation.getConfig().getGroupKey())
                                .collect(Collectors.toList())
                ).size();
                simulationCountPerGroup = simulations.size() / groupCount;
            }
        };
    }
}
