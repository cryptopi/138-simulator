package executors.statistics;

public enum StatisticGrouping {
    NO_GROUPING, GROUPED_AND_UNGROUPED, GROUPED
}
