package executors.simulations;

public class SimulatorContainer implements Runnable {

    private Simulator simulator;

    public SimulatorContainer(Simulator simulator) {
        this.simulator = simulator;
    }

    @Override
    public void run() {
        simulator.simulate();
    }
}
