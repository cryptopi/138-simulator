package executors.simulations;

import config.SimulationConfig;
import simulations.Simulation;
import simulations.SimulationRepository;
import simulations.state.SimulationState;
import tasks.iteration.factories.SimulationIterationTaskFactory;
import tasks.state.CreateSimulationStateTask;
import tasks.state.combiner.SimulationStateCombiner;
import tasks.state.factories.CreateSimulationStateTaskFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public abstract class BaseSimulator implements Simulator {

    private List<Simulation> simulations;
    private SimulationIterationTaskFactory iterationTaskFactory;
    private SimulationStateCombiner stateCombiner;
    private SimulatorCallback callback;
    private SimulationRepository simulationRepository;

    public BaseSimulator(
            List<SimulationConfig> configs,
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository simulationRepository
    ) throws ExecutionException, InterruptedException {
        this.simulations = createSimulations(configs, stateTaskFactory);
        this.iterationTaskFactory = iterationTaskFactory;
        this.stateCombiner = stateCombiner;
        this.callback = callback;
        this.simulationRepository = simulationRepository;
        SimulatorCallback repositoryCallback = new SimulatorCallback() {
            @Override
            public void onStateCompleted(Simulation simulation, int iteration, boolean lastIteration) {}

            @Override
            public void onSimulationCompleted(Simulation simulation) {
                simulationRepository.addSimulation(simulation);
            }
        };
        this.callback = repositoryCallback.andThen(this.callback);
    }

    public BaseSimulator(
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository simulationRepository,
            SimulationConfig ... configs
    ) throws ExecutionException, InterruptedException {
        this(
                Arrays.asList(configs),
                stateTaskFactory,
                iterationTaskFactory,
                stateCombiner,
                callback,
                simulationRepository
        );
    }

    private static List<Simulation> createSimulations(
            List<SimulationConfig> configs,
            CreateSimulationStateTaskFactory stateTaskFactory
    ) throws ExecutionException, InterruptedException {
        List<Future<SimulationState>> stateFutures =
                configs
                        .stream()
                        .map(SimulationConfig::getStateConfig)
                        .map(stateTaskFactory::createTask)
                        .map(CreateSimulationStateTask::compute)
                        .collect(Collectors.toList());
        List<SimulationState> initialStates = new ArrayList<>();
        for (Future<SimulationState> stateFuture : stateFutures) {
            initialStates.add(stateFuture.get());
        }
        List<Simulation> simulations = new ArrayList<>();
        for (int i = 0; i < configs.size(); i++) {
            simulations.add(new Simulation(
                    configs.get(i),
                    initialStates.get(i)
            ));
        }
        return simulations;
    }

    protected List<Simulation> getSimulations() {
        return simulations;
    }

    protected SimulationIterationTaskFactory getIterationTaskFactory() {
        return iterationTaskFactory;
    }

    protected SimulationStateCombiner getStateCombiner() {
        return stateCombiner;
    }

    protected SimulatorCallback getCallback() {
        return callback;
    }

    protected SimulationRepository getSimulationRepository() {
        return simulationRepository;
    }

    @Override
    public void simulate() {
        System.out.println("About to callback from thread: " + Thread.currentThread().getName());
        getCallback().simulatorStarted(simulations);

        for (Simulation simulation : this.simulations) {
            callback.stateCompleted(simulation, 0);
        }
    }
}
