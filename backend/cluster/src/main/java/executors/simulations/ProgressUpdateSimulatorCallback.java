package executors.simulations;

import org.java_websocket.WebSocket;
import server.responses.SimulatorProgressResponse;
import server.websocket.WebsocketWriter;
import simulations.Simulation;
import utils.ThreadPoolUtils;

public class ProgressUpdateSimulatorCallback extends SimulatorCallback {

    private String requestId;
    private WebsocketWriter<SimulatorProgressResponse> writer;

    public ProgressUpdateSimulatorCallback(String requestId, WebSocket webSocket) {
        super();
        this.requestId = requestId;
        this.writer = new WebsocketWriter<>(webSocket);
    }

    @Override
    public void onStateCompleted(
            Simulation simulation,
            int iteration,
            boolean lastIteration
    ) {
        // TODO need to have factory for this and not hardcode
        if (iteration < 1) {
            return;
        }
        ThreadPoolUtils.getFixedPool().submit(() -> {
            writer.write(new SimulatorProgressResponse(
                    requestId,
                    simulation.getConfig().getId(),
                    iteration
            ));
        });
    }

    @Override
    public void onSimulationCompleted(Simulation simulation) {}
}
