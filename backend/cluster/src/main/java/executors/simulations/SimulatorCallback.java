package executors.simulations;

import simulations.Simulation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class SimulatorCallback {

    private List<SimulatorCallback> chainedCallbacks;

    public SimulatorCallback() {
        this.chainedCallbacks = new ArrayList<>();
    }

    public final SimulatorCallback andThen(SimulatorCallback callback) {
        chainedCallbacks.add(callback);
        return this;
    }

    public final void stateCompleted(Simulation simulation, int iteration) {
        boolean lastIteration =
                iteration == simulation.getConfig().getTotalIterationCount();
        onStateCompleted(simulation, iteration, lastIteration);
        for (SimulatorCallback callback : chainedCallbacks) {
            callback.stateCompleted(simulation, iteration);
        }
    }

    public final void simulationCompleted(Simulation simulation) {
        onSimulationCompleted(simulation);
        for (SimulatorCallback callback : chainedCallbacks) {
            callback.simulationCompleted(simulation);
        }
    }

    public final void simulatorStarted(Collection<Simulation> simulations) {
        onSimulatorStarted(simulations);
        for (SimulatorCallback callback : chainedCallbacks) {
            callback.simulatorStarted(simulations);
        }
    }

    public final void simulatorEnded() {
        onSimulatorEnded();
        for (SimulatorCallback callback : chainedCallbacks) {
            callback.simulatorEnded();
        }
    }

    public abstract void onStateCompleted(
            Simulation simulation,
            int iteration,
            boolean lastIteration
    );
    public abstract void onSimulationCompleted(Simulation simulation);

    public void onSimulatorStarted(Collection<Simulation> simulations) {}

    public void onSimulatorEnded() {}
}
