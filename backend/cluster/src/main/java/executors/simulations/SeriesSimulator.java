package executors.simulations;

import config.SimulationConfig;
import simulations.Simulation;
import simulations.SimulationRepository;
import simulations.state.SimulationState;
import tasks.state.combiner.SimulationStateCombiner;
import simulations.state.updates.StateUpdate;
import simulations.state.updates.StateUpdateGroup;
import tasks.iteration.IterationTask;
import tasks.state.factories.CreateSimulationStateTaskFactory;
import tasks.iteration.factories.SimulationIterationTaskFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class SeriesSimulator extends BaseSimulator {

    public SeriesSimulator(
            List<SimulationConfig> configs,
            CreateSimulationStateTaskFactory stateFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository simulationRepository
    ) throws ExecutionException, InterruptedException {
        super(
                configs,
                stateFactory,
                iterationTaskFactory,
                stateCombiner,
                callback,
                simulationRepository
        );
    }

    public SeriesSimulator(
            CreateSimulationStateTaskFactory stateFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository simulationRepository,
            SimulationConfig... configs
    ) throws ExecutionException, InterruptedException {
        super(
                stateFactory,
                iterationTaskFactory,
                stateCombiner,
                callback,
                simulationRepository,
                configs
        );
    }

    @Override
    public void simulate() {
        super.simulate();
        // NOTE run per iteration stats for iteration n on iteration n + 1
        // so it can all go in parallel
        List<Simulation> simulations = getSimulations();
        while (true) {
            boolean done = true;
            // Start up all simulations for next step
            List<Future<List<StateUpdate>>> futures = new ArrayList<>(simulations.size());
            for (int i = 0; i < simulations.size(); i++) {
                Simulation simulation = simulations.get(i);
                if (simulation.hasNextIteration()) {
//                    System.out.println(Arrays.toString(simulation.getLastState().getWealths()));
                    done = false;
                    int next = simulation.getNextIteration();
                    IterationTask iterationTask =
                            getIterationTaskFactory().createTask(simulation, next);

                    Future<List<StateUpdate>> future = iterationTask.compute();
                    futures.add(i, future);
                } else {
                    getCallback().simulationCompleted(simulation);
                }
            }

            if (done) {
                break;
            }

            // Wait for all simulations steps to end
            List<SimulationState> nextStates = new ArrayList<>(simulations.size());
            try {
                List<Future<SimulationState>> stateFutures = new ArrayList<>(simulations.size());
                for (int i = 0; i < simulations.size(); i++) {
                    Future<List<StateUpdate>> future = futures.get(i);
                    if (future != null) {
                        StateUpdateGroup stateUpdates =
                                new StateUpdateGroup(future.get());
                        Simulation simulation = simulations.get(i);
                        SimulationState prevState = simulation.getLastState();
                        stateFutures.add(
                                getStateCombiner()
                                        .combine(prevState, stateUpdates)
                        );
                    }
                }
                for (int i = 0; i < stateFutures.size(); i++) {
                    Future<SimulationState> stateFuture = stateFutures.get(i);
                    if (stateFuture != null) {
                        SimulationState state = stateFuture.get();
                        nextStates.add(i, state);
                    }
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                // TODO exception
            }

            for (int i = 0; i < nextStates.size(); i++) {
                SimulationState nextState = nextStates.get(i);
                Simulation simulation = simulations.get(i);
                if (nextState != null) {
                    int iteration = simulation.getNextIteration();
                    simulation.setStepState(iteration, nextState);
                    getCallback().stateCompleted(simulation, iteration);
                    simulation.incrementIteration();
                }
            }
        }
        getCallback().simulatorEnded();
    }
}
