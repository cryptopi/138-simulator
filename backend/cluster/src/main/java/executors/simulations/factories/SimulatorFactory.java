package executors.simulations.factories;

import config.SimulationConfig;
import executors.simulations.Simulator;
import executors.simulations.SimulatorCallback;
import simulations.SimulationRepository;
import tasks.iteration.factories.SimulationIterationTaskFactory;
import tasks.state.combiner.SimulationStateCombiner;
import tasks.state.factories.CreateSimulationStateTaskFactory;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface SimulatorFactory {

    Simulator createSimulator(
            List<SimulationConfig> configs,
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository repository
    ) throws ExecutionException, InterruptedException;

    Simulator createSimulator(
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository repository,
            SimulationConfig ... configs
    ) throws ExecutionException, InterruptedException;
}
