package executors.simulations.factories;

import config.SimulationConfig;
import executors.simulations.ParallelSimulator;
import executors.simulations.Simulator;
import executors.simulations.SimulatorCallback;
import jdk.jshell.spi.ExecutionControl;
import simulations.SimulationRepository;
import tasks.iteration.factories.SimulationIterationTaskFactory;
import tasks.state.combiner.SimulationStateCombiner;
import tasks.state.factories.CreateSimulationStateTaskFactory;

import java.util.List;

public class ParallelSimulatorFactory implements SimulatorFactory {

    @Override
    public Simulator createSimulator(
            List<SimulationConfig> configs,
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
           SimulationRepository repository
    ) {
        throw new UnsupportedOperationException("Parallel Simulator not done yet");
    }

    @Override
    public Simulator createSimulator(
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository repository,
            SimulationConfig... configs
    ) {
        throw new UnsupportedOperationException("Parallel Simulator not done yet");
    }
}
