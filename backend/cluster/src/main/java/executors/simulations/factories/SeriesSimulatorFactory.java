package executors.simulations.factories;

import config.SimulationConfig;
import executors.simulations.SeriesSimulator;
import executors.simulations.Simulator;
import executors.simulations.SimulatorCallback;
import simulations.SimulationRepository;
import tasks.iteration.factories.SimulationIterationTaskFactory;
import tasks.state.combiner.SimulationStateCombiner;
import tasks.state.factories.CreateSimulationStateTaskFactory;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class SeriesSimulatorFactory implements SimulatorFactory {

    @Override
    public Simulator createSimulator(
            List<SimulationConfig> configs,
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository repository
    ) throws ExecutionException, InterruptedException {
        return new SeriesSimulator(
                configs,
                stateTaskFactory,
                iterationTaskFactory,
                stateCombiner,
                callback,
                repository
        );
    }

    @Override
    public Simulator createSimulator(
            CreateSimulationStateTaskFactory stateTaskFactory,
            SimulationIterationTaskFactory iterationTaskFactory,
            SimulationStateCombiner stateCombiner,
            SimulatorCallback callback,
            SimulationRepository repository,
            SimulationConfig... configs
    ) throws ExecutionException, InterruptedException {
        return new SeriesSimulator(
                stateTaskFactory,
                iterationTaskFactory,
                stateCombiner,
                callback,
                repository,
                configs
        );
    }
}
