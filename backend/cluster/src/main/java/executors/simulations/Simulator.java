package executors.simulations;

public interface Simulator {

    void simulate();
}
