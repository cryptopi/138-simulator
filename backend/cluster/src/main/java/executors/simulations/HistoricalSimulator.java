package executors.simulations;

import simulations.Simulation;
import simulations.SimulationRepository;

import java.util.ArrayList;
import java.util.Collection;

public class HistoricalSimulator implements Simulator {

    private String jobId;
    private String[] groupKeys;
    private SimulatorCallback callback;
    private SimulationRepository simulationRepository;

    public HistoricalSimulator(
            String jobId,
            String[] groupKeys,
            SimulatorCallback callback,
            SimulationRepository simulationRepository
    ) {
        this.jobId = jobId;
        this.groupKeys = groupKeys;
        this.callback = callback;
        this.simulationRepository = simulationRepository;
        if (this.jobId != null && this.groupKeys != null) {
            throw new IllegalArgumentException(
                    "Historical simulation must either be based on jobId " +
                            "or groupKey, not both"
            );
        }
    }

    public void simulate() {
        Collection<Simulation> simulations = null;
        if (jobId != null) {
            simulations = simulationRepository.getSimulationsByJob(jobId);
        } else {
            simulations = new ArrayList<>();
            for (String groupKey : groupKeys) {
                simulations.addAll(simulationRepository.getSimulationsByGroup(groupKey));
            }
        }
        callback.simulatorStarted(simulations);
        for (Simulation simulation : simulations) {
            for (int i = 0; i <= simulation.getConfig().getTotalIterationCount(); i++) {
                callback.stateCompleted(simulation, i);
            }
        }
        callback.simulatorEnded();
    }
}
