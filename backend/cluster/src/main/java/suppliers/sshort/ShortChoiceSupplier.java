package suppliers.sshort;

import config.state.ChoiceGeneratorConfig;
import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;
import suppliers.sint.IntSupplier;

import java.util.ArrayList;
import java.util.List;

public class ShortChoiceSupplier implements ShortSupplier {

    private ChoiceGeneratorConfig config;
    private EnumeratedDistribution<Short> valueDistribution;

    public ShortChoiceSupplier(ChoiceGeneratorConfig config) {
        this.config = config;
        List<Pair<Short, Double>> indexWeights = new ArrayList<>();
        for (int i = 0; i < config.getProbabilities().length; i++) {
            indexWeights.add(new Pair<>(
                    (short)config.getValues()[i],
                    config.getProbabilities()[i]
            ));
        }
        this.valueDistribution = new EnumeratedDistribution<>(indexWeights);
    }

    @Override
    public short get() {
        return valueDistribution.sample();
    }
}
