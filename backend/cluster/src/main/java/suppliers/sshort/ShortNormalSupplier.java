package suppliers.sshort;

import config.state.NormalGeneratorConfig;
import org.apache.commons.math3.distribution.NormalDistribution;

public class ShortNormalSupplier implements ShortSupplier {

    private NormalDistribution distribution;

    public ShortNormalSupplier(NormalGeneratorConfig config) {
        this.distribution =
                new NormalDistribution(config.getMu(), config.getSigma());
    }

    @Override
    public short get() {
        return (short)distribution.sample();
    }
}
