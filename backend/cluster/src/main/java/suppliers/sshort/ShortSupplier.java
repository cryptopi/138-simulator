package suppliers.sshort;

public interface ShortSupplier {

    short get();

    static ShortSupplier singletonSupplier(short value) {
        return () -> value;
    }
}
