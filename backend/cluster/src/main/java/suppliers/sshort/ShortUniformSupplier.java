package suppliers.sshort;

import config.state.UniformGeneratorConfig;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

public class ShortUniformSupplier implements ShortSupplier {

    private UniformIntegerDistribution distribution;

    public ShortUniformSupplier(UniformGeneratorConfig config) {
        this.distribution = new UniformIntegerDistribution(
                (int)config.getLower(),
                (int)config.getUpper()
        );
    }

    @Override
    public short get() {
        return (short)distribution.sample();
    }
}
