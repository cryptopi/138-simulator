package suppliers.sdouble;

import config.state.NormalGeneratorConfig;
import org.apache.commons.math3.distribution.NormalDistribution;
import suppliers.sdouble.DoubleSupplier;

public class NormalSupplier implements DoubleSupplier {

    private NormalDistribution distribution;

    public NormalSupplier(NormalGeneratorConfig config) {
        this.distribution =
                new NormalDistribution(config.getMu(), config.getSigma());
    }

    @Override
    public double get() {
        return distribution.sample();
    }
}
