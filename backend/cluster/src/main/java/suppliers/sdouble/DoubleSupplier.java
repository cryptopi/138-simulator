package suppliers.sdouble;

public interface DoubleSupplier {

    double get();

    static DoubleSupplier singletonSupplier(double value) {
        return () -> value;
    }
}
