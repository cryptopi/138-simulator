package suppliers.sdouble;

import config.state.UniformGeneratorConfig;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import suppliers.sdouble.DoubleSupplier;

public class UniformSupplier implements DoubleSupplier {

    private UniformRealDistribution distribution;

    public UniformSupplier(UniformGeneratorConfig config) {
        this.distribution = new UniformRealDistribution(
                config.getLower(),
                config.getUpper()
        );
    }

    @Override
    public double get() {
        return distribution.sample();
    }
}
