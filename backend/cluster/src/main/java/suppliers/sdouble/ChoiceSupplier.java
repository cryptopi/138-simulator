package suppliers.sdouble;

import config.state.ChoiceGeneratorConfig;
import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class ChoiceSupplier implements DoubleSupplier {

    private ChoiceGeneratorConfig config;
    private EnumeratedDistribution<Double> valueDistribution;

    public ChoiceSupplier(ChoiceGeneratorConfig config) {
        this.config = config;
        List<Pair<Double, Double>> indexWeights = new ArrayList<>();
        for (int i = 0; i < config.getProbabilities().length; i++) {
            indexWeights.add(new Pair<>(
                    config.getValues()[i],
                    config.getProbabilities()[i]
            ));
        }
        this.valueDistribution = new EnumeratedDistribution<>(indexWeights);
    }

    @Override
    public double get() {
        return valueDistribution.sample();
    }
}
