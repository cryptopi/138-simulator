package suppliers.sboolean;

public interface BooleanSupplier {

    boolean get();

    static BooleanSupplier singletonSupplier(boolean value) {
        return () -> value;
    }
}
