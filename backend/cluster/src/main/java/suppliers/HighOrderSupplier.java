package suppliers;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class HighOrderSupplier<F, T> implements Supplier<T> {

    private Supplier<F> from;
    private Function<F, T> converter;

    public HighOrderSupplier(Supplier<F> from, Function<F, T> converter) {
        this.from = from;
        this.converter = converter;
    }

    @Override
    public T get() {
        F original = from.get();
        return converter.apply(original);
    }
}
