package suppliers.sint;

import config.state.ChoiceGeneratorConfig;
import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;
import suppliers.sdouble.DoubleSupplier;

import java.util.ArrayList;
import java.util.List;

public class IntChoiceSupplier implements IntSupplier {

    private ChoiceGeneratorConfig config;
    private EnumeratedDistribution<Integer> valueDistribution;

    public IntChoiceSupplier(ChoiceGeneratorConfig config) {
        this.config = config;
        List<Pair<Integer, Double>> indexWeights = new ArrayList<>();
        for (int i = 0; i < config.getProbabilities().length; i++) {
            indexWeights.add(new Pair<>(
                    (int)config.getValues()[i],
                    config.getProbabilities()[i]
            ));
        }
        this.valueDistribution = new EnumeratedDistribution<>(indexWeights);
    }

    @Override
    public int get() {
        return valueDistribution.sample();
    }
}
