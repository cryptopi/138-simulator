package suppliers.sint;

import mappers.IntIntToIntMapper;

public class IntIntToIntSupplier implements IntSupplier {

    private IntSupplier one;
    private IntSupplier two;
    private IntIntToIntMapper mapper;

    public IntIntToIntSupplier(IntSupplier one, IntSupplier two, IntIntToIntMapper mapper) {
        this.one = one;
        this.two = two;
        this.mapper = mapper;
    }

    @Override
    public int get() {
        return mapper.map(one.get(), two.get());
    }
}
