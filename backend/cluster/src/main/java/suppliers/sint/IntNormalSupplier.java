package suppliers.sint;

import config.state.NormalGeneratorConfig;
import org.apache.commons.math3.distribution.NormalDistribution;

public class IntNormalSupplier implements IntSupplier {

    private NormalDistribution distribution;

    public IntNormalSupplier(NormalGeneratorConfig config) {
        this.distribution =
                new NormalDistribution(config.getMu(), config.getSigma());
    }

    @Override
    public int get() {
        return (int)distribution.sample();
    }
}
