package suppliers.sint;

import config.state.UniformGeneratorConfig;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import suppliers.sint.IntSupplier;

public class IntUniformSupplier implements IntSupplier {

    private UniformIntegerDistribution distribution;

    public IntUniformSupplier(UniformGeneratorConfig config) {
        this.distribution = new UniformIntegerDistribution(
                (int)config.getLower(),
                (int)config.getUpper()
        );
    }

    @Override
    public int get() {
        return distribution.sample();
    }
}
