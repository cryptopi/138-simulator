package suppliers.sint;

public interface IntSupplier {

    int get();

    static IntSupplier singletonSupplier(int value) {
        return () -> value;
    }
}
