import React from 'react';
import Button from '@material-ui/core/Button';
import Main from "./Main.component";

function App({ws}) {
  return (
      <Main ws={ws}/>
  );
}

export default App;
