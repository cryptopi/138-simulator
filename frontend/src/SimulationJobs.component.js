import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from "@material-ui/core/Button";
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import SimulationJobsList from "./SimulationJobsList.component";
import { connect } from 'react-redux';
import CreateSimulationDialog from "./components/CreateSimulationDialog.component";
import { addTemplates } from "./actions/simulations.actions";
import { addVisualizer } from "./actions/visualizations.actions";
import { getTemplateState, getGroupState, getSimulatorState } from "./selectors/simulations";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles(theme => ({
    root: {
        height: '100%'
    },
    noJobs: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex'
    },
}));

const mapStateToProps = state => ({
    templates: getTemplateState(state),
    groups: getGroupState(state),
    simulations: getSimulatorState(state)
});

const actionCreators = {
    addTemplates, addVisualizer
};

export default connect(mapStateToProps, actionCreators)(function SimulationJobs(props) {
    const classes = useStyles();
    const [tabValue, setTabValue] = React.useState(0);
    const [isCreateOpen, setCreateOpen] = React.useState(false);
    const {templates, groups, simulations, send, addTemplates, addVisualizer } = props;

    const handleChange = (event, newValue) => {
        setTabValue(newValue);
    };

    const hasSimulations = templates.all.length > 0;
    const completedTemplates = templates.all.filter(id => {
        const completed = templates.byId[id].completedIterations;
        const total = templates.byId[id].totalIterations;
        return completed === total;
    }).map(id => templates.byId[id]);
    const runningTemplates = templates.all.filter(id => {
        const completed = templates.byId[id].completedIterations;
        const total = templates.byId[id].totalIterations;
        return completed < total;
    }).map(id => templates.byId[id]);

    let tabsComponent = null;
    if (hasSimulations) {
        tabsComponent = (
            <Paper className={classes.root}>
                <AppBar position="static">
                    <Tabs value={tabValue} onChange={handleChange} aria-label="simple tabs example">
                        <Tab label={`Running (${Object.keys(runningTemplates).length})`} {...a11yProps(0)} />
                        <Tab label={`Completed (${Object.keys(completedTemplates).length})`} {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                <TabPanel value={tabValue} index={0}>
                    <SimulationJobsList
                        isRunning={true}
                        templates={runningTemplates}
                        groupMap={groups.byId}
                        simulationMap={simulations.byId}
                        createHandler={() => setCreateOpen(true)}
                    />
                </TabPanel>
                <TabPanel value={tabValue} index={1}>
                    <SimulationJobsList
                        isRunning={false}
                        templates={completedTemplates}
                        groupMap={groups.byId}
                        simulationMap={simulations.byId}
                        createHandler={() => setCreateOpen(true)}
                    />
                </TabPanel>
            </Paper>
        );
    } else {
        tabsComponent = (
            <div className={classes.noJobs}>
                <Button variant="contained" color="primary" onClick={() => setCreateOpen(!isCreateOpen)}>
                    Create a Simulation
                </Button>
            </div>
        );
    }

    const createSubmitted = (request, schema, templateMap, visualizer, visualizationMap, statisticsMap) => {
        setCreateOpen(false);
        addTemplates(templateMap);
        addVisualizer(visualizer, visualizationMap, statisticsMap);
        send(request);
    };

    let createDialogComponent = null;
    if (isCreateOpen) {
        // createDialogComponent = <EditSimulationDialog onSubmit={(simulation => () => console.log(simulation))}/>
        createDialogComponent = <CreateSimulationDialog onSubmit={createSubmitted} onClose={() => setCreateOpen(false)}/>
    }

    return (
        <div className={classes.root}>
            {createDialogComponent}
            {tabsComponent}
        </div>
    );
});