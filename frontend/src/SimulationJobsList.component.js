import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import TemplateJob from "./TemplateJob.component";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 900,
        margin: '0 auto',
    },
    noJobs: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    noJobMessage: {
        marginTop: 30,
        marginBottom: 30
    },
    templateList: {
        marginBottom: 30
    },
    createContainer: {
        textAlign: 'center'
    }
}));

export default function SimulationJobsList(props) {
    const classes = useStyles();
    const {isRunning, templates, groupMap, simulationMap, createHandler} = props;

    const hasTemplates = Object.keys(templates).length > 0;

    let component;
    if (hasTemplates) {
        component = (
            <div className={classes.root}>
                <div className={classes.templateList}>
                    {templates.map(template => (
                        <TemplateJob
                            key={template.id}
                            isRunning={isRunning}
                            template={template}
                            groupMap={groupMap}
                            simulationMap={simulationMap}
                        />
                    ))}
                </div>
                <div className={classes.createContainer}>
                    <Button variant="contained" color="primary" onClick={createHandler}>
                        Create Simulation
                    </Button>
                </div>
            </div>
        );
    } else {
        const phrase = isRunning ? "running" : "completed";
        component = (
            <div className={classes.noJobs}>
                <div className={classes.noJobMessage}>No {phrase} simulations!</div>
                <Button variant="contained" color="primary" onClick={createHandler}>
                    Create Simulation
                </Button>
            </div>
        );
    }

    return component;
}