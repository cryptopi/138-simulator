import { SIMULATOR_PROGRESS, STATISTIC_RESULT} from "../actions/websockets.actions";
import { ADD_VISUALIZER } from "../actions/visualizations.actions";
import { ADD_TEMPLATES } from "../actions/simulations.actions";
import { BATCH } from "../actions/batch.actions";
import { getIterationCount } from "../selectors/visualizations";

const INITIAL_STATE = {
    templates: {}, // map of ID to object of {name, groups (keys), template, iterationCount, completedIterations, completedTime, createdTime}
    groups: {}, // map of key to object of {simulation IDs, completedIterations, iterationCount, schema, templateId}
    simulations: {}, // map of ID to object of {completedIterations, iterationCount, groupKey}
    statistics: {}, // map of ID to object of {values: {map of iteration, value}}
    visualizations: {}, // map of visualization ID to object of {statisticID, config, family}
    visualizers: {} // map of visualizer ID to object of {name, areGroups (bool), map of entityId to visualizationIDs, compare (bool)}
};

const printState = state => {
    console.log(JSON.stringify(state));
};

export const reducer = (state, action) => {
    switch (action.type) {
        case BATCH:
            const { actions } = action;
            for (const action of actions) {
                state = reducer(state, action);
            }
            printState(state);
            return state;
        case SIMULATOR_PROGRESS:
            for (const simulationId in action.simulations) {
                const count = action.simulations[simulationId];
                const groupKey = state.simulations.byId[simulationId].groupKey;
                const templateId = state.groups.byId[groupKey].templateId;
                const justFinished =
                    state.templates.byId[templateId].completedIterations
                    === state.templates.byId[templateId].totalIterations - count;
                state = {
                    ...state,
                    templates: {
                        ...state.templates,
                        byId: {
                            ...state.templates.byId,
                            [templateId]: {
                                ...state.templates.byId[templateId],
                                completedIterations: state.templates.byId[templateId].completedIterations + count
                            }
                        }
                    },
                    groups: {
                        ...state.groups,
                        byId: {
                            ...state.groups.byId,
                            [groupKey]: {
                                ...state.groups.byId[groupKey],
                                completedIterations: state.groups.byId[groupKey].completedIterations + count
                            }
                        }
                    },
                    simulations: {
                        ...state.simulations,
                        byId: {
                            ...state.simulations.byId,
                            [simulationId]: {
                                ...state.simulations.byId[simulationId],
                                completedIterations: state.simulations.byId[simulationId].completedIterations + count
                            }
                        }
                    }
                };
                if (justFinished) {
                    state = {
                        ...state,
                        templates: {
                            ...state.templates,
                            byId: {
                                ...state.templates.byId,
                                [templateId]: {
                                    ...state.templates.byId[templateId],
                                    completedTime: Date.now()
                                }
                            }
                        }
                    }
                }
            }
            printState(state);
            return state;
        case STATISTIC_RESULT:
            for (const statisticId in action.statistics) {
                for (const entityId in action.statistics[statisticId]) {
                    const values = action.statistics[statisticId][entityId];
                    state = {
                        ...state,
                        statistics: {
                            ...state.statistics,
                            byId: {
                                ...state.statistics.byId,
                                [statisticId]: {
                                    ...state.statistics.byId[statisticId],
                                    values: {
                                        ...state.statistics.byId[statisticId].values,
                                        [entityId]: {
                                            ...state.statistics.byId[statisticId].values[entityId],
                                            ...values
                                        }
                                    }
                                }
                            }
                        }
                    };
                }
            }
            printState(state);
            return state;
        case ADD_VISUALIZER:
            const { visualizer, visualizations, statistics} = action;
            state = {
                ...state,
                visualizers: {
                    ...state.visualizers,
                    byId: {
                        ...state.visualizers.byId,
                        [visualizer.id]: {
                            ...visualizer,
                            iterationCount: getIterationCount(visualizer, state.groups.byId, state.simulations.byId)
                        }
                    },
                    all: [
                        ...state.visualizers.all,
                        visualizer.id
                    ]
                }
            };

            state = {
                ...state,
                visualizations: {
                    ...state.visualizations,
                    byId: {
                        ...state.visualizations.byId,
                        ...visualizations
                    },
                    all: {
                        ...state.visualizations.all,
                        ...Object.keys(visualizations)
                    }
                },
                statistics: {
                    ...state.statistics,
                    byId: {
                        ...state.statistics.byId,
                        ...statistics
                    },
                    all: {
                        ...state.statistics.all,
                        ...Object.keys(statistics)
                    }
                }

            };

            printState(state);
            return state;
        case ADD_TEMPLATES:
            for (const templateId in action.templateMap) {
                let totalTemplateIterations = 0;
                const template = action.templateMap[templateId];

                for (const groupKey in template.groups) {
                    const groupInfo = template.groups[groupKey];
                    totalTemplateIterations += groupInfo.iterationCount;
                    groupInfo.templateId = templateId;
                    groupInfo.id = groupKey;
                    groupInfo.completedIterations = 0;
                    state = {
                        ...state,
                        groups: {
                            ...state.groups,
                            byId: {
                                ...state.groups.byId,
                                [groupKey]: groupInfo,
                            },
                            all: [
                                ...state.groups.all,
                                groupKey
                            ]
                        }
                    };

                    for (const simulationId of groupInfo.simulations) {
                        state = {
                            ...state,
                            simulations: {
                                ...state.simulations,
                                byId: {
                                    ...state.simulations.byId,
                                    [simulationId]: {
                                        id: simulationId,
                                        groupKey,
                                        iterationCount: groupInfo.iterationCount / groupInfo.simulations.length,
                                        completedIterations: 0
                                    }
                                },
                                all: [
                                    ...state.simulations.all,
                                    simulationId
                                ]

                            }
                        };
                    }
                }

                state = {
                    ...state,
                    templates: {
                        ...state.templates,
                        byId: {
                            ...state.templates.byId,
                            [templateId]: {
                                ...template,
                                id: templateId,
                                completedIterations: 0,
                                totalIterations: totalTemplateIterations,
                                groups: Object.keys(template.groups),
                                createdTime: Date.now()
                            }
                        },
                        all: [
                            ...state.templates.all,
                            templateId
                        ]
                    }
                };
            }
            printState(state);
            return state;
        default:
            console.log("Unrecognized action: " + JSON.stringify(action));
            return state;
    }
};