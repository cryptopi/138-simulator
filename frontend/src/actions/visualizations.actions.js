export const ADD_VISUALIZER = "ADD_VISUALIZER";

export const addVisualizer = (visualizer, visualizations, statistics) => ({
    type: ADD_VISUALIZER,
    visualizer,
    visualizations,
    statistics
});