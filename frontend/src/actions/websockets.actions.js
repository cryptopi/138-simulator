export const SIMULATOR_PROGRESS = "SIMULATOR_PROGRESS";
export const STATISTIC_RESULT = "STATISTIC_RESULT";

export const receiveSimulatorProgressEmpty = () => ({
    type: SIMULATOR_PROGRESS,
    simulations: {}
});

export const addToSimulatorReceiveProgress = (dest, src) => {
    for (const simulationId in src.simulations) {
        const prev = dest.simulations[simulationId] || 0;
        dest.simulations[simulationId] = prev + src.simulations[simulationId];
    }
    return dest;
};

export const receiveSimulatorProgress = (simulationId, deltaIterations) => ({
    type: SIMULATOR_PROGRESS,
    simulations: {
        [simulationId]: deltaIterations
    },
});

export const addToReceiveStatistic = (dest, src) => {
    for (const statisticId in src.statistics) {
        dest.statistics[statisticId] = dest.statistics[statisticId] || {};
        for (const entityId in src.statistics[statisticId]) {
            dest.statistics[statisticId][entityId] = {
                ...dest.statistics[statisticId][entityId],
                ...src.statistics[statisticId][entityId]
            }
        }
    }
    return dest;
};

export const receiveStatisticEmpty = () => ({
    type: STATISTIC_RESULT,
    statistics: {}
});

export const receiveStatistic = (statisticId, entityId, iteration, value) => ({
    type: STATISTIC_RESULT,
    statistics: {
        [statisticId]: {
            [entityId]: {
                [iteration]: value
            }
        }
    }
});
