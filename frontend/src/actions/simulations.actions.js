export const ADD_TEMPLATES = "ADD_TEMPLATES";

export const addTemplates = templateMap => ({
   type: ADD_TEMPLATES,
   templateMap
});