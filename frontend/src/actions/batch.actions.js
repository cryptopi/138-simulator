export const BATCH = "BATCH";

export const addToBatch = (batch, action) => {
    batch.actions.push(action);
    return batch;
};

export const batch = (...actions) => ({
    type: BATCH,
    actions
});