export const resolveForeignKeys = (obj, foreignKeys, tables) => {
    if (foreignKeys.length === 0) {
        return obj;
    }
    const foreignKeyName = foreignKeys[0];
    foreignKeys = foreignKeys.slice(1);
    const table = tables[0];
    tables = tables.slice(1);
    const allResolved = [];
    for (const foreignKey of obj[foreignKeyName]) {
        const foreignValue = table[foreignKey];
        const resolved = resolveForeignKeys(foreignValue, foreignKeys, tables);
        resolved.id = foreignKey;
        allResolved.push(resolved);
    }
    obj = {
        ...obj,
        [foreignKeyName]: allResolved
    };
    return obj;
};

export const resolveForeignKeysFromState = (state, foreignKeys, tables) => {
    const resolvedState = {...state, byId: {}};
    Object.keys(state.byId).forEach(key => {
        resolvedState.byId[key] = resolveForeignKeys(
            state.byId[key],
            foreignKeys,
            tables
        );
    });
    return resolvedState;
};
