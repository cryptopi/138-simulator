import {createSelector} from "reselect";
import {getGroupState, getSimulatorState} from "./simulations";

export const getVisualizersState = state => state.visualizers;
export const getVisualizationsState = state => state.visualizations;
export const getStatisticsState = state => state.statistics;

const completedStatistics = new Set();
const isStatisticCompleted = (statisticId, statistic, forEntityIds, iterationCount) => {
    if (completedStatistics.has(statisticId)) {
        return true;
    }
    const completed = forEntityIds.every(
        id => statistic.values[id]
            && Object.keys(statistic.values[id]).length >= iterationCount + 1
    );

    if (completed) {
        completedStatistics.add(statisticId);
    }
    return completed;
};

const completedVisualizations = new Set();
export const isVisualizationCompleted = (visualizationId,
                                  visualization,
                                  forEntityIds,
                                  statisticMap,
                                  iterationCount) => {
    if (completedVisualizations.has(visualizationId)) {
        return true;
    }
    const isCompleted = Object.values(visualization.statistics).every(
        statisticId => isStatisticCompleted(
            statisticId, statisticMap[statisticId], forEntityIds, iterationCount
        )
    );
    if (isCompleted) {
        completedVisualizations.add(visualizationId);
    }
    return isCompleted;
};

export const getIterationCount = (visualizer, groupMap, simulationMap) => {
    let getEntityIterationCount;
    if (visualizer.areGroups) {
        getEntityIterationCount = id => groupMap[id].schema.totalIterationCount;
    } else {
        getEntityIterationCount = id => simulationMap[id].iterationCount;
    }
    return Math.min(...visualizer.entityIds.map(id => getEntityIterationCount(id)));
};

export const getVisualizerGroups = (visualizer, simulationMap) => {
    if (visualizer.areGroups) {
        return visualizer.entityIds;
    } else {
        return Array.from(new Set(visualizer.entityIds.map(
            simulationId => simulationMap[simulationId].groupKey
        )));
    }
};

const completedVisualizers = new Set();
const isVisualizerCompleted = (visualizerId, visualizer, visualizations, statistics, groups, simulations) => {
    if (completedVisualizers.has(visualizerId)) {
        return true;
    }
    let iterationCount = visualizer.iterationCount;
    const isCompleted = Object.values(visualizer.visualizations).flat().every(
        visualizationId => isVisualizationCompleted(
            visualizationId,
            visualizations.byId[visualizationId],
            visualizer.entityIds,
            statistics.byId,
            iterationCount
        )
    );
    if (isCompleted) {
        completedVisualizers.add(visualizerId);
    }
    return isCompleted;
};

export const getCompletedVisualizers = createSelector(
    [getVisualizersState, getVisualizationsState, getStatisticsState, getGroupState, getSimulatorState],
    (visualizers, visualizations, statistics, groups, simulations) => visualizers.all.filter(
        id => isVisualizerCompleted(id, visualizers.byId[id], visualizations, statistics, groups, simulations)
    )
);