import { createSelector } from "reselect";
import { resolveForeignKeysFromState } from "./utils";

const getFullGroups = (groups, simulationMap) => {
    return resolveForeignKeysFromState(
        groups,
        ["simulations"],
        [simulationMap]
    )
};

// SELECTORS

export const getTemplateState = state => state.templates;
export const getGroupState = state => state.groups;
export const getSimulatorState = state => state.simulations;

export const getFullTemplates = createSelector(
    [getTemplateState, getGroupState, getSimulatorState],
    (templates, groups, simulations) => {
        resolveForeignKeysFromState(
            templates,
            ["groups", "simulations"],
            [groups.byId, simulations.byId]
        );
    }
);