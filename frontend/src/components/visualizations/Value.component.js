import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import {Slider} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { TOP_PERCENT_WEALTH } from "../../constants/types";

const useStyles = makeStyles(theme => ({
    root: {
    },
    error: {

    }
}));

export default function Value(props) {
    const classes = useStyles();
    const {visualization, iteration, entityIds, legend} = props;

    if (entityIds.length > 1) {
        return (
            <div className={classes.error}>
                <Typography>Cannot render value for more than one entity</Typography>
            </div>
        )
    }


    const statistic = visualization.statistics.root;
    const value = statistic.values[entityIds[0]][iteration];

    let inner = null;
    if (statistic.type === TOP_PERCENT_WEALTH) {
        inner = (
            <>
                <span>Top {(value * 100).toFixed(2)}% have {statistic.wealthPercent}% of the wealth</span>
            </>
        )
    } else {
        inner = <Typography>{value}</Typography>
    }

    return (
        <div>
            {inner}
        </div>
    )
}
