import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import { PIE, HISTOGRAM, SCATTER, LINE, VALUE } from "../../constants/types";
import {CircularProgress} from "@material-ui/core";
import {cloneDeep} from "lodash";
import Value from "./Value.component";

const styles = theme => ({
    root: {
        width: '100%',
        flex: 1,
        padding: 8
    },
    notReady: {

    }
});

const componentClassMap = {
    // PIE: Pie,
    // HISTOGRAM: Histogram,
    // SCATTER: Scatter,
    // LINE: Line,
    VALUE: Value
};

class PerIterationVisualization extends React.Component {

    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.ready !== this.props.ready ||
            nextProps.visualization !== this.props.visualization ||
            nextProps.entityIds !== this.props.entityIds ||
            nextProps.legend !== this.props.legend ||
            nextProps.iteration !== this.props.iteration;
    }

    render() {
        const {classes, visualization, entityIds, legend, iteration, ready, statisticsMap} = this.props;
        // console.log(this.props);

        console.log("Per iteration visualization rendering");

        const denormalizedVisualization = {...visualization};
        denormalizedVisualization.statistics = {};
        Object.keys(visualization.statistics).forEach(name => {
            const statisticId = visualization.statistics[name];
            // TODO could also prep statistic by filtering out only iteration data, etc.
            const statistic = statisticsMap[statisticId];
            denormalizedVisualization.statistics[name] = statistic;
        });

        let inner = null;

        if (!ready) {
            inner = (
                <div className={classes.notReady}>
                    <CircularProgress/>
                </div>
            )
        }

        if (visualization.type in componentClassMap) {
            const ComponentClass = componentClassMap[visualization.type];
            inner = <ComponentClass
                visualization={denormalizedVisualization}
                iteration={iteration}
                entityIds={entityIds}
                legend={legend}
            />
        } else {
            throw new Error(`Per iteration visualization type ${visualization.type} is not recognized`);
        }

        return (
            <Paper className={classes.root}>
                {inner}
            </Paper>
        )
    }
}

export default withStyles(styles)(PerIterationVisualization);
