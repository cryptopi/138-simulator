import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import {Slider} from "@material-ui/core";
import PerIterationVisualization from "./PerIterationVisualization.component";
import { isVisualizationCompleted } from "../../selectors/visualizations";
import Typography from "@material-ui/core/Typography";
import {HISTOGRAM, VALUE, SCATTER, LINE, PIE} from "../../constants/types";
import RGL, { WidthProvider } from "react-grid-layout";

const ReactGridLayout = WidthProvider(RGL);

const useStyles = makeStyles(theme => ({
    root: {
    },
    error: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    visualizationContainer: {
        display: 'flex',
        flexDirection: 'column'
    }
}));

const COLUMN_COUNT = 40;
const ROW_HEIGHT = 20;
const DEFAULT_WIDTH = 10;
const DEFAULT_HEIGHT = 10;
const visualizationSizeMap = {
    HISTOGRAM: [DEFAULT_WIDTH, DEFAULT_HEIGHT],
    VALUE: [DEFAULT_WIDTH / 2, DEFAULT_HEIGHT],
    SCATTER: [DEFAULT_WIDTH, DEFAULT_HEIGHT],
    LINE: [DEFAULT_WIDTH, DEFAULT_HEIGHT],
    PIE: [DEFAULT_WIDTH, DEFAULT_HEIGHT]
};

const getVisualizationSize = type => {
    return visualizationSizeMap[type];
};

export default function PerIterationVisualizations(props) {
    const classes = useStyles();
    const {visualizations, entityIds, iteration, visualizationsMap, statisticsMap, totalIterations, legend} = props;

    if (entityIds.length === 0) {
        return (
            <div className={classes.error}>
                <Typography>Filter must match at least one group</Typography>
            </div>
        );
    }

    let x = 0;
    let y = 0;
    let maxHeight = 0;
    let layout = [];
    const components = visualizations.map(id => {
        const visualization = visualizationsMap[id];
        const ready = isVisualizationCompleted(
            id, visualization, entityIds, statisticsMap, totalIterations
        );
        const [w, h] = getVisualizationSize(visualization.type);

        // Layout info
        maxHeight = Math.max(maxHeight, h);
        let newX = x + w;
        if (newX > COLUMN_COUNT) {
            x = 0;
            y += maxHeight;
            maxHeight = 0;
            newX = w;
        }
        const componentLayout = {i: id, x, y, w, h};
        layout.push(componentLayout);

        const component = (
            <div key={id} className={classes.visualizationContainer}>
                <PerIterationVisualization
                    visualization={visualization}
                    entityIds={entityIds}
                    legend={legend}
                    iteration={iteration}
                    ready={ready}
                    statisticsMap={statisticsMap}
                />
            </div>
        );
        x = newX;
        return component;

    });

    return (
        <ReactGridLayout layout={layout} cols={COLUMN_COUNT} rowHeight={ROW_HEIGHT}>
            {components}
        </ReactGridLayout>
    )
}
