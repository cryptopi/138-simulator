import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from "@material-ui/core/Button";
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import SimulationJobsList from "../SimulationJobsList.component";
import { connect } from 'react-redux';
import {Collapse, Dialog, DialogContent, DialogTitle} from "@material-ui/core";
import DialogActions from "@material-ui/core/DialogActions";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Drawer from "@material-ui/core/Drawer";
import SimulationIcon from "@material-ui/icons/Person";
import StatisticsIcon from "@material-ui/icons/Assessment";
import GeneralInitialStateConfig from './initial-state/GeneralInitialStateConfig.component';
import GeneratorConfig from './initial-state/GeneratorConfig.component';
import LocationGeneratorConfig from './initial-state/LocationGeneratorConfig.component';
import { cloneDeep } from 'lodash';

const drawerWidth = 200;

const useStyles = makeStyles(theme => ({
    dialogContentRoot: {
        display: 'flex',
        overflowY: 'hidden'
    },
    contentContainer: {
        display: 'flex',
        overflowY: 'hidden'
    },
    navigation: {
        overflowY: 'auto',
        minWidth: drawerWidth,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    mainDivider: {
        margin: '0 20px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        overflowY: 'auto'
    }
}));

export default function EditSimulationDialog(props) {
    const classes = useStyles();
    const [openLists, setOpenLists] = React.useState(new Set(['initialstate']));
    const [contentName, setContentName] = React.useState('initialstate/agentlocations');
    const [formValue, setFormValue] = React.useState(props.previousFormValue || {
        initialState: {
            general: {},
            skillGenerator: {},
            agentLocations: {},
            eventLocations: {},
            eventMagnitudes: {}
        }
    });
    const [completeSections, setCompleteSections] = React.useState(new Set());


    const { onSubmit, onClose, previousFormValue } = props;

    const listOpened = name => {
        let newSet = new Set(openLists);
        newSet.add(name);
        setOpenLists(newSet);
    };

    const listClosed = name => {
        let newSet = new Set(openLists);
        newSet.delete(name);
        setOpenLists(newSet);
    };

    const listToggled = name => {
        if (openLists.has(name)) {
            listClosed(name);
        } else {
            listOpened(name);
        }
    };

    const listClicked = name => {
        setContentName(name);
    };

    const sectionUpdated = (setter, key, value, completed) => {
        let newFormValue = cloneDeep(formValue);
        setter(newFormValue, value);
        setFormValue(newFormValue);
        let newCompleted = new Set(completeSections);
        if (completed) {
            newCompleted.add(key);
        } else {
            newCompleted.delete(key);
        }
        setCompleteSections(newCompleted);
    };

    const makeSetter = (keys) => {
        return (formValue, value) => {
            for (let i = 0; i < keys.length - 1; i++) {
                formValue = formValue[keys[i]];
            }
            formValue[keys[keys.length - 1]] = value;
        };
    };

    const isCreating = !previousFormValue;
    const title = isCreating ? "Create Simulation Job" : "View Simulation Job";
    const readonly = !isCreating;

    let contentComponent = null;
    let contentTitle = null;
    switch (contentName) {
        case 'initialstate/general':
            contentTitle = 'General';
            contentComponent = (
                <GeneralInitialStateConfig
                    key="general"
                    value={formValue['initialState']['general']}
                    onUpdate={(value, isCompleted) => sectionUpdated(makeSetter(['initialState', 'general']), 'initialstate/general', value, isCompleted)}
                />
            );
            break;
        case 'initialstate/skills':
            contentTitle = 'Skills';
            contentComponent = (
                <GeneratorConfig
                    key="skills"
                    value={formValue['initialState']['skillGenerator']}
                    onUpdate={(value, isCompleted) => sectionUpdated(makeSetter(['initialState', 'skillGenerator']), 'initialstate/skills', value, isCompleted)}
                />
            );
            break;
        case 'initialstate/agentlocations':
            contentTitle = 'Agent Locations';
            contentComponent = (
                <LocationGeneratorConfig
                    key="agent"
                    value={formValue['initialState']['agentLocations']}
                    onUpdate={(value, isCompleted) => sectionUpdated(makeSetter(['initialState', 'agentLocations']), 'initialstate/agentlocations', value, isCompleted)}
                />
            );
            break;
        case 'initialstate/eventlocations':
            contentTitle = 'Event Locations';
            contentComponent = (
                <LocationGeneratorConfig
                    key="eventlocations"
                    value={formValue['initialState']['eventLocations']}
                    onUpdate={(value, isCompleted) => sectionUpdated(makeSetter(['initialState', 'eventLocations']), 'initialstate/eventlocations', value, isCompleted)}
                />
            );
            break;
        case 'initialstate/eventtypes':
            contentTitle = 'Event Types';
            contentComponent = (
                <GeneratorConfig
                    key="eventtypes"
                    value={formValue['initialState']['eventMagnitudes']}
                    onUpdate={(value, isCompleted) => sectionUpdated(makeSetter(['initialState', 'eventMagnitudes']), 'initialstate/eventmagnitudes', value, isCompleted)}
                />
            );
            break;

    };

    return (
        <Dialog open={true} onClose={onClose}>
            <DialogTitle id="dialog-title">{title}</DialogTitle>
            <DialogContent dividers={true} className={classes.dialogContentRoot}>
                <div className={classes.contentContainer}>
                    <div className={classes.navigation}>
                        <List>
                            <ListItem button key="initialstate" onClick={() => listToggled('initialstate')}>
                                <ListItemText>Initial State</ListItemText>
                            </ListItem>
                            <Collapse in={openLists.has('initialstate')} timeout="auto" unmountOnExit>
                                <List component="div" disablePadding>
                                    {['General', 'Skills', 'Agent Locations', 'Event Locations', 'Event Types'].map((text, index) => {
                                        const key = text.toLowerCase().replace(/\s/g, '');
                                        return (
                                            <ListItem button className={classes.nested} key={key} onClick={() => listClicked(`initialstate/${key}`)}>
                                                <ListItemText primary={text}/>
                                            </ListItem>
                                        );
                                    })}
                                </List>
                            </Collapse>
                        </List>
                    </div>
                    <Divider className={classes.mainDivider} orientation="vertical" flexItem/>
                    <main className={classes.content}>
                        <Typography variant="h6" gutterBottom>{contentTitle}</Typography>
                        {contentComponent}
                    </main>
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Cancel
                </Button>
                <Button variant="contained" onClick={onSubmit(formValue)} color="primary">
                    Create
                </Button>
            </DialogActions>
        </Dialog>
    );
};