import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MultiNumericInput from "../utils/MultiNumericInput.component";

const useStyles = makeStyles(theme => ({
    formRoot: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%'
        }
    }
}));

export default function Component(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(props.value);
    const { onUpdate } = props;

    const isComplete = (value) => {
        return 'agentCount' in value && 'eventCount' in value && 'gridBounds' in value;
    };

    const setKeyValue = (key, keyValue) => {
        let newValue = {};
        if (value) {
            newValue = {...value};
        }
        newValue[key] = keyValue;
        setValue(newValue);
        onUpdate(newValue, isComplete(newValue));
    };

    return (
        <div>
            <form noValidate autoComplete="off" className={classes.formRoot}>
                <MultiNumericInput value={value.agentCount} callback={(value) => setKeyValue('agentCount', value)} label="Agent Count" isInteger={true} min={20}/>
                <MultiNumericInput value={value.eventCount} callback={(value) => setKeyValue('eventCount', value)} label="Event Count" isInteger={true} min={20}/>
                <MultiNumericInput value={value.gridBounds} callback={(value) => setKeyValue('gridBounds', value)} label="Grid Bounds" isInteger={true} min={20}/>
            </form>
        </div>
    );
};