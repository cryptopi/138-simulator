import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MultiNumericInput from "../utils/MultiNumericInput.component";

const useStyles = makeStyles(theme => ({
    formRoot: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%'
        }
    }
}));

export default function Component(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(props.value);
    const { onUpdate } = props;

    const isComplete = (value) => {
        return 'lower' in value && 'upper' in value;
    };

    const setKeyValue = (key, keyValue) => {
        let newValue = {};
        if (value) {
            newValue = {...value};
        }
        newValue[key] = keyValue;
        setValue(newValue);
        onUpdate(newValue, isComplete(newValue));
    };

    return (
        <div>
            <Typography variant="subtitle1" gutterBottom>Uniform Distribution</Typography>
            <form noValidate autoComplete="off" className={classes.formRoot}>
                <MultiNumericInput value={value.lower} callback={(value) => setKeyValue('lower', value)} label="Lower Bound" min={0}/>
                <MultiNumericInput value={value.upper} callback={(value) => setKeyValue('upper', value)} label="Upper Bound" min={0}/>
            </form>
        </div>
    );
};