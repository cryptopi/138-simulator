import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MultiNumericInput from "../utils/MultiNumericInput.component";

const useStyles = makeStyles(theme => ({
    formRoot: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%'
        }
    }
}));

export default function Component(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(props.value);
    const { onUpdate } = props;

    const isComplete = (value) => {
        return 'mu' in value && 'sigma' in value;
    };

    const setKeyValue = (key, keyValue) => {
        let newValue = {};
        if (value) {
            newValue = {...value};
        }
        newValue[key] = keyValue;
        setValue(newValue);
        onUpdate(newValue, isComplete(newValue));
    };

    return (
        <div>
            <Typography variant="subtitle1" gutterBottom>Normal Distribution</Typography>
            <form noValidate autoComplete="off" className={classes.formRoot}>
                <MultiNumericInput value={value.mu} callback={(value) => setKeyValue('mu', value)} label="Mu" min={0}/>
                <MultiNumericInput value={value.sigma} callback={(value) => setKeyValue('sigma', value)} label="Sigma" min={0}/>
            </form>
        </div>
    );
};