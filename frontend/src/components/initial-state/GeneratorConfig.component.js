import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MultiNumericInput from "../utils/MultiNumericInput.component";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ButtonMenu from "../utils/ButtonMenu.component";
import ChoiceGeneratorConfig from "./ChoiceGeneratorConfig.component";
import UniformGeneratorConfig from "./UniformGeneratorConfig.component";
import NormalGeneratorConfig from "./NormalGeneratorConfig.component";

const useStyles = makeStyles(theme => ({
    chooseContainer: {
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        width: 320
    },
    removeContainer: {
        textAlign: 'center',
        marginTop: 20
    },
    config: {
        width: 320
    }
}));

export default function Component(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(props.value);
    const [complete, setComplete] = React.useState(false);
    const { onUpdate } = props;
    console.log(`Generator has value ${JSON.stringify(value)}`)

    const setKeyValue = (key, keyValue) => {
        let newValue = {};
        if (value) {
            newValue = {...value};
        }
        newValue[key] = keyValue;
        setValue(newValue);
        onUpdate(newValue, complete);
    };

    const remove = () => {
        const newValue = {};
        setValue(newValue);
        setComplete(false);
        onUpdate(newValue, false);
    };

    const configUpdated = (value, complete) => {
        setComplete(complete);
        setValue(value);
        onUpdate(value, complete)
    };

    const generatorType = value.type;
    let component = null;
    if (!generatorType) {
        const options = [{key: 'choice', title: 'Choice'}, {key: 'uniform', title: 'Uniform'}, {key: 'normal', title: 'Normal'}];
        component = (
            <div className={classes.chooseContainer}>
                <ButtonMenu buttonTitle="Choose" onUpdate={(value) => setKeyValue('type', value)} options={options} value={value}/>
            </div>
        );
    } else if (generatorType === 'choice') {
        component = (
            <ChoiceGeneratorConfig onUpdate={configUpdated} value={value} className={classes.config}/>
        );
    } else if (generatorType === 'uniform') {
        component = (
            <UniformGeneratorConfig onUpdate={configUpdated} value={value} className={classes.config}/>
        );
    } else if (generatorType === 'normal') {
        component = (
            <NormalGeneratorConfig onUpdate={configUpdated} value={value} className={classes.config}/>
        );
    }

    if (generatorType) {
        component = (
            <div className={classes.contentContainer}>
                {component}
                <div className={classes.removeContainer}>
                    <Button color="primary" onClick={remove}>Remove</Button>
                </div>
            </div>
        )
    }

    return component;
};