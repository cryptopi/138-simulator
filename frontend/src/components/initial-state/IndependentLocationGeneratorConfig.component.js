import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MultiNumericInput from "../utils/MultiNumericInput.component";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import GeneratorConfig from "./GeneratorConfig.component";
import { cloneDeep } from "lodash";

const useStyles = makeStyles(theme => ({
    formRoot: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%'
        }
    },
    radioContainer: {
        justifyContent: 'space-between'
    }
}));

export default function Component(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(props.value);
    const [completedComponents, setCompletedComponents] = React.useState(new Set());
    const [radioValue, setRadioValue] = React.useState('xGenerator');
    const [complete, setComplete] = React.useState(false);
    const { onUpdate } = props;

    if (!('xGenerator' in value)) {
        value.xGenerator = {}
    }
    if (!('yGenerator' in value)) {
        value.yGenerator = {};
    }

    const radioSelected  = event => {
        const coordinate = event.target.value;
        setRadioValue(coordinate)
    };
    console.log('Rendering with radio value ' + radioValue);
    console.log(value[radioValue]);

    const componentUpdated = (name) => (componentValue, isCompleted) => {
        let newValue = cloneDeep(value);
        newValue[name] = componentValue;
        console.log(newValue);
        setValue(newValue);
        let newCompletedComponents = new Set(completedComponents);
        if (isCompleted) {
            newCompletedComponents.add(name);
        } else {
            newCompletedComponents.delete(name);
        }
        setCompletedComponents(newCompletedComponents);
        onUpdate(newValue, newCompletedComponents.size === 2);
    };

    let coordinateComponent = null;
    if (radioValue === 'xGenerator') {
        coordinateComponent = (
            <GeneratorConfig
                key={radioValue}
                value={value[radioValue]}
                onUpdate={componentUpdated(radioValue)}
            />
        );
    } else {
        coordinateComponent = (
            <GeneratorConfig
                key={radioValue}
                value={value[radioValue]}
                onUpdate={componentUpdated(radioValue)}
            />
        )
    }


    return (
        <div>
            <Typography variant="subtitle1" gutterBottom>Independent Coordinates</Typography>
            <RadioGroup aria-label="position" name="position" value={radioValue} onChange={radioSelected} row className={classes.radioContainer}>
                <FormControlLabel
                    value="xGenerator"
                    control={<Radio color="primary" />}
                    label="X"
                    labelPlacement="top"
                />
                <FormControlLabel
                    value="yGenerator"
                    control={<Radio color="primary" />}
                    label="Y"
                    labelPlacement="top"
                />
            </RadioGroup>
            {coordinateComponent}
        </div>
    );
};