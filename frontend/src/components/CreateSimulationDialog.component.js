import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";
import {Collapse, Dialog, DialogContent, DialogTitle} from "@material-ui/core";
import DialogActions from "@material-ui/core/DialogActions";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import GeneralInitialStateConfig from './initial-state/GeneralInitialStateConfig.component';
import GeneratorConfig from './initial-state/GeneratorConfig.component';
import LocationGeneratorConfig from './initial-state/LocationGeneratorConfig.component';
import { cloneDeep } from 'lodash';
import TextField from "@material-ui/core/TextField";
import { parseSchema } from '../schema';

const drawerWidth = 200;

const useStyles = makeStyles(theme => ({
    field: {
        width: '100%'
    }
}));

export default function CreateSimulationDialog(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState('');
    const { onClose, onSubmit } = props;

    const handleChange = event => {
        setValue(event.target.value);
    };

    const create = () => {
        const {request, schema, templateMap, visualizer, visualizationMap, statisticsMap} = parseSchema(value);
        console.log("Request: ", request);
        console.log("Visualizer: ", visualizer);
        console.log("Visualization map: ", visualizationMap);
        console.log("Statistics map: ", statisticsMap);
        onSubmit(request, schema, templateMap, visualizer, visualizationMap, statisticsMap)
    };

    return (
        <Dialog open={true} onClose={onClose} fullWidth={true}>
            <DialogTitle id="dialog-title">Create Simulation Job</DialogTitle>
            <DialogContent dividers={true}>
                <TextField
                    id="outlined-multiline-flexible"
                    label="Job Schema"
                    multiline
                    value={value}
                    onChange={handleChange}
                    variant="outlined"
                    className={classes.field}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    Cancel
                </Button>
                <Button variant="contained" onClick={create} color="primary">
                    Submit
                </Button>
            </DialogActions>
        </Dialog>
    );
};