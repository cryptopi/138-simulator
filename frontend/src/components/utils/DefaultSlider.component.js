import React from 'react';
import Slider from "@material-ui/core/Slider";


export default class DefaultSlider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: props.initialValue};
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        if (this.state.value === null) {
            this.onChange(null, this.props.values[0]);
        }
    }

    onChange(event, value) {
        this.setState(() => ({value}));
        this.props.onChangeCommitted(event, value);
    }

    render() {
        const {values, initialValue, ...rest} = this.props;
        return (
            <Slider
                {...rest}
                value={this.state.value}
                min={values[0]}
                max={values[values.length - 1]}
                onChangeCommitted={this.onChange}
            />
        );
    }
}