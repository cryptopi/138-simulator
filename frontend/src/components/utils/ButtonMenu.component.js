import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MultiNumericInput from "../utils/MultiNumericInput.component";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles(theme => ({
    formRoot: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%'
        }
    }
}));

export default function Component(props) {
    const classes = useStyles();
    const [anchorLocation, setAnchorLocation] = React.useState(null);
    const { buttonTitle, onUpdate, options } = props;

    const handleClick = event => {
        setAnchorLocation(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorLocation(null);
    };

    const handleTypeSelected = type => () => {
        onUpdate(type);
        handleClose();
    };

    return (
        <>
            <Button variant="contained" color="secondary" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                {buttonTitle}
            </Button>
            <Menu
                anchorEl={anchorLocation}
                keepMounted
                open={Boolean(anchorLocation)}
                onClose={handleClose}
            >
                {options.map((elem, index) => {
                    return <MenuItem key={elem.key} onClick={handleTypeSelected(elem.key)}>{elem.title}</MenuItem>
                })}
            </Menu>
        </>
    );
};