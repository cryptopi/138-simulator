import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {TextField} from "@material-ui/core";


const useStyles = makeStyles(theme => ({
    root: {

    },
}));

export default function Component(props) {
    const classes = useStyles();
    const [valid, setValid] = React.useState(true);
    const [value, setValue] = React.useState(props.value || '');
    const [helpText, setHelpText] = React.useState('');
    const { isInteger, callback, label, validator, min, max, isPositive} = props;

    const onChange = event => {
        const newValue = event.target.value;
        setValue(newValue);
    };

    const makeRange = (start, stop, stepSize) => {
        let values = [];
        for (let i = start; i <= stop; i += stepSize) {
            values.push(i);
        }
        return values;
    };

    const parseNumber = str => {
        const parsed = parseFloat(str);
        if (isNaN(parsed)) {
            return ['Invalid number'];
        }
        if (!isFinite(parsed)) {
            return ['Infinity'];
        }
        if (isInteger && !Number.isInteger(parsed)) {
            return ['Must be integer'];
        }
        if (min !== undefined && parsed < min) {
            return [`Must be greater than ${min}`];
        }
        if (max !== undefined && parsed > max) {
            return [`Must be greater than ${max}`];
        }
        let customErrorMessage = '';
        if (validator) {
            customErrorMessage = validator(parsed);
        }
        if (customErrorMessage.length > 0) {
            return [customErrorMessage];
        }
        return ['', parsed];
    };

    const validate = (value) => {
        const isAlreadyNumber = !!value.toFixed;
        if (isAlreadyNumber) {
            const [errorMessage, parsed] = parseNumber(value);
            if (errorMessage.length > 0) {
                return [false, errorMessage];
            }
            return [true, '', parsed];
        }
        const noSpaces = value.replace(/\s/g, '');
        const isRange = noSpaces.indexOf('->') >= 0;
        const isList = !isRange && noSpaces.indexOf(',') >= 0;
        if (isRange) {
            const parts = noSpaces.split('->');
            const startOfRange = parts[0];
            const afterArrow = parts[1];
            const afterArrowParts = afterArrow.split(',')
            if (afterArrowParts.length !== 2) {
                return [false, 'Missing end of range'];
            }
            const endOfRange = afterArrowParts[0];
            const stepSize = afterArrowParts[1];
            const testValues = makeRange(parseFloat(startOfRange));
            const toParse = [startOfRange, endOfRange, stepSize];
            const parsed = [];
            for (let i = 0; i < toParse.length; i++) {
                const [errorMessage, parsedValue] = parseNumber(toParse[i]);
                if (errorMessage.length > 0) {
                    return [false, errorMessage];
                }
                parsed[i] = parsedValue;
            }
            const [parsedStart, parsedEnd, parsedStepSize] = parsed;
            const range = makeRange(parsedStart, parsedEnd, parsedStepSize);
            return [true, '', range];
        }
        if (isList) {
            const parts = noSpaces.split(',');
            const parsed = [];
            for (let i = 0; i < parts.length; i++) {
                const [errorMessage, parsedValue] = parseNumber(parts[i]);
                if (errorMessage.length > 0) {
                    return [false, errorMessage];
                }
                parsed[i] = parsedValue;
            }
            return [true, '', parsed];
        }
        const [errorMessage, parsedValue] = parseNumber(noSpaces);
        if (errorMessage.length > 0) {
            return [false, errorMessage];
        }
        return [true, '', parsedValue];
    };

    const onBlur = () => {
        const [isValid, errorMessage, parsedValue] = validate(value);
        if (isValid) {
            setValue(parsedValue);
            setHelpText('');
            callback(parsedValue);
        } else {
            setHelpText(errorMessage);
        }
        setValid(isValid);
    };

    return (
        <TextField
            error={!valid}
            onChange={onChange}
            onBlur={onBlur}
            value={value}
            variant="standard"
            label={label}
            helperText={helpText}
        />
    )
};