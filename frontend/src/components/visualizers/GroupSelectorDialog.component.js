import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { createFilter } from "../../utils/object-filter";
import GroupSelectorTree from "./GroupSelectorTree.component";
import GroupSelectorResolver from "./resolvers/GroupSelectorResolver.component";
import { addType } from "../../utils/types";
import {Dialog, DialogContent, DialogTitle} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Draggable from 'react-draggable';
import Paper from "@material-ui/core/Paper";
import { ResizableBox } from 'react-resizable';

const useStyles = makeStyles(theme => ({
    root: {
        pointerEvents: "none"
    },
    paper: {
        pointerEvents: "auto"
    },
    dialogTitle: {
        cursor: "move"
    },
    resizable: {
        position: "relative",
        display: "flex",
        flexDirection: "column",
        "& .react-resizable-handle": {
            position: "absolute",
            width: 20,
            height: 20,
            bottom: 0,
            right: 0,
            background:
                "url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2IDYiIHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiNmZmZmZmYwMCIgeD0iMHB4IiB5PSIwcHgiIHdpZHRoPSI2cHgiIGhlaWdodD0iNnB4Ij48ZyBvcGFjaXR5PSIwLjMwMiI+PHBhdGggZD0iTSA2IDYgTCAwIDYgTCAwIDQuMiBMIDQgNC4yIEwgNC4yIDQuMiBMIDQuMiAwIEwgNiAwIEwgNiA2IEwgNiA2IFoiIGZpbGw9IiMwMDAwMDAiLz48L2c+PC9zdmc+')",
            "background-position": "bottom right",
            padding: "0 3px 3px 0",
            "background-repeat": "no-repeat",
            "background-origin": "content-box",
            "box-sizing": "border-box",
            cursor: "se-resize"
        }
    }
}));

function DialogPaper(props) {
    return (
        <Draggable handle="#dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper style={{maxWidth: "none"}} {...props} />
        </Draggable>
    )
}

export default function GroupSelectorDialog(props) {
    const classes = useStyles();
    const {filter, currentResolver, onResolverChange, isOpen, onClose} = props;

    const dialogClasses = {root: classes.root, paper: classes.paper};
    return (
        <Dialog
            open={isOpen}
            onClose={onClose}
            hideBackdrop
            classes={dialogClasses}
            scroll="paper"
            PaperComponent={DialogPaper}
            keepMounted
        >
            <ResizableBox
                height={450}
                width={570}
                className={classes.resizable}
            >
                <DialogTitle id="dialog-title" className={classes.dialogTitle}>Filter Groups</DialogTitle>
                <DialogContent dividers={true}>
                    <GroupSelectorResolver
                        groupFilter={filter}
                        currentResolver={currentResolver}
                        onResolverChange={resolver => onResolverChange(resolver)}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose} color="primary">
                        Done
                    </Button>
                </DialogActions>
            </ResizableBox>
        </Dialog>
    );
}