import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import CheckIcon from "@material-ui/icons/Check";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
    contents: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    name: {
        marginLeft: 30,
        marginRight: 40,
        display: 'flex',
        alignItems: 'center'
    },
    buttons: {
        '& > *': {
            margin: theme.spacing(1),
        }
    },
    left: {
        display: 'flex'
    },
    right: {
        marginRight: 20,
        display: 'flex',
        alignItems: 'center'
    },
    progressIcon: {
        width: '30px !important',
        height: '30px !important'
    }


}));

export default function VisualizerSummaryRow(props) {
    const classes = useStyles();
    const { visualizerId, visualizer, isRunning } = props;

    const handleView = () => {

    };

    const handleDelete = () => {

    };

    let progressComponent;
    if (isRunning) {
        progressComponent = <CircularProgress className={classes.progressIcon}/>;
    } else {
        progressComponent = <CheckIcon/>;
    }

    // TODO implement delete button
    // TODO make progress icon be deterministic

    return (
        <Paper>
            <div className={classes.contents}>
                <div className={classes.left}>
                    <div className={classes.name}><span>{visualizer.name}</span></div>
                    <div className={classes.buttons}>
                        <Button color="primary" onClick={handleDelete}>Delete</Button>
                        <Button color="primary" component={Link} to={`/visualizer/${visualizerId}`}>View</Button>
                    </div>
                </div>
                <div className={classes.right}>
                    {progressComponent}
                </div>
            </div>
        </Paper>
    );
}