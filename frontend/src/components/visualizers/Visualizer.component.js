import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import {connect} from "react-redux";
import SplitterLayout from 'react-splitter-layout';
import '../../../node_modules/react-splitter-layout/lib/index.css';
import {getGroupState, getSimulatorState} from "../../selectors/simulations";
import {getVisualizersState, getVisualizationsState, getStatisticsState} from "../../selectors/visualizations";
import GroupSelectorDialog from './GroupSelectorDialog.component';
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import SimulationJobsList from "../../SimulationJobsList.component";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import {addType} from "../../utils/types";
import {createFilter, LeafResolver, runFilter} from "../../utils/object-filter";
import VisualizerPane from "./VisualizerPane.component";
import {getVisualizerGroups} from "../../selectors/visualizations";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    const hidden = value !== index;

    const styles = hidden ? {} : {display: 'flex', flexDirection: 'column', flex: 1};

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={hidden}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            style={styles}
            {...other}
        >
            {value === index && <Box style={styles} p={3}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles(theme => ({
    '@global': {
        '.splitter-layout': {
            position: 'static'
        }
    },
    root: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    appBarContent: {
        display: 'flex',
        alignItems: 'center'
    },
    tabs: {
        flex: 1
    },
    buttons: {
        marginRight: 30,
        marginTop: 10,
        marginBottom: 10
    },
    button: {
        '&:not(:last-child)': {
            marginRight: 20
        }
    },
    tabContent: {
        display: 'flex',
        flexDirection: 'column'
    }
}));

const mapStateToProps = state => ({
    groupState: getGroupState(state),
    simulationState: getSimulatorState(state),
    visualizersState: getVisualizersState(state),
    visualizationsState: getVisualizationsState(state),
    statisticsState: getStatisticsState(state),
    // completedVisualizerIds: getCompletedVisualizers(state)
});

const actionCreators = {
    // addTemplates, addVisualizer
};

export default connect(mapStateToProps, actionCreators)(function AllVisualizers(props) {
    const classes = useStyles();
    const {
        visualizersState,
        visualizationsState,
        statisticsState,
        visualizerId,
        groupState,
        simulationState
    } = props;

    const [tabValue, setTabValue] = React.useState(0);
    const [isFilterOpen, setFilterOpen] = React.useState(false);
    const [filter, setFilter] = React.useState(null);
    const [resolver, setResolver] = React.useState(null);
    const [selectedGroups, setSelectedGroups] = React.useState(null);

    const visualizer = visualizersState.byId[visualizerId];
    let groupKeys = getVisualizerGroups(visualizer, simulationState.byId);

    let initializedFilter = filter;
    if (!initializedFilter) {
        const groupSchemas = groupKeys.map(groupKey => {
            let schema = groupState.byId[groupKey].schema;
            schema = addType(schema, 'SCHEMA');
            schema.stateConfig = addType(schema.stateConfig, "STATE");
            return {
                id: groupKey,
                schema
            };
        });
        const groupIds = groupSchemas.map(obj => obj.id);
        initializedFilter = createFilter(groupSchemas.map(obj => obj.schema), groupIds);

        setFilter(initializedFilter);
    }

    const onResolverChange = resolver => {
        setResolver(resolver);
        const ids = runFilter(filter, resolver);
        setSelectedGroups(Array.from(ids));
    };

    if (!selectedGroups) {
        setSelectedGroups(groupKeys);
    }

    const handleTabChange = (event, newValue) => {
        setTabValue(newValue);
    };

    const openFilterDialog = () => {
        setFilterOpen(true);
    };

    const closeFilterDialog = () => {
        setFilterOpen(false);
    };

    return (
        <div className={classes.root}>
            <GroupSelectorDialog
                isOpen={isFilterOpen}
                onClose={closeFilterDialog}
                filter={initializedFilter}
                currentResolver={resolver}
                onResolverChange={onResolverChange}/>
            <AppBar position="static">
                <div className={classes.appBarContent}>
                    <Tabs className={classes.tabs} value={tabValue} onChange={handleTabChange} aria-label="simple tabs example">
                        <Tab label="Per Iteration" {...a11yProps(0)} />
                        <Tab label="Across Iterations" {...a11yProps(1)} />
                    </Tabs>
                    <div className={classes.buttons}>
                        <Button
                            className={classes.button}
                            variant="contained"
                            color="secondary"
                            onClick={openFilterDialog}>
                            Filter Groups
                        </Button>
                        <Button
                            className={classes.button}
                            variant="contained"
                            color="secondary">
                            Add Visualization
                        </Button>
                    </div>
                </div>
            </AppBar>
            <TabPanel value={tabValue} index={0} classes={{root: classes.tabContent}}>
                <VisualizerPane
                    perIteration={true}
                    entityIds={selectedGroups}
                    visualizer={visualizersState.byId[visualizerId]}
                    visualizationsMap={visualizationsState.byId}
                    statisticsMap={statisticsState.byId}
                />
            </TabPanel>
            <TabPanel value={tabValue} index={1}>
            </TabPanel>
        </div>
    );
});