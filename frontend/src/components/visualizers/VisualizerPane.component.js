import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import {Slider} from "@material-ui/core";
import IterationSlider from "./IterationSlider.component";
import PerIterationVisualizations from "../visualizations/PerIterationVisualizations.component";

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column'
    },
    visualizationsContainer: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        marginBottom: 24
    },
}));

export default function PerIterationVisualizerPane(props) {
    const classes = useStyles();
    const {perIteration, entityIds, visualizer, visualizationsMap, statisticsMap} = props;

    const [currentIteration, setCurrentIteration] = React.useState(0);

    const visualizationsKey = perIteration ? "perIteration" : "acrossIterations";

    return (
        <div className={classes.root}>
            <Paper className={classes.visualizationsContainer}>
                <PerIterationVisualizations
                    visualizations={visualizer.visualizations[visualizationsKey]}
                    visualizationsMap={visualizationsMap}
                    statisticsMap={statisticsMap}
                    entityIds={entityIds}
                    iteration={currentIteration}
                    totalIterations={visualizer.iterationCount}
                />
            </Paper>
            <Paper>
                <IterationSlider
                    allowRange={!perIteration}
                    maxIteration={visualizer.iterationCount}
                    onChange={setCurrentIteration}
                    playSpeed={200}
                />
            </Paper>
        </div>
    )
}
