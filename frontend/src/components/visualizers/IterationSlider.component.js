import {withStyles} from "@material-ui/core/styles";
import {Slider} from "@material-ui/core";
import React from "react";
import Typography from "@material-ui/core/Typography";
import PlayIcon from '@material-ui/icons/PlayCircleFilled';
import PauseIcon from '@material-ui/icons/PauseCircleFilled';
import IconButton from "@material-ui/core/IconButton";

const styles = theme => ({
    root: {
        paddingTop: 16,
        paddingRight: 32,
        paddingBottom: 12,
        paddingLeft: 20,
    },
    sliderContainer: {
        display: 'flex',
        marginBottom: 8
    },
    playButton: {
        marginRight: 26
    }
});

const MARK_COUNT = 5;
const formatIterationLabel = iteration => "" + iteration;

class IterationSlider extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentIterations: [0, 0],
            isPlaying: false
        };
        this.playTimer = 0;
    }

    onSliderChange = (event, value) => {
        this.setState((state, props) => ({
            currentIterations: props.allowRange ? value : [value, value]
        }));
        this.props.onChange(value);
    };

    getNextSliderValue = () => {
        const currentIndex = this.props.allowRange ? 1 : 0;
        let end = this.state.currentIterations[currentIndex] + 1;
        if (end > this.props.maxIteration) {
            end = 0;
        }
        if (this.props.allowRange) {
            return [this.state.currentIterations[0], end];
        } else {
            return end;
        }
    };

    play = () => {
        this.playTimer = setInterval(() => {
            const value = this.getNextSliderValue();
            this.onSliderChange(null, value);
        }, this.props.playSpeed);
        this.setState(() => ({
            isPlaying: true
        }));
    };

    pause = () => {
        clearInterval(this.playTimer);
        this.playTimer = 0;
        this.setState(() => ({
            isPlaying: false
        }));
    };

    playToggled = () => {
        if (this.state.isPlaying) {
            this.pause();
        } else {
            this.play();
        }
    };


    render() {
        const { classes, allowRange, maxIteration } = this.props;

        const iterationsPerMark = Math.floor(maxIteration / (MARK_COUNT - 1));
        const marks = new Array(MARK_COUNT).fill(0).map((value, markIndex) => {
            const iteration = markIndex === MARK_COUNT - 1 ? maxIteration : iterationsPerMark * markIndex;
            return {
                value: iteration,
                label: "" + iteration
            }
        });


        const value = allowRange ? this.state.currentIterations : this.state.currentIterations[0];
        const labelText = allowRange ? "Iteration Range" : "Iteration";

        return (
            <div className={classes.root}>
                <div className={classes.sliderContainer}>
                    <IconButton color="primary" className={classes.playButton} onClick={this.playToggled}>
                        {this.state.isPlaying ? <PauseIcon/> : <PlayIcon/>}
                    </IconButton>
                    <Slider
                        value={value}
                        valueLabelFormat={formatIterationLabel}
                        getAriaValueText={formatIterationLabel}
                        step={1}
                        valueLabelDisplay="auto"
                        marks={marks}
                        onChange={this.onSliderChange}
                        min={0}
                        max={maxIteration}
                    />
                </div>
                <Typography className={classes.label} align="center">{labelText}</Typography>
            </div>
        )
    }
}

export default withStyles(styles)(IterationSlider);