import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
}));

export default function KeyResolverComponent(props) {
    const classes = useStyles();

    return (
        <div>
            No refinements possible
        </div>
    );
}