import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import getGroupNamer from '../../../utils/namers/group-namer';
import {LeafResolver, ObjectResolver, ValueFilter, ValueResolver} from '../../../utils/object-filter';
import { getResolverComponentClass } from "./resolver.utils";
import TreeItem from '@material-ui/lab/TreeItem';
import ValueResolverComponent from "./ValueResolver.component";
import { fade } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    valueComponentContainer: {
        display: 'flex',
        alignItems: 'center',
        "& .name": {
            marginRight: 40
        },
        "& .valueComponent": {
            flex: 1
        }
    },
    group: {
        paddingLeft: 20,
        marginLeft: 7.5,
        borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
    }

}));

export default function KeyResolverComponent(props) {
    const classes = useStyles();
    const {type, filter, onResolverChange, keyPath, initialResolver} = props;
    const [isAnd, setIsAnd] = React.useState(initialResolver ? initialResolver.isAnd : true);
    const [keyResolverMap, setKeyResolverMap] = React.useState(initialResolver ? {...initialResolver.keyResolverMap} : {});

    const namer = getGroupNamer(type);

    const onSubResolverChange = (key, and) => resolver => {
        const newResolverMap = {
            ...keyResolverMap,
            [key]: resolver
        };
        setKeyResolverMap(newResolverMap);
        onResolverChange(new ObjectResolver(newResolverMap, and));
    };

    const keysWithChildren = filter.getKeys().filter(key => !namer.skip(key) && filter.getFilter(key).hasChoice);
    const subFilterComponents = keysWithChildren.map(key => {
        const subFilter = filter.getFilter(key);
        const callback = onSubResolverChange(key, isAnd);
        let ComponentClass = getResolverComponentClass(subFilter);
        if (ComponentClass != null) {
            return (
                <ComponentClass
                    type={type}
                    filter={subFilter}
                    filterKey={key}
                    onResolverChange={callback}
                    keyPath={keyPath + "/" + key}
                    initialResolver={keyResolverMap[key]}
                />
            );
        } else {
            throw new Error(`Key ${key} maps to unrecognized filter type`);
        }
    });

    return (
        <>
            {keysWithChildren.map((key, i) => {
                const filterComponent = subFilterComponents[i];
                if (filterComponent.type.name === "ValueResolverComponent") {
                    return (
                        <div key={key} className={classes.valueComponentContainer}>
                            <span className="name">{namer.key(key)}</span>
                            <div className="valueComponent">
                                {filterComponent}
                            </div>
                        </div>
                    )
                } else {
                    return (
                        <TreeItem
                            classes={{group: classes.group}}
                            key={key}
                            nodeId={`${keyPath}/${key}`}
                            label={namer.key(key)}
                            className={classes.treeItem}
                            TransitionProps={{unmountOnExit: false}}>
                            {filterComponent}
                        </TreeItem>
                    )
                }
            })}
        </>
    );
}