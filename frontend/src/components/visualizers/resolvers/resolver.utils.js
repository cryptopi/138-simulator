import {KeyedFilter, TypeFilter, ValueFilter} from "../../../utils/object-filter";
import KeyResolver from "./KeyResolver.component";
import TypeResolver from "./TypeResolver.component";
import ValueResolver from "./ValueResolver.component";

export const getResolverComponentClass = filter => {
    if (filter instanceof KeyedFilter) {
        return KeyResolver;
    }
    if (filter instanceof TypeFilter) {
        return TypeResolver;
    }
    if (filter instanceof ValueFilter) {
        return ValueResolver;
    }
    return null;
};