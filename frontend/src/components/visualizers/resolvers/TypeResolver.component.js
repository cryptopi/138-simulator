import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import getGroupNamer from '../../../utils/namers/group-namer';
import { ValueResolver, LeafResolver } from '../../../utils/object-filter';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import { getResolverComponentClass } from "./resolver.utils";
import LeafResolverComponent from "./LeafResolver.component";
import {Grow} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    choicesContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    label: {
        marginRight: 20
    },
    root: {
        display: 'block'
    },
    choices: {
        display: 'flex',
        justifyContent: 'space-around',
        width: '100%'
    },
    formControl: {
        width: '100%',
        maxWidth: 400,
        marginBottom: 20,
        display: 'flex',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
}));

export default function TypeResolverComponent(props) {
    const classes = useStyles();
    const {filter, onResolverChange, keyPath, initialResolver} = props;

    const [typeResolverMap, setTypeResolverMap] = React.useState(initialResolver ? {...initialResolver.valueResolverMap} : {});
    const [selectedTypes, setSelectedTypes] = React.useState(initialResolver ? new Set(Object.keys(initialResolver.valueResolverMap)) : new Set());
    const [visibleType, setVisibleType] = React.useState(initialResolver && Object.keys(initialResolver.valueResolverMap).length === 1 ? Object.keys(initialResolver.valueResolverMap)[0] : '');

    const types = filter.values;
    const typeMap = filter.valueMap;
    const namerMap = {};
    Object.keys(typeMap).forEach(type => {
        namerMap[type] = getGroupNamer(type);
    });

    // Incredibly, incredibly sketchy. Should have clean way to override fixed states from displaying
    const isDummyType = types.length === 1 && types[0] === 'STATE';
    const isDummyState = selectedTypes.size === 1 && Array.from(selectedTypes)[0] && visibleType === 'STATE';
    if (isDummyType && !isDummyState) {
        setSelectedTypes(new Set(['STATE']));
        setVisibleType('STATE');
    }

    const handleTypeSelected = event => {
        const newSelectedTypes = new Set(selectedTypes);
        const type = event.target.name;
        const remove = newSelectedTypes.has(type);
        if (remove) {
            newSelectedTypes.delete(type);
        } else {
            newSelectedTypes.add(type);
        }
        setSelectedTypes(newSelectedTypes);
        if (!filter.getFilter(type).hasChoice) {
            onSubResolverChange(type, newSelectedTypes)(new LeafResolver());
        }
        if (newSelectedTypes.size === 1) {
            setVisibleType(Array.from(newSelectedTypes)[0]);
        } else {
            setVisibleType('');
        }
        if (remove) {
            onSubResolverChange(type, newSelectedTypes)(null);
        }
    };

    const handleVisibleTypeChange = event => {
        setVisibleType(event.target.value);
    };

    const onSubResolverChange = (type, filterTypes) => resolver => {
        const newMap = {
            ...typeResolverMap,
            [type]: resolver
        };
        if (resolver === null) {
            delete newMap[type];
        }
        setTypeResolverMap(newMap);
        const filteredMap = {};
        filterTypes.forEach(type => {
            filteredMap[type] = newMap[type];
        });
        onResolverChange(new ValueResolver(Object.keys(filteredMap), filteredMap));
    };

    const typeChoiceElements = types.map(type => {
        const name = namerMap[type].type;
        const control = (
            <Checkbox
                color="primary"
                checked={selectedTypes.has(type)}
                onChange={handleTypeSelected}
                name={type}/>
        );
        return (
            <FormControlLabel key={type} control={control} label={name} labelPlacement="top"/>
        );
    });

    let typeSelector = null;
    const selectedTypesWithChildren = Array.from(selectedTypes).filter(type => filter.getFilter(type).hasChoice);
    if (selectedTypesWithChildren.length >= 1 && selectedTypes.size > 1) {
        const id = keyPath + Array.from(types).join("");
        typeSelector = (
            <FormControl className={classes.formControl}>
                <InputLabel id={id}>Refine Type</InputLabel>
                <Select
                    labelId={id}
                    value={visibleType}
                    className={classes.select}
                    onChange={handleVisibleTypeChange}>
                    {Array.from(selectedTypes).map(type => {
                        return (
                            <MenuItem key={type} value={type}>{namerMap[type].type}</MenuItem>
                        );
                    })}
                </Select>
            </FormControl>
        )
    }

    let subComponent = null;
    if (visibleType.length > 0) {
        const subFilter = filter.getFilter(visibleType);
        if (subFilter.hasChoice) {
            const ComponentClass = getResolverComponentClass(subFilter);
            if (ComponentClass != null) {
                const callback = onSubResolverChange(visibleType, selectedTypes);
                subComponent = (
                    <ComponentClass
                        type={visibleType}
                        filter={subFilter}
                        onResolverChange={callback}
                        keyPath={keyPath}
                        initialResolver={typeResolverMap[visibleType]}
                    />
                );
            } else {
                throw new Error(`Type ${visibleType} for type resolver is not valid`);
            }
        } else {
            subComponent = <LeafResolverComponent />
        }
    }

    return (
        <div className={classes.root}>
            {!isDummyType && (
                <div className={classes.choicesContainer}>
                    {/*<span className={classes.label}>Type(s):</span>*/}
                    <div className={classes.choices}>
                        {typeChoiceElements}
                    </div>
                </div>
                )
            }
            <Grow in={!!typeSelector}>
                <div className={classes.selectorContainer}>
                    {typeSelector}
                </div>
            </Grow>
            <Grow in={!!subComponent}>
                <div className={classes.subComponent}>
                    {subComponent}
                </div>
            </Grow>
        </div>
    );
}