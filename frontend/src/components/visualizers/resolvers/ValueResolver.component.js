import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import getGroupNamer from '../../../utils/namers/group-namer';
import {KeyedFilter, ValueFilter, Leaf, ValueResolver, LeafResolver} from '../../../utils/object-filter';
import {FormControlLabel} from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import { getResolverComponentClass } from "./resolver.utils";
import Slider from "@material-ui/core/Slider";
import DefaultSlider from "../../utils/DefaultSlider.component";

const useStyles = makeStyles(theme => ({
    typeSelectionContainer: {


    }
}));

export default function ValueResolverComponent(props) {
    const classes = useStyles();
    const {type, filter, filterKey, onResolverChange, keyPath, initialResolver} = props;

    const [resolverValues, setResolverValues] = React.useState(initialResolver ? [...initialResolver.values] : []);
    const [valueResolverMap, setValueResolverMap] = React.useState(initialResolver ? {...initialResolver.valueResolverMap} : {});

    const namer = getGroupNamer(type);
    const valueNamer = value => namer.value(filterKey, value);

    const onSliderChange = (event, value) => {
        const newValues = [value];
        const newMap = {
            [value]: new LeafResolver()
        };
        setResolverValues(newValues);
        setValueResolverMap(newMap);
        onResolverChange(new ValueResolver(newValues, newMap));
    };

    const onCheckboxChange = event => {
        let newValues = [...resolverValues];
        const newValueMap = {...valueResolverMap};
        const value = event.target.name;
        if (value in newValueMap) {
            delete newValueMap[value];
            const index = newValues.indexOf(value);
            newValues = newValues.splice(index, 1);
        } else {
            newValueMap[value] = new LeafResolver();
            newValues.push(value);
        }
        setResolverValues(newValues);
        setValueResolverMap(newValueMap);
        onResolverChange(new ValueResolver(newValues, newValueMap));
    };

    let resolverComponent = null;
    if (filter.isNumeric) {
        const values = filter.values;
        const initialValue = resolverValues.length === 1 ? resolverValues[0] : null;
        resolverComponent = (
            <DefaultSlider
                initialValue={initialValue}
                values={values}
                getAriaValueText={valueNamer}
                step={null}
                valueLabelDisplay="auto"
                marks={values.map(value => ({
                    value,
                    label: valueNamer('' + value)
                }))}
                onChangeCommitted={onSliderChange}
            />
        );
    } else {
        const choiceElements = filter.values.map(value => {
            const name = valueNamer(value);
            const control = (
                <Checkbox
                    color="primary"
                    checked={value in valueResolverMap}
                    onChange={onCheckboxChange}
                    name={value}/>
            );
            return (
                <FormControlLabel key={type} control={control} label={name} labelPlacement="top"/>
            );
        });
        // Discrete choices
        resolverComponent = (
            <div className={classes.discreteContainer}>
                {choiceElements}
            </div>
        )
    }

    return resolverComponent
}