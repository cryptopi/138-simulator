import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import KeyResolverComponent from "./KeyResolver.component";
import {runFilter, ValueResolver} from "../../../utils/object-filter";
import TreeView from "@material-ui/lab/TreeView";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";

const useStyles = makeStyles(theme => ({
    tree: {
        width: '100%'
    }
}));

export default function GroupSelectorResolver(props) {
    const classes = useStyles();
    const {groupFilter, currentResolver, onResolverChange} = props;

    const [expanded, setExpanded] = React.useState([]);
    const [selected, setSelected] = React.useState([]);

    const handleToggle = (event, nodeIds) => {
        setExpanded(nodeIds);
    };

    const handleSelect = (event, nodeIds) => {
        setSelected(nodeIds);
    };

    const onKeyResolverChange = resolver => {
        const fullResolver = new ValueResolver(['SCHEMA'], {
            SCHEMA: resolver
        });
        // const ids = runFilter(groupFilter, fullResolver);
        // console.log(resolver);
        // console.log(ids);
        onResolverChange(fullResolver);
    };

    return (
        <TreeView
            className={classes.tree}
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}
            expanded={expanded}
            selected={[]}
            onNodeToggle={handleToggle}
            onNodeSelect={handleSelect}
        >
            <KeyResolverComponent
                type="SCHEMA"
                filter={groupFilter.getFilter('SCHEMA')}
                initialResolver={currentResolver ? currentResolver.getResolver('SCHEMA') : null}
                onResolverChange={onKeyResolverChange} keyPath={"/"}/>
        </TreeView>
    );
}