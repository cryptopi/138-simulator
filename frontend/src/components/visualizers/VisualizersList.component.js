import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";
import VisualizerSummaryRow from "./VisualizerSummaryRow.component";

const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 900,
        margin: '0 auto',
        marginTop: 24
    },
    noJobs: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    noJobMessage: {
        marginTop: 30,
        marginBottom: 30
    },
    templateList: {
        marginBottom: 30
    },
    createContainer: {
        textAlign: 'center'
    }
}));

export default function VisualizersList(props) {
    const classes = useStyles();
    const {visualizers, completedIds, createHandler} = props;


    let component = (
        <div className={classes.root}>
            <div className={classes.templateList}>
                {visualizers.all.map(id => (
                    <VisualizerSummaryRow
                        key={id}
                        visualizerId={id}
                        visualizer={visualizers.byId[id]}
                        isRunning={!completedIds.has(id)}
                    />
                ))}
            </div>
            <div className={classes.createContainer}>
                <Button variant="contained" color="primary" onClick={createHandler}>
                    Create Visualizer
                </Button>
            </div>
        </div>
    );

    return component;
}