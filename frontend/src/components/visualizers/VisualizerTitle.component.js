import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Button from "@material-ui/core/Button";
import {getGroupState, getSimulatorState, getTemplateState} from "../../selectors/simulations";
import {addTemplates} from "../../actions/simulations.actions";
import {addVisualizer} from "../../actions/visualizations.actions";
import {getVisualizersState, getVisualizationsState, getStatisticsState, getCompletedVisualizers} from "../../selectors/visualizations";
import {connect} from "react-redux";
import VisualizersList from "./VisualizersList.component";

const useStyles = makeStyles(theme => ({
}));

const mapStateToProps = state => ({
    visualizersState: getVisualizersState(state),
});

const actionCreators = {};

export default connect(mapStateToProps, actionCreators)(function AllVisualizers(props) {
    const classes = useStyles();
    const {visualizersState, visualizerId} = props;

    return (
        <div>{visualizersState.byId[visualizerId].name}</div>
    );
});