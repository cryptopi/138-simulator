import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Button from "@material-ui/core/Button";
import {getGroupState, getSimulatorState, getTemplateState} from "../../selectors/simulations";
import {addTemplates} from "../../actions/simulations.actions";
import {addVisualizer} from "../../actions/visualizations.actions";
import {getVisualizersState, getVisualizationsState, getStatisticsState, getCompletedVisualizers} from "../../selectors/visualizations";
import {connect} from "react-redux";
import VisualizersList from "./VisualizersList.component";

const useStyles = makeStyles(theme => ({
    root: {
        height: '100%'
    },
    empty: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex'
    },
}));

const mapStateToProps = state => ({
    visualizersState: getVisualizersState(state),
    visualizationsState: getVisualizationsState(state),
    statisticsState: getStatisticsState(state),
    completedVisualizerIds: getCompletedVisualizers(state)
});

const actionCreators = {
    // addTemplates, addVisualizer
};

export default connect(mapStateToProps, actionCreators)(function AllVisualizers(props) {
    const classes = useStyles();
    const {visualizersState, visualizationsState, statisticsState, completedVisualizerIds} = props;

    const hasVisualizers = visualizersState.all.length > 0;

    const createHandler = () => {

    };

    let component;
    if (hasVisualizers) {
        component = (
            <VisualizersList
                visualizers={visualizersState}
                completedIds={new Set(completedVisualizerIds)}
                createHandler={createHandler} />
        );
    } else {
        component = (
            <div className={classes.empty}>
                <Button variant="contained" color="primary" onClick={createHandler}>
                    Create a Visualizer
                </Button>
            </div>
        );
    }

    return (
        <div className={classes.root}>
            {component}
        </div>
    );
});