import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import getGroupNamer from '../../utils/namers/group-namer';
import {createFilter} from "../../utils/object-filter";
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';

const useStyles = makeStyles(theme => ({
}));

const excludeKeys = new Set(['groupKey', 'jobId', 'name']);

export default function GroupSelector(props) {
    const classes = useStyles();
    const {groupFilter, onSelect} = props;

    const [expanded, setExpanded] = React.useState([]);
    const [selected, setSelected] = React.useState([]);

    const handleToggle = (event, nodeIds) => {
        setExpanded(nodeIds);
    };

    const handleSelect = (event, nodeIds) => {
        setSelected(nodeIds);
        onSelect(nodeIds);
    };

    return (
        <div>
            <TreeView
                className={classes.root}
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpandIcon={<ChevronRightIcon />}
                expanded={expanded}
                selected={[]}
                onNodeToggle={handleToggle}
                onNodeSelect={handleSelect}
            >
                {groupFilter.getKeys().map(key => {
                    if (excludeKeys.has(key)) {
                        return null;
                    }
                    if (groupFilter.getFilter(key).hasChoice) {
                        const nodeId = `schema/${key}`;
                        let innerItems = [];
                        if (key === 'stateConfig') {
                            const stateFilter = groupFilter.getFilter(key).getFilter('STATE');
                            innerItems = stateFilter.getKeys().map(key => {
                                if (excludeKeys.has(key)) {
                                    return null;
                                }
                                if (stateFilter.getFilter(key).hasChoice) {
                                    const nodeId = `state/${key}`;
                                    return (
                                        <TreeItem key={nodeId} nodeId={nodeId} label={getGroupNamer('STATE').key(key)}/>
                                    )
                                }
                            });
                            innerItems = innerItems.filter(val => !!val);
                        }
                        let component;
                        if (innerItems.length > 0) {
                            component = (
                                <TreeItem key={nodeId} nodeId={nodeId} label={getGroupNamer('SCHEMA').key(key)}>
                                    {innerItems}
                                </TreeItem>
                            );
                        } else {
                            component = (
                                <TreeItem key={nodeId} nodeId={nodeId} label={getGroupNamer('SCHEMA').key(key)}/>
                            )
                        }
                        return component;
                    }
                    return null;
                })}
            </TreeView>
        </div>
    );
}