export const PIE = "PIE";
export const LINE = "LINE";
export const HISTOGRAM = "HISTOGRAM";
export const SCATTER = "SCATTER";
export const VALUE = "VALUE";

export const TOP_PERCENT_WEALTH = "TOP_PERCENT_FIXED_WEALTH";