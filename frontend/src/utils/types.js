
export const addType = (obj, type) => {
    return {
        ...obj,
        type: type
    }
};

// https://stackoverflow.com/a/9716488
export const isNumeric = val => {
    return !isNaN(parseFloat(val)) && isFinite(val);
};