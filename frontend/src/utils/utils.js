
export const setPath = (obj, path, value) => {
    const fullObj = obj;
    for (let i = 0; i < path.length - 1; i++) {
        obj = obj[path[i]];
    }
    obj[path[path.length - 1]] = value;
    return fullObj;
};

export const nestedForLoops = (values, func) => {
    const helper = (values, func, args, index) => {
        if (values.length === 0) {
            return [func(...args)];
        }
        const rest = values.slice(1);
        const current = values[0];
        let results = [];
        for (const value of current) {
            args[index] = value;
            results = results.concat(helper(rest, func, args, index + 1));
        }
        return results;
    };
    return helper(values, func, [], 0);
};