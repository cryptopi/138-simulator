
export default class CustomMap {

    constructor(keyFunc) {
        this.keyFunc = keyFunc || JSON.stringify;
        this.map = {};
    }

    put(key, val) {
        this.map[this.keyFunc(key)] = val;
    }

    get(key) {
        return this.map[this.keyFunc(key)];
    }

    delete(key) {
        delete this.map[this.keyFunc(key)];
    }

    contains(key) {
        return this.keyFunc(key) in this.map;
    }
}