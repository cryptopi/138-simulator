
const make = (typeName, keyNamer, valueNamer, skip = []) => {
    valueNamer = valueNamer || ((key, value) => {throw new Error(
        `Value Namer not set. Tried to name key ${key} and value ${value}`
    )});
    const toSkip = new Set(skip);
    return {
        type: typeName,
        key: keyNamer,
        value: valueNamer,
        skip: val => toSkip.has(val)
    }
};

const identity = x => x;

const makeKeyMapNamer = map => {
    return key => {
        if (!(key in map)) {
            throw Error(`Name for key ${key} not found`);
        }
        return map[key];
    }
};

const makeValueNamer = namers => {
    return (key, value) => {
        if (!(key in namers)) {
            throw Error(`Cannot name value for key ${key}`);
        }
        return namers[key](value);
    }
};

const arrayNamer = array => array.join(", ");

const makeBooleanNamer = (trueName = "True", falseName = "Falsel") => {
    return value => value ? trueName : falseName;
};

const nullNamer = make("None");

const schemaNamer = make(
    "Schema",
    makeKeyMapNamer({
        stateConfig: "State Config",
        agentMovement: "Agent Movement",
        eventMovement: "EventMovement",
        wealthRedistribution: "Wealth Redistribution",
        eventInteractionConfig: "Event Interaction",
        totalIterationCount: "Iteration Count",
        count: "Simulation Count"
    }),
    null,
    ["name", "groupKey", "jobId"]
);

const stateNamer = make(
    "State",
    makeKeyMapNamer({
        skillGenerator: "Skill Generator",
        agentLocationGenerator: "Agent Locations",
        eventLocationGenerator: "Event Locations",
        eventMagnitudeGenerator: "Event Magnitudes",
        agentCount: "Agent Count",
        eventCount: "Event Count",
        gridBounds: "Grid  Bounds"
    })
);

const normalNamer = make(
    "Normal",
    makeKeyMapNamer({
        mu: "Mu",
        sigma: "Sigma",
    }),
    makeValueNamer({
        mu: identity,
        sigma: identity
    })
);

const choiceNamer = make(
    "Discrete Choice",
    makeKeyMapNamer({
        values: "Values",
        probabilities: "Probabilities"
    }),
    makeValueNamer({
        values: arrayNamer,
        probabilities: identity
    })
);

const uniformNamer = make(
    "Uniform",
    makeKeyMapNamer({
        lower: "Lower Bound",
        upper: "Upper Bound"
    }),
    makeValueNamer({
        lower: identity,
        upper: identity
    })
);

const independentNamer = make(
    "Independent",
    makeKeyMapNamer({
        xGenerator: "X",
        yGenerator: "Y"
    })
);

const randomNamer = make(
    "Random",
    makeKeyMapNamer({
        stepSize: "Step Size"
    }),
    makeValueNamer({
        stepSize: identity
    })
);

const wealthVaryingValueNamer = make(
    "Wealthy Varying",
    makeKeyMapNamer({
        wealthCutoffs: "Wealth Cutoffs",
        values: "Values",
        areCutoffsPercentile: "Percentile"
    }),
    makeValueNamer({
        wealthCutoffs: arrayNamer,
        values: arrayNamer,
        areCutoffsPercentile: makeBooleanNamer()
    })
);

const generalRedistributionNamer = make(
    "General Redistribution",
    makeKeyMapNamer({
        taxBrackets: "Tax Cutoffs",
        redistributionBrackets: "Redistribution Cutoffs",
        isTaxPercent: "Tax Percent"
    }),
    makeValueNamer({
        isTaxPercent: makeBooleanNamer
    })
);

const linearNamer = make(
    "Linear",
    makeKeyMapNamer({
        multiplier: "Slope",
        offset: "Intercept",
        min: "Min",
        max: "Max",
        variable: "Variable"
    }),
    makeValueNamer({
        multiplier: identity,
        offset: identity,
        min: identity,
        max: identity,
        variable: makeKeyMapNamer({
            SKILL: "Skill",
            WEALTH: "Wealth",
            GENERATION: "Generation"
        })
    })
);

const multiplicativeNamer = make(
    "Multiplicative",
    makeKeyMapNamer({
        luckyFactor: "Lucky Factor",
        unluckyFactor: "Unlucky Factor",
        includeEventMagnitude: "Include Event Magnitude"
    }),
    makeValueNamer({
        luckyFactor: identity,
        unluckyFactor: identity,
        includeEventMagnitude: makeBooleanNamer
    })
);

const eventInteractionNamer = make(
    "Event Interaction",
    makeKeyMapNamer({
        probabilityLuckyEvents: "Probability Lucky Events",
        probabilityUnluckyEvents: "Probability Unlucky Events",
        wealth: "Wealth"
    })
);

const namers = {
    NULL: nullNamer,
    SCHEMA: schemaNamer,
    STATE: stateNamer,
    NORMAL: normalNamer,
    CHOICE: choiceNamer,
    UNIFORM: uniformNamer,
    INDEPENDENT: independentNamer,
    RANDOM: randomNamer,
    WEALTH_VARYING_VALUE: wealthVaryingValueNamer,
    GENERAL_REDISTRIBUTION: generalRedistributionNamer,
    LINEAR: linearNamer,
    MULTIPLICATIVE: multiplicativeNamer,
    EVENT_INTERACTION: eventInteractionNamer
};

export default type => {
    return namers[type];
};