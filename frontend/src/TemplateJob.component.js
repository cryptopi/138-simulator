import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { ExpandLess, ExpandMore } from "@material-ui/icons";
import LinearProgress from '@material-ui/core/LinearProgress';
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Moment from 'react-moment';
import Tooltip from "@material-ui/core/Tooltip";
import Collapse from "@material-ui/core/Collapse";

const useStyles = makeStyles(theme => ({
    contents: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    expandIcon: {
        marginLeft: 20,
        marginRight: 30,
        display: 'flex',
        alignItems: 'center'
    },
    name: {
        marginRight: 40,
        display: 'flex',
        alignItems: 'center'
    },
    buttons: {
        '& > *': {
            margin: theme.spacing(1),
        }
    },
    left: {
        display: 'flex'
    },
    right: {
        marginRight: 20,
        display: 'flex',
        alignItems: 'center'
    }


}));

export default function TemplateJob(props) {
    const classes = useStyles();
    const [isExpanded, setExpanded] = React.useState(false);
    const {isRunning, template, groupMap, simulationMap } = props;

    const num = template.completedIterations / template.totalIterations * 100;

    const handleExpandedClicked = () => {
        setExpanded(!isExpanded);
    };

    const handleView = () => {

    };

    const handleDelete = () => {

    };

    const handleCancel = () => {

    };

    const handleVisualize = () => {

    };

    let momentComponent = null;
    let tooltipTitle = '';
    const prefaceWith = preface => text => {
        return preface + text;
    };

    if (isRunning) {
        momentComponent = (
            <Moment filter={prefaceWith("Created ")} fromNow>{template.createdTime}</Moment>
        );
    } else {
        tooltipTitle = <Moment filter={prefaceWith("Runtime: ")} duration={template.createdTime} date={template.completedTime}/>
        momentComponent = (
            <Moment filter={prefaceWith("Finished ")} fromNow>{template.completedTime}</Moment>
        );
    }

    // TODO implement delete and view buttons
    // TODO implement group dropdown
    // TODO implement visualize button - redirect to create visualization dialog

    return (
        <Paper key={template.id}>
            {isRunning ? <LinearProgress variant="determinate" value={num}/> : null}
            <div className={classes.contents}>
                <div className={classes.left}>
                    <div className={classes.expandIcon}>
                        <IconButton aria-label="delete" onClick={handleExpandedClicked}>
                            {isExpanded ? <ExpandLess/> : <ExpandMore/>}
                        </IconButton>
                    </div>
                    <Tooltip title={tooltipTitle}>
                        <div className={classes.name}><span>{template.name}</span></div>
                    </Tooltip>
                    <div className={classes.buttons}>
                        {isRunning ?
                            <Button color="primary" onClick={handleCancel}>Cancel</Button> :
                            <Button color="primary" onClick={handleDelete}>Delete</Button>}
                        <Button color="primary" onClick={handleView}>View</Button>
                        {!isRunning ? <Button color="primary" onClick={handleVisualize}>Visualize</Button> : null}
                    </div>
                </div>
                <div className={classes.right}>
                    {momentComponent}
                </div>
            </div>
            <Collapse in={isExpanded}>
                {/*{template.groups.map(id => groupMap[id]).map(group => (*/}
                {/*    <GroupJob group={group}/>*/}
                {/*))}*/}
            </Collapse>
        </Paper>
    );
}