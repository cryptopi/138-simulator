import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux'
import configureStore from "./store";
import { WEBSOCKET} from "./urls";
import Moment from "react-moment";
import 'typeface-roboto';
import '../node_modules/react-grid-layout/css/styles.css';
import '../node_modules/react-resizable/css/styles.css';

const ws = new WebSocket(WEBSOCKET);

Moment.startPooledTimer(60);

ReactDOM.render(
    <Provider store={configureStore(ws)}>
        <App ws={ws}/>
    </Provider>,
    document.getElementById('root')
);
