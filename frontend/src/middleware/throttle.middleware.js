import { throttle } from "lodash";

export const groupActionsMiddleware = (times, reducers, makeInitialValues) => {
    const throttledDispatches = {};
    const cleanupTimeouts = {};
    const toDispatch = {};

    return store => next => action => {

        const clearFlush = type => {
            if (cleanupTimeouts[type]) {
                clearTimeout(cleanupTimeouts[type]);
                cleanupTimeouts[type] = null;
            }
        };

        const makeGroupedNext = (type, time, reducer) => {
            const nextWrapper = () => {
                clearFlush(type);
                const toSend = toDispatch[type];
                // console.log("Calling next for real: ", toSend);
                // console.log(Date.now());
                toDispatch[type] = makeInitialValues[type]();
                next(toSend);
            };
            const throttled = throttle(nextWrapper, time);
            return action => {
                toDispatch[type] = reducer(toDispatch[type], action);
                clearFlush(type);
                cleanupTimeouts[type] = setTimeout(() => throttled.flush(), time);
                throttled(action);
            };
        };

        const {type} = action;
        const reducer = reducers[type];
        if (!reducer) {
            return next(action);
        }
        if (!toDispatch[type]) {
            toDispatch[type] = makeInitialValues[type]();
        }
        if (!throttledDispatches[type]) {
            const time = times[type];
            throttledDispatches[type] = makeGroupedNext(type, time, reducer);
        }
        throttledDispatches[type](action);
    }
};