import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AboutIcon from '@material-ui/icons/Info';
import JobsIcon from '@material-ui/icons/Computer';
import StatisticsIcon from '@material-ui/icons/Assessment';
import SimulationIcon from '@material-ui/icons/Person';
import VisualizationIcon from '@material-ui/icons/TrendingUp';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

export default (props) =>
{
    const classes = useStyles();
    // const jobsOpenState = React.useState(true);
    //
    // const openElementHandler = ([state, stateSet]) => {
    //     return () => {
    //         stateSet(!state);
    //     };
    // };

    return (
        <div>
            <ListItem button component={Link} to="/simulations">
                <ListItemIcon>
                    <JobsIcon/>
                </ListItemIcon>
                <ListItemText primary="Simulations"/>
            </ListItem>
            {/*<Collapse in={jobsOpenState[0]} timeout="auto" unmountOnExit>*/}
            {/*    <List component="div" disablePadding>*/}
            {/*        <ListItem button component={Link} to="/jobs/simulations" className={classes.nested}>*/}
            {/*            <ListItemIcon>*/}
            {/*                <SimulationIcon/>*/}
            {/*            </ListItemIcon>*/}
            {/*            <ListItemText primary="Simulations"/>*/}
            {/*        </ListItem>*/}
            {/*        <ListItem button component={Link} to="/jobs/statistics" className={classes.nested}>*/}
            {/*            <ListItemIcon>*/}
            {/*                <StatisticsIcon/>*/}
            {/*            </ListItemIcon>*/}
            {/*            <ListItemText primary="Statistics"/>*/}
            {/*        </ListItem>*/}
            {/*    </List>*/}
            {/*</Collapse>*/}
            <ListItem button component={Link} to="/visualizers">
                <ListItemIcon>
                    <VisualizationIcon/>
                </ListItemIcon>
                <ListItemText primary="Visualizers"/>
            </ListItem>
            <ListItem button component={Link} to="/about">
                <ListItemIcon>
                    <AboutIcon/>
                </ListItemIcon>
                <ListItemText primary="About"/>
            </ListItem>
        </div>
    )
};