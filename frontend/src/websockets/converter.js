import {
    receiveSimulatorProgress,
    receiveStatistic
} from '../actions/websockets.actions';
export const websocketToActions = data => {
    switch (data.type) {
        case "SIMULATOR_PROGRESS":
            return [receiveSimulatorProgress(data.simulationId, 1)];
        case "STAT_RESULT":
            return [receiveStatistic(data.result.statisticId, data.result.entityId, data.result.iteration, data.result.value)];
        case "ERROR":
            console.log("ERROR: " + data);
        default:
            console.log("UNKNOWN: " + data);
    }
};