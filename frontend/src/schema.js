import {cloneDeep} from "lodash";
import {nestedForLoops, setPath} from './utils/utils';
import * as uuid from "uuid";
import { PIE, LINE, HISTOGRAM, SCATTER, VALUE, TOP_PERCENT_WEALTH } from "./constants/types";
import CustomMap from "./utils/CustomMap";

const INT = "INT";
const FLOAT = "FLOAT";
const STRING = "STRING";
const BOOL = "BOOL";
const MULTI = "MULTI_VALUE";

class TypeToken {
    
    constructor(single, data) {
        this.single = single;
        this.data = data;
    }
    
    isSingle() {
        return this.single;
    }
}

class TypeResolverToken {
    
    constructor(...types) {
        this.types = types.reduce((acc, cur, i) => {
            acc[cur.type] = cur;
            return acc;
        }, {});
    }

    resolveType(type) {
        return this.types[type];
    }
}

const normal = (dataType) => ({
    "type": "NORMAL",
    "mu": new TypeToken(true, dataType),
    "sigma": new TypeToken(true, dataType)
});

const choice = (dataType) => ({
    "type": "CHOICE",
    values: new TypeToken(false, dataType),
    probabilities: new TypeToken(false, FLOAT)
});


const uniform = (dataType) => ({
    "type": "UNIFORM",
    lower: new TypeToken(true, dataType),
    upper: new TypeToken(true, dataType)
});

const generatorTypeResolver = (dataType) => new TypeResolverToken(
    normal(dataType),
    choice(dataType),
    uniform(dataType)
);

const independentLocation = () => ({
    "type": "INDEPENDENT",
    "xGenerator": generatorTypeResolver(INT),
    "yGenerator": generatorTypeResolver(INT)
});

const locationGeneratorTypeResolver = () => new TypeResolverToken(
    independentLocation()
);

const randomMovement = () => ({
    "type": "RANDOM",
    stepSize: new TypeToken(true, FLOAT)
});

const movementTypeResolver = () => new TypeResolverToken(
    randomMovement()
);

const wealthVaryingValue = () => ({
    "wealthCutoffs": new TypeToken(false, FLOAT),
    "values": new TypeToken(false, FLOAT),
    "areCutoffsPercentile": new TypeToken(true, BOOL)
});

const generalRedistribution = () => ({
    "type" : "GENERAL",
    "taxBrackets": wealthVaryingValue(),
    "redistributionBrackets": wealthVaryingValue(),
    "isTaxPercent": new TypeToken(true, BOOL)
});

const redistributionTypeResolver = () => new TypeResolverToken(
    generalRedistribution()
);

const linear = () => ({
    "type": "LINEAR",
    multiplier: new TypeToken(true, FLOAT),
    offset: new TypeToken(true, FLOAT),
    min: new TypeToken(true, FLOAT),
    max: new TypeToken(true, FLOAT),
    variable: new TypeToken(true, STRING)
});

const numberTypeResolver = () => new TypeResolverToken(
    linear()
);

const multiplicativeEventWealth = () => ({
    "type": "MULTIPLICATIVE",
    luckyFactor: new TypeToken(true, FLOAT),
    unluckyFactor: new TypeToken(true, FLOAT),
    includeEventMagnitude: new TypeToken(true, BOOL)
});

const eventWealthTypeResolver = () => new TypeResolverToken(
    multiplicativeEventWealth()
);

const eventInteraction = () => ({
    "probabilityLuckyEvents": numberTypeResolver(),
    "probabilityUnluckyEvents": numberTypeResolver(),
    "wealth": eventWealthTypeResolver()
});

const SIMULATION_TYPES = {
    "stateConfig": {
        "skillGenerator": generatorTypeResolver(INT),
        "agentLocationGenerator": locationGeneratorTypeResolver(),
        "eventLocationGenerator": locationGeneratorTypeResolver(),
        "eventMagnitudeGenerator": generatorTypeResolver(FLOAT),
        "agentCount": new TypeToken(true, INT),
        "eventCount": new TypeToken(true, INT),
        "gridBounds": new TypeToken(true, INT)
    },
    "agentMovement": movementTypeResolver(),
    "eventMovement": movementTypeResolver(),
    "wealthRedistribution": redistributionTypeResolver(),
    "eventInteractionConfig": eventInteraction(),
    "totalIterationCount": new TypeToken(true, INT),
    "count": new TypeToken(true, INT)
};

const makeExpandSection = (path, values) => ({
    path,
    values
});

const getSectionsToExpand = (simulation, types) => {
    const withPath = (obj, types, path) => {
        if (!types) {
            console.log(`WARNING: Path ${path} resulted in undefined types`);
            return [];
        }
        if (!(obj instanceof Object)) {
            return [];
        }
        if (types instanceof TypeToken) {
            if (types.single && Array.isArray(obj)) {
                return makeExpandSection(path, obj);
            } else {
                return [];
            }
        } else if (types instanceof TypeResolverToken) {
            if (!obj.type) {
                console.log(`WARNING: Path ${path} is missing type information`);
            }
            return withPath(obj, types.resolveType(obj.type), path);
        } else {
            return Object.keys(obj).map(key => {
                let newPath = [...path];
                newPath.push(key);
                return withPath(obj[key], types[key], newPath);
            }).flat();
        }
    };
    return withPath(simulation, types, []);
};

const expandTemplateOld = template => {
    const sectionsToExpand = getSectionsToExpand(template, SIMULATION_TYPES);
    let collapsedTemplate = cloneDeep(template);
    sectionsToExpand.forEach(section => {
        setPath(collapsedTemplate, section.path, section.values[0])
    });
    return [nestedForLoops(sectionsToExpand.map(section => section.values), (...values) => {
        let group = cloneDeep(collapsedTemplate);
        for (let i = 0; i < values.length; i++) {
           const path = sectionsToExpand[i].path;
           setPath(group, path, values[i]);
        }
        return [group, values];
    }), sectionsToExpand.map(section => section.path)];
};

const isMultiToken = obj => {
    return obj.type && obj.type === MULTI;
};

const makeMultiToken = (...values) => ({
    type: MULTI,
    values
});

const expandMultiTokens = obj => {
    if (!(obj instanceof Object) || Array.isArray(obj)) {
        return makeMultiToken(obj);
    }

    if (isMultiToken(obj)) {
        const toExpand = obj.values;
        const allExpanded = toExpand.map(obj => {
            return expandMultiTokens(obj).values;
        }).flat();
        return makeMultiToken(...allExpanded);
    }

    const expandedKeys = [];
    const expandedValues = [];
    Object.keys(obj).forEach(key => {
        expandedKeys.push(key);
        const expanded = expandMultiTokens(obj[key]);
        expandedValues.push(expanded.values);
    });
    const templateObject = {};
    for (let i = 0; i < expandedKeys.length; i++) {
        const key = expandedKeys[i];
        const value = expandedValues[i];
        templateObject[key] = value[0];
    }

    const expanded = nestedForLoops(expandedValues, (...values) => {
        const templateCopy = cloneDeep(templateObject);
        for (let i = 0; i < values.length; i++) {
            const key = expandedKeys[i];
            templateCopy[key] = values[i];
        }
        return templateCopy;
    });

    return makeMultiToken(...expanded);
};

const expandTemplate = template => {
    return expandMultiTokens(template).values;
};

const getId = () => {
    return uuid.v4();
};

const parseTemplate = (parsed) => {
    let simulations = [];
    const templateMap = {};
    const { template } = parsed;
    // Step 1: Expand out all arrays that should be single values via cartesian product
    const grouping = expandTemplate(template);
    // Step 2: Generate group keys, simulation IDs, and request IDs
    const templateId = getId();
    templateMap[templateId] = {
        name: template.name,
        template,
        groups: {}
    };
    for (const group of grouping) {
        group.groupKey = getId();
        const ids = Array.from(Array(group.count)).map(i => getId());
        templateMap[templateId].groups[group.groupKey] = {
            simulations: ids,
            schema: group,
            iterationCount: group.totalIterationCount * group.count
        };
        delete group.count;
        group.jobId = templateId;
        simulations.push({
            ids,
            config: group
        });
    }

    return [simulations, templateMap];
};

const MANY = "MANY";
const ONE = "ONE";

const getVisualizationCardinality = visualization => {
    switch (visualization.type) {
        case PIE:
            return ONE;
        case LINE:
            return ONE;
        case VALUE:
            return ONE;
        case HISTOGRAM:
            return MANY;
        case SCATTER:
            return MANY;
        default:
            throw Error(visualization.type);
    }
};

const getStatisticCardinality = statistic => {
    switch (statistic.type) {
        case TOP_PERCENT_WEALTH:
            return ONE;
        default:
            throw Error(statistic.type);
    }
};

const getCombiner = (templateCardinality, visualizationCardinality, statisticCardinality) => {
    if (templateCardinality === ONE && (statisticCardinality === visualizationCardinality)) {
        return {
            type: "NONE"
        }
    } else if (visualizationCardinality === MANY && templateCardinality === MANY && statisticCardinality === ONE) {
        return {
            type: "COLLECT"
        }
    } else if (visualizationCardinality === MANY && templateCardinality === MANY && statisticCardinality === MANY) {
        throw Error(); // need collect & flatten
    } else if (visualizationCardinality === ONE && templateCardinality === MANY && statisticCardinality === ONE) {
        return {
            type: "MEAN"
        }
    }
    throw Error();
};


const parseVisualizer = (defaultName, visualizer, groupKeys, count) => {
    const templateCardinality = count > 1 ? MANY : ONE;

    const statisticValueMap = new CustomMap(JSON.stringify);
    let statistics = [];
    let visualizerVisualizations = {};
    let statisticsMap = {};
    let visualizationMap = {};
    for (const category in visualizer.visualizations) {
        visualizerVisualizations[category] = [];
        for (const visualization of visualizer.visualizations[category]) {
            const visualizationId = getId();
            const { statistic } = visualization;
            const stateStatistic = cloneDeep(statistic);
            stateStatistic.values = {};
            let statisticId = statisticValueMap.get(statistic) || getId();
            const visualizationCardinality = getVisualizationCardinality(visualization);
            const statisticCardinality = getStatisticCardinality(statistic);
            visualizerVisualizations[category].push(visualizationId);
            visualization.statistics = {root: statisticId};
            delete visualization.statistic;
            visualizationMap[visualizationId] = visualization;
            if (!(statisticId in statisticsMap)) {
                statisticsMap[statisticId] = stateStatistic;
            }
            statistic.combiners = {};
            for (const groupKey of groupKeys) {
                const combiner = getCombiner(templateCardinality, visualizationCardinality, statisticCardinality);
                statistic.combiners[groupKey] = [combiner];
            }

            statistic.id = statisticId;
            statistics.push(statistic);
        }

    }
    const parsedVisualizer = cloneDeep(visualizer);
    parsedVisualizer.id = getId();
    parsedVisualizer.name = parsedVisualizer.name || defaultName;
    parsedVisualizer.entityIds = groupKeys;
    parsedVisualizer.areGroups = true;
    parsedVisualizer.visualizations = visualizerVisualizations;
    // TODO probably gonna need to insert original visualization schema,
    // need to rehydrate
    return [parsedVisualizer, visualizationMap, statisticsMap, statistics];
};

export const parseSchema = schema => {
    const parsed = JSON.parse(schema);
    const isFull = parsed.type === "FULL";
    let request = {id: getId()};
    if (isFull) {
        request.type = "FULL";
    } else {
        request.type = "STATS";
    }
    if (isFull) {
        const [simulations, templateMap] = parseTemplate(parsed);
        request.simulations = simulations;

        let groupKeys = [];
        for (const templateId in templateMap) {
            groupKeys = groupKeys.concat(Object.keys(templateMap[templateId].groups));
        }

        const [visualizer, visualizationMap, statisticsMap, statistics] = parseVisualizer(
            parsed.template.name,
            parsed.visualizer,
            groupKeys,
            parsed.template.count
        );
        request.statistics = statistics;
        request.execution = parsed.execution;

        return {
            request,
            schema,
            templateMap, // template ID to {groupKey: simulation IDs}
            visualizer, // groupKey to {visualizationID : visualization}
            visualizationMap,
            statisticsMap
            // Maybe include statistic IDs in groupKey of template map or include groupKey/simulation Id in statistic?
        };
    } else {
        throw Error(); // TODO statistics request
    }
};